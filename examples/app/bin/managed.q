// ================================== \\
//  Launch managed application stack  \\
// ================================== \\

-1 "=== VERIFY PRESENCE OF EVERY ENVVAR REQUIRED ===";
envvars:`Q`MIQ_BASE`MIQ_BASE_CFG`MIQ_LOG`COMMON_SLOT;
{if[""~getenv[x]; -2"[ERROR] Environment variable missing: ",-3!x; exit 10]} each envvars;

-1 "=== CLEARING LOG DIRECTORY ===";
hdel each {` sv'x,'key[x]} -1!`$getenv[`MIQ_LOG];

-1 "=== CODE & CONFIGURATION === ";
system"l ",getenv[`COMMON_SLOT],"/codebase.q";

/ envvars
/.miq.system.require[`var;]'[`Q`MIQ_BASE`MIQ_BASE_CFG`MIQ_LOG`COMMON_SLOT];

/ Reconfiguration & option assertion
.arg.mandatory.add'[`env`executor`common`ports];
.arg.config.set[`program.description;"Launcher of managed application stack for example application"];
.arg.config.set[`help.auto;1b];
/.arg.config.set'[` sv'`assert,'`auto`exit`help;111b];
.arg.config.apply[];

.clan.config.set[`port.definition;`relative];
.clan.config.set[`port.base;first .arg.get`ports];
.clan.config.apply[];

.STL.load each `exec`mat;

.log.info "=== PROCESS RESOLUTION ===";
.log.show processes:key .clan.select pattern:(.arg.get[`clan];.arg.get[`role];`);
if[0~count processes; '.log.error "Process definition matches no processes: ",-3!pattern];

.log.info "=== PROCESS MATERIALISATION ===";
handles:.mat.emerge each processes;


/ Temporary arrangements for convenience
s:{-25!(handles;(exit;0)); -25!(handles;::)} / Needs to flush for .z.exit
.z.exit:{[x] s[]; show x;}

/
TODO Test presence of relevant directories and matriarch files
TODO Allow binding to concrete interface


#!/bin/bash
SELF_DIR=$(dirname $(readlink -f ${BASH_SOURCE[0]}))
SELF_FILE=$(basename $(readlink -f ${BASH_SOURCE[0]}))

echo "[INFO] Executing: $SELF_DIR/$SELF_FILE $@"
echo "[INFO] Changing directory: $SELF_DIR"
cd $SELF_DIR

ENV=$(readlink -f $1)       # env file
EXECUTOR=$(readlink -f $2)  # startup script
shift 2                     # Remove above from $@

echo "[INFO] Sourcing: $ENV"
source $ENV

EXIT=$?
if [[ $EXIT -gt 0 ]]; then
    echo "[ERROR] Source failed: $EXIT!";
    exit 1;
fi

echo "[INFO] Starting: $EXECUTOR"
#$Q $EXECUTOR -env $ENV -executor $EXECUTOR $@
q $EXECUTOR -env $ENV -executor $EXECUTOR -name ${EXECUTOR##*/}.$$ $@

EXIT=$?
if [[ $EXIT -gt 0 ]]; then
    echo "[ERROR] Booting failed: $EXIT!";
    exit 2;
fi

exit 0

#
# TODO check that ENV and EXECUTOR files exist!


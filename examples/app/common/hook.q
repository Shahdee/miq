// Logging
\d .log
.app.hook.log.generate.name:{[x]
  opts:$[.is.qd[x]; x; @[{.arg.args[]}; ::; {`$raze each .Q.opt[.z.x]}]];

  / TODO how to make sure the object class is defined before this common code being called...
  / - maybe the need disappear once we rework log module and hooks?
  if[.miq.object.is.defined[`clan.process];
    sig:.miq.object.signature[`clan.process];
    if[all sig in key opts; :` sv raze opts[sig]];
    .log.error "Cannot generate log name using value of options: ",-3!sig;
  ];

  .log.error "Generating random log name: ",-3!name:`$(10?.Q.a,.Q.A),".",string .z.i;
  name
 };


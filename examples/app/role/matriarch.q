// Matriarch

/ Reconfiguration
/.arg.mandatory.add'[`env`executor`common`ports];
.arg.mandatory.add'[`common`ports];
.arg.config.set[`program.description;"Management process central to clan"];
.arg.config.apply[];

.clan.config.set[`port.definition;`relative];
.clan.config.set[`port.base;first .arg.get`ports];
.clan.config.apply[];

/ Necessary libraries
.STL.load each `exec`mat;

/ Process materialisation
processes:key .clan.select``;
processes:processes where not .clan.self~/:processes;
handles:.mat.emerge each processes;

/ TODO delayed connect to processes on the same level or above
/.clan.connect (`;.clan.self[`role];`);

/ TODO define role name within supporting module to prevent hardcoding
/.mat.self:`matriarch;


/ Temporary arrangements for convenience
s:{-25!(handles;(exit;0)); -25!(handles;::)} / Needs to flush for .z.exit
.z.exit:{[x] s[]; show x;}


/system"p 9001"

/tab:([]date:`date$();time:`timestamp$();sym:`$();price:`float$();size:`long$())

/.STL.load`tp`tl;
.STL.load each `tp`schema;

/.tp.init[.CFG.load];
.tp.config.set[`mode;`batched];
.tp.config.set[`interval;10000];
.tp.init[];
/.schema.init[];

.COMMON.load`schemas;

.tp.table.add[;`] each exec name from .schema.list[];


/
.log.init[`tp_;`:/home/michal/logs]

if[system"t"; //Is the timer on
  .z.ts:.tp.batchPub;
  upd:.tp.batchUpd;
  ];

if[not system"t";
  system"t 1000";
  .z.ts:.tp.singlePub;
  upd:.tp.singleUpd;
  ];

.tp.tick["log_tp.";"/kdb/tlogs"]

system"d ",string .miq.cfg.root[];

mat.opts.general  :.miq.object.signature[`clan.process],`port`common`debug;
mat.opts.matriarch:mat.opts.general,`ports;
/mat.opts.worker   :(),`master;

mat.emerge.sleep:100;
/mat.emerge.inject:@[.arg.get;`inject;{`$()}];
mat.emerge.introduce:`clan`role;
mat.emerge.restrict:`tier`instance;

mat.hb.enable  :1b;
mat.hb.interval:30000;


// =======
//  Hooks
// =======
/.mat.z.proc.general:{[ch;opt]
/  .mat.h.ipc.async[ch;({.clan.identify .z.w};::)];
/ };

/.mat.z.proc.matriarch:{[ch;opt]
/  /.slot.remote.load ;
/  .mat.h.ipc.async[ch;(`.ROLE.load;opt)];
/ };

.mat.z.opts.common:{[ch;opt]
  / TODO move the check to callback processing
  if[0~count opt; .mat.h.log.warn "Option [common] missing value"; :()];

  common:opt!.slot.resolve[`COMMON] each opt;
  /.slot.remote.load each common;

  if[0<count where 1<>count each common;
    '.mat.h.log.error "Ambiguous module resolution: ",-3!where 1<>count each common;
  ];
  common:.slot.module.path each raze common;

  .mat.h.log.info "Executing common code: ",-3!common;
  .mat.h.ipc.async[ch;({system"l ",x}@';1_'string common)];
  /.mat.h.ipc.async[ch;({.clan.identify .z.w};::)];

  zw:.mat.h.ipc.sync[ch;".z.w"];
  manager:update handle:zw,connect:1b from .clan.select .clan.self;
  .mat.h.ipc.async[ch;(`.clan.universe.add;manager)];
 };

.mat.z.opts.role:{[ch;opt]
  .mat.h.ipc.async[ch;(`.ROLE.load@';opt)];
 };

/
TODO .mat.z.startup & .mat.z.shutdown hooks to be code thar runs first after start & last before stop?


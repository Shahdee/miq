system"d ",string .miq.cfg.root[];

arg.opts:flip`opt`cast`alias`description!flip(
  (`env      ;`fs   ;`$()  ;"Location of application environment file (shell)"          )
 ;(`executor ;`fs   ;`$()  ;"Location of script launching the application"              )
 ;(`tier     ;`s    ;`$()  ;"Application environment tier, i.e. DEV, QA, PROD"          )
 ;(`instance ;`s    ;`$()  ;"Instance within application tier, i.e. DEV.A, DEV.B"       )
 ;(`clan     ;`sl   ;`$()  ;"Application instance can be subdivided into clans"         )
 ;(`role     ;`sl   ;`$()  ;"Role assumed by the process"                               )
 ;(`name     ;`s    ;`$()  ;"Process name unique within single instance"                )
 ;(`p        ;`port ;`port ;"Listening port of process"                                 )
 ;(`ports    ;`jr   ;`$()  ;"Port range made available for application"                 )
 ;(`common   ;`sl   ;`$()  ;"Common code loaded by all processes from COMMON slot"      )
 ;(`debug    ;`b    ;`$()  ;"Debugging flag"                                            )
 ;(`help     ;`b    ;`$()  ;"Print help"                                                )
  );

arg.mandatory:`tier`instance`clan`role`name;

arg.assert.auto:1b;
arg.assert.exit:1b;
arg.assert.exitcode:9;

arg.program.description:"Individual process in full control of Matriarch. Not intended for manual execution.";


\l miq/miq.q
.miq.config.set[`module.module.directory;`:miq`:STL];
.miq.config.set[`module.classes;1#`module];
.miq.init[];

.miq.module.load[`module;`log];
.log.config.set[`level;`info];
.log.init[];

.miq.module.load[`module] each`arg`env;

.arg.init[];
.arg.add .'(
  (`name ;`s  )
 ;(`port ;`j  )
 ;(`date ;`d  )
 ;(`idxs ;`sl )
  );
.arg.process[];

.env.init[];

show .bb;
show .arg.args[];


.miq.module.load[`module;`clan];
.arg.add .'(
  (`tier     ;`s )
 ;(`instance ;`s )
 ;(`clan     ;`s )
 ;(`role     ;`s )
  );
.arg.inject .'(
  (`tier     ;`QA)
 ;(`instance ;`A )
 ;(`clan     ;`fx)
 ;(`role     ;`tp)
  );
.arg.process[];

.clan.init[];


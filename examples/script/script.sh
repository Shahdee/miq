#!/bin/bash
SELF_DIR=$(dirname $(readlink -f ${BASH_SOURCE[0]}))
SELF_FILE=$(basename $(readlink -f ${BASH_SOURCE[0]}))

echo "[INFO] Executing: $SELF_DIR/$SELF_FILE $@"
echo "[INFO] Changing directory: $SELF_DIR"
cd $SELF_DIR

SCRIPT=$1
shift

#q script.q -p :1213 -name test -port 1212 -date 2020.03.12 -idxs SAP,FTSE,DOW
q $SCRIPT $@


* Documentation: [https://shahdee.gitlab.io/miq](https://shahdee.gitlab.io/miq/#/)
* Author       : Michal Širochman / [LinkedIn](https://www.linkedin.com/in/sirochman)

What
===
* miQ kernel provides common machinery that underpins the module ecosystem
* Formal definition of a 'module' enables creation of universally shareable code
* Ecosystem of common purpose modules to use from the get go

Why
===
* Write code once and reuse endlessly!
* Manage inter-module dependencies through formal interface definition
* Built-in state space for modules
* Built-in configuration management for modules
* Code separation based on its purpose - infrastracture, configuration, application, ...
* Greatly extended type system and uniform type checking
* Many ease of life \& utility functionality that you cannot imagine any other language not having
* Reduce cost & time requirements of delivering on project goals by using code that already exists
* Using open source can reduce development time drastically while making the code more robust and feature full

Embedding
===
```
git clone https://gitlab.com/Shahdee/miq.git
cd miq/
MIQ_SILENT_BOOT=true MIQ_MODULE_PATH=STL/ QINIT=miq/miq.q q
```

* `MIQ_SILENT_BOOT` - optional, supress logging during until miQ self-initialises
* `MIQ_MODULE_PATH` - optional, paths to directories containing modules
* `QINIT` - use to embed miQ into your q

You can also quick-boot miQ kernel by simply executing it:
```
q miq/miq.q
```

And put into a file named `miq`, you can call `miq` command from your shell to start an miQ enabled console:
```
#!/bin/bash
Q=<__PATH_TO_Q_BINARY__>
MIQ=<__PATH_TO_MIQ_REPO__>

export QINIT=${MIQ}/miq/miq.q
export MIQ_MODULE_PATH=${MIQ}/STL
export MIQ_SILENT_BOOT=true

rlwrap -r ${Q} $@
```

What's in
===
* *miq* - miQ kernel
* *STL* - standard miQ modules
* *test* - unit tests ([miQ.test](https://gitlab.com/Shahdee/miq.test) | [docu](https://shahdee.gitlab.io/miq.test/#/))
* *docs* - documentation
* *ci* - CI pipelines
* *c* - [WIP] C code used by some miQ *STL* libraries
* *examples* - [WIP] Some examples, serves mostly dev testing purpose at the moment


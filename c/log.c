#include "k.h"
#include <stdio.h>      // fopen(),fclose(),snprintf()
#include <unistd.h>     // dup2()
#include <sys/stat.h>   // chmod()
#include <stdlib.h>     // malloc()
#include <string.h>     // strlen()

K redirect(K f, K m) {
    C *path;                                // My file path
    if (f->t == KC) {                       // Handle string
        path = malloc(1+f->n * sizeof(C));  // Extra byte for NUL
        snprintf(path,1+f->n,"%s",kC(f));
    } else if (f->t == -KS) {               // Handle symbol
        S symbol = f->s;
        if (symbol[0] == ':') symbol++;     // Strips a single leading colon
        int size = 1+strlen(symbol);
        path = malloc(size * sizeof(C));
        snprintf(path,size,"%s",symbol);    // Symbol IS a string
    } else
        return krr ("type");

    if (m->t != -KJ) return krr ("type");
    J mode = m->j;                          // My octal mode represented in decimal

    FILE *file;
    file = fopen(path,"w");                 // Opens new file for writing
    if (file == NULL)
        return orr(path);                   // Craps out if it fails

    if (mode != nj) {                       // 0Nj means don't change permission bits
        if (chmod(path,mode) == -1)         // chmod() returns -1 on error
            return orr(path);
    }

    dup2(fileno(file),STDOUT_FILENO);       // Duplicates log's fd, so both
    dup2(fileno(file),STDERR_FILENO);       // stdout and stderr fds are it's copies

    fclose(file);                           // Close our original fd

    return kb(1);                           // Return 1b
}

/*
 * TODO simplify the flow? specifically the (f)ile handling part
 * TODO have redirect accept a list of values instead?
 */

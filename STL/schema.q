// ======== \\
//  schema  \\
// ======== \\
/ Schema management

\d .schema
/ Meta
M.FAMILY :`stl;
M.NAME   :`schema;
M.VERSION:`0.001.0;
M.OBJECTS:(),`schema.schema;

/ Special
SELF  :system"d";
BB    :.miq.bb.new[SELF];

/ Object definition
O.SCHEMA:.miq.object.define[1b;`schema.schema;`name`columns`types;`s`sl`generic];
.miq.object.import[`schema.;` sv SELF,`o];

/ Config
CFG.DEFAULT :` sv SELF,`config`default;
CFG.EXPLICIT:.miq.cfg.explicit[SELF];
CFG.APPLIED :.miq.bb.new[` sv SELF,`config];
CFG.CONSTANT:` sv SELF,`C;

// ===========
//  Constants
// ===========
INIT   :(` sv BB,`init) set 0b;
SCHEMAS:` sv BB,`schemas;

/ Table templates
TT.SCHEMAS:1!o.schema.tt[]^flip(1#`zp)!(1#"P")$\:();

// ========
//  Config
// ========
/ IDEA .miq.cfg.import[` sv SELF,`config;`set`assemble`get] ?
/ - this will require to rework CFG.* setup and record the values...
/ - so I can automatically project the functions with the correct values
/ NOTE is it good idea to make it all tightly integrated? maybe...
.miq.fn.copy[` sv SELF,`config`set     ;.miq.cfg.set[CFG.EXPLICIT;CFG.DEFAULT]];
.miq.fn.copy[` sv SELF,`config`assemble;.miq.cfg.assemble[CFG.EXPLICIT;CFG.DEFAULT]];
.miq.fn.copy[` sv SELF,`config`get     ;.miq.cfg.get[CFG.APPLIED]];

.miq.cfg.default[CFG.DEFAULT].'(
  (`overwrite ;0b        )      / NOTE seems kinda crap config item name / purpose
 ;(`type.test ;`standard )      / NOTE instead of performance, redo into `q`miq`mixed option to restrict the schema type domain?
 );

.schema.config.apply:{[]
  .miq.cfg.apply[CFG.CONSTANT;CFG.APPLIED;.schema.config.assemble]
    each `overwrite`type.test;
 };

// ==========
//  Messages
// ==========
.schema.i.messages:{[]
  .is.generate[`warn`error;;1;]./:\:.[;(::;0);` sv SELF,`is,](
    (`present ;"No such schema: "                 )
   ;(`valid   ;"Schema not valid: "               )
   ;(`column  ;"Not a schema column: "            )
   ;(`type    ;"Not a valid schema column type: " )
  );
 };

// =========
//  General
// =========
.schema.init:{[fnConfigLoader]
  .miq.handler.import[`stl.log;` sv SELF,`h];
  .schema.i.messages[];

  .schema.h.log.info "Initialising: ",-3!SELF;
  SCHEMAS set @[get;SCHEMAS;TT.SCHEMAS];

  .miq.cfg.loader[fnConfigLoader;last` vs SELF];
  .schema.config.apply[];

  INIT set 1b;
  .schema.h.log.info "Initialisation completed: ",-3!SELF;
 };


// Accessors
.schema.list:{[x] SCHEMAS[x]};

// Checks
.schema.is.present:{[s] $[.is.s[s]; s in key SCHEMAS; 0b]};
.schema.is.column :{[schema;column] $[.schema.is.present[schema] & .is.s[column]; column in .schema.list[schema][`columns]; 0b]};
.schema.is.type   :{[x] $[.is.any[`h`s;x]; .is.q.type[x]|.is.miq.type[x]|x in (`;0Nh); 0b]};

/ TODO should this be a check?
.schema.is.valid:{[s]
  if[not .schema.is.present[s]; :0b];

  info:.schema.list[s];
  def:.schema.definition[s];

  $[(=). count each info`columns`types  ; 0b;
    (=). count each (def;info`columns)  ; 0b;
    not key[def]~info[`columns]         ; 0b;
    not all .schema.is.type'[info`types]; 0b;
                                          1b
  ]
 };

// ========
//  Schema
// ========
/ TODO implement this reverse-lookup of schema definitions
.schema.identify:{[qt]

 };

/ TODO accept traditional q types?
.schema.new:{[sName;slColName;colTypes]
  .is.error[`s`sl]@'(sName;slColName);
  .schema.is.error.type'[colTypes];
  if[.schema.is.present[sName]; '.schema.h.log.error "Schema already defined: ",-3!sName];

  SCHEMAS upsert (sName;0#`;();.z.p);
  .schema.column.i.add[sName].'flip(slColName;colTypes);
  sName
 };

.schema.drop:{[sName]
  .schema.is.error.present[sName];

  delete from SCHEMAS where name = sName;
  sName
 };

.schema.definition:{[sName]
  .schema.is.error.present[sName];
  (!). SCHEMAS[sName][`columns`types]
 };

/ TODO rename to .schema.verify?
/ TODO may need merging with .schema.is.valid[]?
.schema.test:{[sName;qmData]
  .is.error.qm[qmData];
  .schema.is.error.present[sName];

  def:.schema.definition[sName];
  if[not key[def]~cols qmData; :0b];
  all .schema.type.test .'get[def],'enlist each get flip 0!qmData
 };

.schema.table:{[sName]
  .schema.is.error.present[sName];

  def:.schema.definition[sName];
  types:.schema.type.resolve each def;
  types:@[types;where 1<count each types;:;0Nh];
  flip .miq.u.q.info'[;`empty] neg types
 };

.schema.omamori:{[qmData]
  .is.error.qm[qmData];

  k:count keys qmData;
  k!enlist first each flip 0#0!qmData
 };

// =========
//  Columns
// =========
.schema.column.add:{[sName;sCol;colType]
  .schema.is.error.present[sName];
  .is.error.s[sCol];
  .schema.is.error.type[colType];

  .schema.column.i.add[sName;sCol;colType]
 };

.schema.column.i.add:{[sName;sCol;colType]
  def:.schema.definition[sName];
  if[(not C.OVERWRITE[]) & sCol in key[def];
    '.schema.h.log.error "Column definition overwrite forbidden: ",-3!(sName;sCol;colType)
  ];

  def:def,enlist[sCol]!enlist[colType];
  SCHEMAS upsert (sName;key[def];get[def];.z.p);
  sCol
 };

.schema.column.remove:{[sName;sCol]
  .schema.is.error.present[sName];
  .schema.is.error.column[sName;sCol];

  .schema.column.i.remove[sName;sCol]
 };

.schema.column.i.remove:{[sName;sCol]
  def:sCol _ .schema.definition[sName];
  SCHEMAS upsert (sName;key[def];get[def];.z.p);
  sCol
 };

/ TODO rename to .schema.column.verify[]?
.schema.column.test:{[sName;sCol;data]
  .schema.is.error.present[sName];
  .schema.is.error.column[sName;sCol];
  .is.error.list[data];

  .schema.type.test[;data] .schema.definition[sName][sCol]
 };

// =======
//  Types
// =======
.schema.type.resolve:{[colType];
  .schema.is.error.type[colType];
  $[.is.q.type[colType];::;.is.miq2q] colType
 };

/ TODO rename to .schema.type.verify[]??
.schema.type.test:{[colType;data]
  .schema.is.error.type[colType];

  $[null colType        ;
      1b;
    .is.h[colType]      ;
      $[colType < 0h;
          neg[colType]~type[data];
          ((),colType)~distinct type'[data]
      ];
    .is.miq.atom[colType];
      all .is[.is.neg[colType]][data];
      all .is[colType]'[data]
  ]
 };


/
TODO .schema.apply[] to shape the table and leverage miQ type casting (once completed)
TODO Accept multiple types per column?
TODO Update and extend to be same, but update only changes existing and extend only adds new?
q).schema.new[`table;1;`date`time`sym`px`vol;`d`t`s`f`j]
q).OC.SCHEMA(`table;1), flip((`date;`d);(`time;`t);(`sym;`s);(`px;`f);(`vol;`j))
IDEA Advanced table schema definitions with types, lengths, domains, serialisation flag and more?
TODO Hold mappings of schemas to tables so we can implement some nice & simple checks without forcing user to write boilerplate code


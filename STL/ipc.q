// ===== \\
//  IPC  \\
// ===== \\

\d .ipc
/ Meta
M.FAMILY  :`stl;
M.NAME    :`ipc;
M.VERSION :`0.001.0;
M.COMMANDS:`ip`lsof`bash`timeout`echo;

.miq.system.is.warn.command'[M.COMMANDS];

/ Special
SELF      :system"d";
BB        :.miq.bb.new[SELF];

/ Config
CFG.EXPLICIT:.miq.cfg.explicit[SELF];
CFG.DEFAULT :` sv SELF,`config`default;
CFG.APPLIED :.miq.bb.new[` sv SELF,`config];
CFG.CONSTANT:` sv SELF,`C;

// ===========
//  Constants
// ===========
INIT      :(` sv BB,`init) set 0b;
SERVICES  :` sv BB,`services;
INTERFACES:` sv BB,`interfaces;
HANDLES   :` sv BB,`handles;
LISTENER  :` sv BB,`listener;

SERVICE.LIST:` sv BB,`service`list;

/ Table templates
TT.SERVICES  :2!flip`name`protocol`port`alias!"SSJ*"$\:();
TT.INTERFACES:flip`ifname`operstate`family`scope`address`hostname!"SSSSSS"$\:();
TT.HANDLES   :1!flip`handle`active`address`openSelf`openTime`closeSelf`closeTime!"IBSBPBP"$\:();

// ========
//  Config
// ========
.miq.fn.copy[`.ipc.config.set     ;.miq.cfg.set[CFG.EXPLICIT;CFG.DEFAULT]];
.miq.fn.copy[`.ipc.config.assemble;.miq.cfg.assemble[CFG.EXPLICIT;CFG.DEFAULT]];
.miq.fn.copy[`.ipc.config.get     ;.miq.cfg.get[CFG.APPLIED]];

.miq.cfg.default[CFG.DEFAULT].'(
  (`timeout           ;5000            )
 ;(`flush             ;0b              )
 ;(`hostname          ;.z.h            )
 ;(`port.range        ;0 65535         )
 ;(`port.unprivileged ;1024            )
 ;(`service.file      ;`:/etc/services )
 ;(`service.strict    ;1b              )
  );

.ipc.config.apply:{[]
  .miq.cfg.apply[CFG.CONSTANT;CFG.APPLIED;.ipc.config.assemble] each
    `timeout`flush`hostname`port.range`port.unprivileged`service.file`service.strict;

  .miq.fn.copy[`.ipc.syntax.service] .ipc.syntax.i.service $[C.SERVICE.STRICT[];`strict;`lax];

  / TODO borrow `ipc.flush explicit config if explicit `flush not given
  .ipc.config.local[];
 };

.ipc.config.local:{[]
  L.FLUSH::C.FLUSH[];
 };

// ==========
//  Messages
// ==========
.ipc.i.messages:{[]
  .is.generate[`warn`error;;1;]./:\:.[;(::;0);` sv SELF,](
    (`is.port               ;"Not a valid port number: "  )
   ;(`is.service            ;"Not a valid service name: " )
   ;(`is.handle             ;"Not a valid handle: "       )
   ;(`is.address            ;"Not a valid IPC address: "  )
   ;(`is.listener           ;"Not a valid IPC listener: " )
   ;(`port.is.open          ;"Port not open: "            )
   ;(`port.is.free          ;"Port not free: "            )
   ;(`port.is.privileged    ;"Port not privileged: "      )
   ;(`port.is.unprivileged  ;"Port not unprivileged: "    )
  );
 };

// ===========
//  Interface
// ===========
.ipc.i.interface:{[]
  api:.miq.ns.prune[`h`i`t`z`C`L`config] .miq.ns.trim[``init] .miq.ns.tree[`.ipc];
  api:exec name from group api;
  api:api where not .miq.syntax.name.is.constant'[api];

  handler    :` sv'`stl,'`$1_'string api;
  implementer:api;
  arity      :.miq.fn.arity'[api];

  / TODO Make this nice and describe every function!
  .miq.handler.define[1b;;;;"!!! Description missing !!! IPC module public interface dump"]'[handler;implementer;arity]
 };

// =========
//  General
// =========
.ipc.init:{[fnConfigLoader]
  .miq.handler.import[`stl.log;` sv SELF,`h];

  .ipc.h.log.info "Initialising: ",-3!SELF;

  .ipc.i.messages[];
  .ipc.i.interface[];

  INTERFACES set @[get;INTERFACES;.ipc.i.interfaces[]      ];
  HANDLES    set @[get;HANDLES   ;.ipc.i.handles TT.HANDLES];
  LISTENER   set @[get;LISTENER  ;.z.X(1+.z.X?"-p")        ];

  .miq.cfg.loader[fnConfigLoader;last` vs SELF];
  .ipc.config.apply[];

  SERVICES set .ipc.i.services[C.SERVICE.FILE[]];
  SERVICE.LIST set distinct raze exec {x,y}'[name;alias] from SERVICES[];

  .miq.hook.primary[`.z.po;`.ipc.po;.ipc.po];
  .miq.hook.primary[`.z.pc;`.ipc.pc;.ipc.pc];

  .miq.handler.set[`stl.ipc.connect    ;`.ipc.connect   ];
  .miq.handler.set[`stl.ipc.disconnect ;`.ipc.disconnect];
  .miq.handler.set[`stl.ipc.sync       ;`.ipc.sync      ];
  .miq.handler.set[`stl.ipc.async      ;`.ipc.async     ];
  .miq.handler.set[`stl.ipc.flush      ;`.ipc.flush     ];
  .miq.handler.set[`stl.ipc.oneshot    ;`.ipc.oneshot   ];

  INIT set 1b;
  .ipc.h.log.info "Initialisation completed: ",-3!SELF;
 };

.ipc.i.services:{[fsServices]
  .is.error.fs[fsServices];

  .ipc.h.log.info "Sourcing services file: ",-3!fsServices;
  services:read0 fsServices;                                      / Load services file content
  services:.miq.u.text.uncomment[" \t#"] each services;           / Uncomment
  services:services where not .is.empty'[services];               / Remove empty lines
  services:.miq.u.text.dedup[" \t"] each services;                / Merge repeated white space characters

  TT.SERVICES upsert flip @[;2;"J"$] @[;0 1 3;`$] @[;0 2 1 3]     / Reorder and convert
    flip {(x 0;;;2_x) . "/" vs x 1} each " "vs'services           / Split and fit
 };

.ipc.i.interfaces:{[]
  .miq.system.is.error.command[`ip];
  .ipc.h.log.debug "Sourcing information on all interfaces";
  interfaces:`$(.j.k raze system"ip -br -j addr")@\:`ifname;
  TT.INTERFACES upsert raze .ipc.i.ipaddr each interfaces
 };

.ipc.i.handles:{[qtHandles]
  handles:key .z.W;
  missing:handles where not handles in\: key qtHandles;
  if[0~count missing; :qtHandles];
  addresses:.ipc.handle.info'[missing][`address];
  qtHandles,TT.HANDLES upsert flip`handle`address!(missing;addresses)
 };

.ipc.i.ipaddr:{[sInterface]
  .miq.system.is.error.command[`ip];
  .ipc.h.log.debug "Sourcing information on interface: ",-3!sInterface;
  dAddr:(),.j.k raze @[system;"ip -j addr show ",string sInterface; "[]"];
  dName:(),.j.k raze @[system;"ip -j -r addr show ",string sInterface; "[]"];
  if[()~dAddr; .ipc.h.log.warn "No such interface: ",-3!sInterface; :()];

  / show returns empty dicts for interfaces not requested
  if[0h~type dAddr; dAddr:raze dAddr; dName:raze dName];
  info:`ifname`operstate#dAddr;

  / Some interfaces simply don't have any addresses tied to them
  if[()~first dAddr[`addr_info]; :()];

  / addr_info segment is a table for single entry, or list of dicts for multiple entries
  / NOTE newer ip command versions return list of tables! {@[x;where type'[x]<>98h;enlist]}
  / NOTE and even newer ip command versions return an enlisted list of dicts!!!!!
  / TODO $[0h~type & 1~count; .z.s first]
  $[0h~type first dAddr[`addr_info];
    [
      addr:(uj/) enlist each first dAddr[`addr_info];
      name:(uj/) enlist each first dName[`addr_info];
    ];
    [
      addr:first dAddr[`addr_info];
      name:first dName[`addr_info];
    ]
  ];

  (count[addr]#`$info)^`$(select family,scope,address:local from addr)^(select hostname:local from name)
 };

// Accessors
.ipc.listener  :{[] LISTENER[]};
.ipc.services  :{[] SERVICES[]};
.ipc.interfaces:{[] INTERFACES[]};

.ipc.handles:{[x]
  update active:handle in key .z.W from HANDLES;

  $[.is.none[x]    ; HANDLES[];
    .is.b[x]       ; select from HANDLES where active;
    .is.any[`i`j;x]; select from HANDLES where handle=x;
    .is.cs[x]      ; select from HANDLES where address=x;
    .is.s[x]       ; select from HANDLES where address like ("*",string[x],"*");
                     .is.error.any[`none`b`i`cs`s;x]
  ]
 };

// ==========================
//  Naming syntax validation
// ==========================
.ipc.syntax.host:{[x]
  if[.is.qs[x]; x:string x];
  .is.error.cl[x];

  label:{(64>count x) & all (first x;last x) in .miq.u.text.Aa};
  $[253<count x                      ; 0b;
    not all x in .miq.u.text.Aan,".-"; 0b;
                                       all label'["." vs x]
  ]
 };

.ipc.syntax.i.service.strict:{[x]
  if[.is.qs[x]; x:string x];
  .is.error.cl[x];

  $[15<count x                      ; 0b;
    any "-"=(first x;last x)        ; 0b;
    not all x in .miq.u.text.Aan,"-"; 0b;
    not any x in .miq.u.text.Aa     ; 0b;
                                      not any 1=deltas where "-"=x
  ]
 };

.ipc.syntax.i.service.lax:{[x]
  if[.is.qs[x]; x:string x];
  .is.error.cl[x];

  $[16<count x                           ; 0b;
    any "-"=(first x;last x)             ; 0b;
    not all x in .miq.u.text.Aan,"-+._*/"; 0b;
    not any x in .miq.u.text.Aa          ; 0b;
                                           not any 1=deltas where "-"=x
  ]
 };

// ============================
//  Connection & Communication
// ============================
/ Checks
.ipc.is.port     :{[x] $[.is.whole[x]; x within C.PORT.RANGE[]; 0b]};
.ipc.is.service  :{[x] $[.is.s[x]; x in SERVICE.LIST[]; 0b]};
.ipc.is.interface:{[x] $[.is.s[x]; x in INTERFACES[][`ifname]; 0b]};
.ipc.is.handle   :{[x] .is.ch[x]};

.ipc.is.address:{[x]
  portlike:{$[.is.s[x]; .ipc.is.service[x]; .ipc.is.port[x]]};
  $[.is.s[x]    ; .ipc.is.service[x];
    .is.whole[x]; .ipc.is.port[x];
    .is.cs[x]   ; .z.s {$[all x in .Q.n;"J"$x;`$x]} last ":" vs string x;
    .is.cl[x]   ; .ipc.is.listener[x];
    3~count x   ; $[all .is[`b`s]@'(x 0 1); portlike[x 2]; 0b];
    2~count x   ; $[.is.any[`b`s;x 0]; portlike[x 1]; 0b];
                  0b
  ]
 };

.ipc.is.listener:{[x]
  if[not .is.cl[x]; :0b];
  l:":" vs $["rp,"~3#x;3_x;x];

  portservice:{$[all x in .Q.n; .ipc.is.port["J"$x]; .ipc.syntax.service[x]]};
  $[1~count l; portservice[first l];
    2~count l; $[.is.empty first l; 1b; .ipc.syntax.host[first l]] & portservice[last l];
               0b
  ]
 };

/ Casting
.ipc.to.address:{[x]
  .ipc.is.error.address[x];

  fromListener:{[x]
    x:$["rp,"~3#x;3_x;x];
    `$":",":" sv $[":" in x;":" vs x; ("";x)]
  };

  $[.is.any[`s`whole;x]; `$"::",string[x];
    .is.cs[x]          ; x;
    .is.cl[x]          ; fromListener[x];
    3~count x          ; `$":" sv string[x][3 1 2];
                         `$"::",string[x 1]
  ]
 };

.ipc.to.listener:{[x]
  .ipc.is.error.address[x];

  $[.is.any[`s`whole;x]; string[x];
    .is.cs[x]          ; 1_string x;
    .is.cl[x]          ; x;
    3~count x          ; $[x 0;"rp,";""],":" sv string[x][1 2];
                         $[x 0;"rp,";""],string[x 1]
  ]
 };

/ Primary API
.ipc.listen:{[address]
  .ipc.is.error.address[address];
  listener:.ipc.to.listener[address];

  system"p ",listener;
  get LISTENER set listener
 };

.ipc.test:{[address]
  .miq.system.is.error.command'[`timeout`bash`echo];
  .ipc.is.error.address[address];

  timeout:string C.TIMEOUT[]%1000;
  hp:2#":"vs 1_string .ipc.to.address[address];
  host:$[""~hp[0]; string .z.h; hp[0]];
  port:hp[1];

  cmd:"timeout ",timeout," bash -c 'echo > /dev/tcp/",host,"/",port,"' 2>/dev/null; echo $?";
  not get first system cmd
 };

.ipc.connect:{[address]
  $[2~count address;
    [addr:first address;tm:last address];
    [addr:first address;tm:C.TIMEOUT[]]
  ];

  .ipc.is.error.address[addr];
  .is.error.whole[tm];

  .ipc.i.connect (.ipc.to.address[addr];tm)
 };

.ipc.i.connect:{[address]
  .ipc.h.log.info "Opening connection: ",-3!address;
  .ipc.handle.i.store[;1b;first address] hopen address
 };

.ipc.disconnect:{[address]
  addr:0!select from .ipc.handles[address] where null closeTime;
  $[0~count addr; '.ipc.h.log.error "No matching address: ",-3!address;
    1~count addr; .ipc.i.disconnect first addr;
                  .ipc.i.disconnect each addr
  ]
 };

.ipc.i.disconnect:{[qdAddress]
  .ipc.h.log.info "Closing connection: ",-3!qdAddress;
  hclose qdAddress[`handle];
  .ipc.handle.i.close[qdAddress`handle;1b]
 };

.ipc.oneshot:{[address;msg]
  .ipc.is.error.address[address];
  .ipc.to.address[address]@msg
 };

.ipc.sync:{[ch;msg]
  .is.error.any[`ch`chl;ch];
  $[.is.atom[ch]; ch@msg; ch@\:msg]
 };

.ipc.async:{[ch;msg]
  .is.error.any[`ch`chl;ch];
  $[.is.atom[ch]; neg[ch][msg]; -25!(ch;msg)];
  if[L.FLUSH; .ipc.flush[ch]]
 };

.ipc.flush:{[ch]
  .is.error.any[`ch`chl;ch];
  $[.is.atom[ch]; neg[ch][]; -25!(ch;::)]
 };

// ================
//  Port utilities
// ================
/ Checks
.ipc.port.is.open        :{[x] $[.ipc.is.port[x]; .ipc.test[`$":" sv string (`;C.HOSTNAME[];x)]; 0b]};
.ipc.port.is.free        :{[x] $[.ipc.is.port[x]; not .ipc.port.is.open[x]; 0b]};
.ipc.port.is.privileged  :{[x] $[.ipc.is.port[x]; x<.ipc.port.unprivileged[]; 0b]};
.ipc.port.is.unprivileged:{[x] not .ipc.port.is.privileged[x]};

/ Accessors
.ipc.port.range:{[] C.PORT.RANGE[]};
.ipc.port.unprivileged:{[] C.PORT.UNPRIVILEGED[]};

/ Port listing
.ipc.port.list.all :{[x] .is.error.any[`j`none;x]; $[.is.none[x]; ::; {x where x>y}[;x-1]] {x+til 1+y-x}. C.PORT.RANGE[]};
.ipc.port.list.open:{[x] .is.error.any[`j`none;x]; $[.is.none[x]; ::; {x where x>y}[;x-1]] "J"$'last each ":" vs'.ipc.port.i.lsof[][`n]};
.ipc.port.list.free:{[x] .is.error.any[`j`none;x]; .ipc.port.list.all[x] except .ipc.port.list.open[x]};

/ Port retrieval & info
.ipc.port.random:{[j;constraint]
  .is.error.j[j];
  .is.error.any[`none`j`jl;constraint];

  neg[j]?$[
    .is.j[constraint] ; .ipc.port.list.free[constraint];
    .is.jl[constraint]; {x where x within y}[.ipc.port.list.free[];constraint];
                        .ipc.port.list.free[]
  ]
 };

.ipc.port.table:{[]
  `pid`ppid`command`fd`protocol`address xcol
    update "J"$p, "J"$R, "I"$f, `$P, -1!'`${$["*"~first x;":",1_x;x]}'[n] from
    .ipc.port.i.lsof[]
 };

.ipc.port.i.lsof:{[]
  .miq.system.is.error.command[`lsof];

  / TODO `lsof -i` fails when there are no open sockets visible by the user
  / TODO better way to deal with this? how to determine open and unused ports then?
  lsof:@[system;"lsof -i -sTCP:LISTEN -nPF cPnR";()];
  if[.is.empty[lsof]; :flip`p`R`c`f`P`n!"******"$\:()];

  lsof:(where "p"~/:first each lsof) cut lsof;
  raze {[cll]
    o:(f:where "f"~/:first each cll) cut cll;
    o:{(`$'first each x)!(1_'x)} each (f[0]#cll),/:o
  } each lsof
 };

// ========
//  Handle
// ========
.ipc.handle.list:{[] key .z.W};

.ipc.handle.info:{[ch]
  .ipc.is.error.handle[ch];
  lsof:.ipc.handle.i.lsof[ch];
  hp:":"vs last"->"vs lsof[`n];
  `ch`protocol`host`port`address`login!(ch;`$lsof[`t];`$hp[0];"J"$hp[1];-1!`$":" sv hp;lsof[`L])
 };

.ipc.handle.host:{[ch] .ipc.handle.info[ch][`host]};
.ipc.handle.port:{[ch] .ipc.handle.info[ch][`port]};

.ipc.handle.refresh:{[] .ipc.i.handles[HANDLES[]]};

.ipc.handle.i.lsof:{[ch]
  .miq.system.is.error.command[`lsof];
  o:system"lsof -P -i -a -p ",string[.z.i]," -d ",string[ch]," -FpcftnL";
  if[0~count o; '.ipc.h.log.error "lsof failed for handle: ",-3!ch];

  {(`$'first each x)!1_'x} o
 };

.ipc.handle.i.store:{[ch;bSelf;csAddress] HANDLES upsert TT.HANDLES[-1i],`handle`address`openSelf`openTime!(ch;csAddress;bSelf;.z.p); ch};
.ipc.handle.i.close:{[ch;bSelf] HANDLES upsert `handle`closeSelf`closeTime!(ch;bSelf;.z.p); ch};

// ============================
//  Custom hook implementation
// ============================
/ TODO update to .ipc.z.* once you rework miQ hook code
.ipc.po:{[handle]
  address:-1!`$last"->"vs .ipc.handle.i.lsof[handle][`n];
  .ipc.handle.i.store[handle;0b;address];
 };

.ipc.pc:{[handle]
  .ipc.h.log.info "Handle closed: ",-3!handle;
  .ipc.handle.i.close[handle;0b];
 };

/
TODO use netstat -tln instead of lsof to determine open ports?
TODO many functions will need to be updated to handle user and password secret
TODO handle user and password in the connection string
IDEA shared file locking? do I need it? does it belong here? see flock(1)
TODO new option translate localhost to affect behaviour of .ipc.handle.info[]?
TODO deferred response
TODO address retrieval based on interface and IPv standard
IDEA record number of request per handle?
TODO custom address type name of ctAddress?, defined within module
TODO .ipc.handles[] needs to improve formal parameter checking, i.e. don't allow bulk close, reflect standard address type, closing by address ...
TODO use .is.one[] to handle complex structures like address and listener? or rather object-based type system?


// ====== \\
//  PROF  \\
// ====== \\

\d .prof

// ===========
//  Constants
// ===========
SELF     :system"d";
BB       :.miq.bb.new[SELF];
INIT     :(` sv BB,`init) set 0b;
RUN.ID   :` sv BB,`run.id;
RUN.STEP :` sv BB,`run.step;
RUN.ITER :` sv BB,`run.last;
RUN.ERROR:` sv BB,`run.error;
ORIGINAL :` sv BB,`original;

UNWINDING:` sv BB,`unwinding;
RUNNING  :` sv BB,`running;
FUNCTION :` sv BB,`function;
TOTAL    :` sv BB,`total;

/ Config
CFG.EXPLICIT:.miq.cfg.explicit[SELF];
CFG.DEFAULT :` sv SELF,`config`default;
CFG.APPLIED :.miq.bb.new[` sv SELF,`config];
CFG.CONSTANT:` sv SELF,`C;

/ Table templates
TT.RUNNING :1!flip`n`function`child!"JSN"$\:();
TT.FUNCTION:1#2!flip`id`step`function`stack`parent`depth`user`context`input`start`end`total`own`space!"JJS*SJSS*PPNNJ"$\:();
TT.TOTAL   :flip`name`runs`time`ratio!"SJNF"$\:();

// ========
//  Config
// ========
.miq.fn.copy[`.prof.config.set     ;.miq.cfg.set[CFG.EXPLICIT;CFG.DEFAULT]];
.miq.fn.copy[`.prof.config.assemble;.miq.cfg.assemble[CFG.EXPLICIT;CFG.DEFAULT]];
.miq.fn.copy[`.prof.config.get     ;.miq.cfg.get[CFG.APPLIED]];

.miq.cfg.default[CFG.DEFAULT].'(
  (`stack   ;0b )
 ;(`parent  ;0b )
 ;(`depth   ;0b )
 ;(`user    ;0b )
 ;(`context ;0b )
 ;(`input   ;0b )
 ;(`exclude ;0#`)
  );

.prof.config.exclude:{
  `.prof.z.exec`.prof.z.dd.exec`.miq.hook.process
 ,`.prof.h.trap.apply`.prof.h.trap.print
 ,`.prof.i.ENTRY_POINT`.Q.trp`.Q.ts
 ,exec name from group raze .miq.ns.tree each `.prof.run`.prof.extend
 };

.prof.config.apply:{[]
  .miq.cfg.apply[CFG.CONSTANT;CFG.APPLIED;.prof.config.assemble] each
    `stack`parent`depth`user`context`input
   ,enlist(`exclude;{distinct .prof.config.exclude[],x});

  $[C.STACK[];
      .miq.hook.register[`.prof.z.exec;`stack;.prof.extend.stack];
      if[.miq.hook.is.label[`.prof.z.exec;`stack]; .miq.hook.remove[`.prof.z.exec;`stack]]
  ];

  $[C.PARENT[];
      .miq.hook.register[`.prof.z.exec;`parent;.prof.extend.parent];
      if[.miq.hook.is.label[`.prof.z.exec;`parent]; .miq.hook.remove[`.prof.z.exec;`parent]]
  ];

  $[C.DEPTH[];
      .miq.hook.register[`.prof.z.exec;`depth;.prof.extend.depth];
      if[.miq.hook.is.label[`.prof.z.exec;`depth]; .miq.hook.remove[`.prof.z.exec;`depth]]
  ];

  $[C.USER[];
      .miq.hook.register[`.prof.z.exec;`user;.prof.extend.user];
      if[.miq.hook.is.label[`.prof.z.exec;`user]; .miq.hook.remove[`.prof.z.exec;`user]]
  ];

  $[C.CONTEXT[];
      .miq.hook.register[`.prof.z.exec;`context;.prof.extend.context];
      if[.miq.hook.is.label[`.prof.z.exec;`context]; .miq.hook.remove[`.prof.z.exec;`context]]
  ];

  $[C.INPUT[];
      .miq.hook.register[`.prof.z.exec;`input;.prof.extend.input];
      if[.miq.hook.is.label[`.prof.z.exec;`input]; .miq.hook.remove[`.prof.z.exec;`input]]
  ];

  .prof.config.local[];
 };

.prof.config.local:{
  @[{L.SUM};::;{L.SUM::0}];
  L.STACK   ::C.STACK[];
  L.PARENT  ::C.PARENT[];
  L.USER    ::C.USER[];
  L.CONTEXT ::C.CONTEXT[];
 };

// =========
//  General
// =========
.prof.init:{[fnConfigLoader]
  .miq.handler.import[`stl.log ;` sv SELF,`h];
  .miq.handler.export[`stl.trap;` sv SELF,`h];   / Dereference to allow exclusion of profiling @.prof.config.exclude[]

  .prof.h.log.info "Initialising: ",-3!SELF;

  RUN.ID    set @[get;RUN.ID   ;{0}          ];
  RUN.STEP  set @[get;RUN.STEP ;{1}          ];
  RUN.ITER  set @[get;RUN.ITER ;{1}          ];
  RUN.ERROR set @[get;RUN.ERROR;{""}         ];
  UNWINDING set @[get;UNWINDING;{0b}         ];
  RUNNING   set @[get;RUNNING  ;{TT.RUNNING} ];
  ORIGINAL  set @[get;ORIGINAL ;{(0#`)!()}   ];
  FUNCTION  set @[get;FUNCTION ;{TT.FUNCTION}];
  TOTAL     set @[get;TOTAL    ;{TT.TOTAL}   ];

  .miq.cfg.loader[fnConfigLoader;last` vs SELF];
  .prof.config.apply[];

  INIT set 1b;
  .prof.h.log.info "Initialisation completed: ",-3!SELF;
 };

.prof.enable:{[nsFunction]
  .is.error.any[`none`ns;nsFunction];
  fns:.prof.expand[nsFunction];
  $[.is.list[fns]; raze .prof.i.enable'[fns]; .prof.i.enable[fns]]
 };

.prof.i.enable:{[nsFunction]
  $[nsFunction in C.EXCLUDE[]    ; [.prof.h.log.info "Excluding from profiling: ",-3!nsFunction; `$()];
    nsFunction in .prof.enabled[]; [.prof.h.log.debug "Profiling already enabled: ",-3!nsFunction; nsFunction];
                                   [.prof.h.log.info "Profiling: ",-3!nsFunction; .prof.i.wrap[nsFunction]]
  ]
 };

.prof.disable:{[nsFunction]
  .is.error.any[`none`ns;nsFunction];
  fns:.prof.expand[nsFunction];
  $[.is.list[fns]; .prof.i.disable'[fns]; .prof.i.disable[fns]]
 };

.prof.i.disable:{[nsFunction]
  $[nsFunction in .prof.enabled[];
      [.prof.h.log.info "Disabling profiling of: ",-3!nsFunction; .prof.i.restore[nsFunction]];
      .prof.h.log.warn "Profiling not enabled: ",-3!nsFunction
  ];
  nsFunction
 };

/ TODO use .miq.fn.is.executable[]?
/      Nope, profiler does not handle 'code' for the time being... :(
.prof.expand:{[nsFunction]
  .is.error.any[`none`ns;nsFunction];
  if[.is.ns[nsFunction]; .miq.ns.is.error.present[nsFunction]];

  $[.miq.ns.is.name[nsFunction];
      [.is.error.any[`op`fn`fp`qc`qi;@[get;nsFunction;nsFunction]]; nsFunction];
      names where in[;`op`fn`fp`qc`qi] (')[.is.type;get] each names:(group .miq.ns.tree[nsFunction])[`name]
  ]
 };

.prof.reset:{[] .prof.i.disable each .prof.enabled[]; .prof.clear[];};
.prof.clear:{[] TOTAL set TT.TOTAL; FUNCTION set TT.FUNCTION;};

// Accessors
.prof.enabled :{[] key ORIGINAL[]};
.prof.excluded:{[] C.EXCLUDE[]};

.prof.original:{[ns] .is.error.ns[ns]; $[ns in .prof.enabled[]; ORIGINAL[ns]; ()]};

.prof.runs:{[id]
  if[.is.none[id]; :1_FUNCTION[]];
  .is.error.any[`whole`wholeList;id];
  ?[FUNCTION[];.miq.fq.d2cond[in;(1#`id)!enlist id];0b;()]
 };

.prof.total:{[id]
  $[.is.none[id];
      if[count[FUNCTION[]]~1+exec sum runs from TOTAL; :TOTAL[]];
      .is.error.any[`whole`wholeList;id]
  ];
  get TOTAL set `ratio xdesc update ratio:100*time%sum time from select runs:count id, time:sum own by function from .prof.runs[id]
 };

// Internals
.prof.i.wrap:{[nsFunction]
  f:get nsFunction;
  ORIGINAL set ORIGINAL[],enlist[nsFunction]!enlist[f];

  info:.miq.fn.info[f];
  name:-3!nsFunction;
  args:{"(",$[.is.empty[x];"::";x],")"} ";" sv string info[`signature];

  code:" .prof.z.exec (",name,";",(-3!info[`arity] in 0 1),";",args,")";
  nsFunction set .miq.fn.copy[`.prof.i.ENTRY_POINT] -5!.miq.qk.fn.wrap[info`signature;code];
  .prof.i.ENTRY_POINT:();
  nsFunction
 };

.prof.i.restore:{[nsFunction]
  nsFunction set ORIGINAL[nsFunction];
  ORIGINAL set nsFunction _ ORIGINAL[];
  nsFunction
 };

// Functions for viewing / manipulating global state during execution run
.prof.run.id   :{[] RUN.ID[]};
.prof.run.step :{[] RUN.STEP[]};
.prof.run.depth:{[] count RUNNING[]};
.prof.run.error:{[] RUN.ERROR[]};
.prof.run.reset:{[] UNWINDING set 0b; RUNNING set TT.RUNNING; RUN.STEP set 1; RUN.ITER set 1};

// ====================================
//  Configurable profiling enhacements
// ====================================
.prof.extend.i.stackReduce:{[stack;nsFunction]
  frames:first each`$stack[;1;0];
  function:frames?nsFunction;
  entry:first w where function<w:where frames=`.prof.i.ENTRY_POINT;

  / Early exit if we fail to recognise `.prof frames to remove
  if[null[entry] | function~count frames; :$[0<last w; last[w]#stack; stack]];

  gap:entry-function;
  stk:stack@til[count stack] except raze w+\:neg til gap;
  .[stk;(::;3);:;til count stk]
 };

.prof.extend.stack:{[nsFunction;bUnary;args]
  / copy of .miq.u.stack[]
  stk:{$[(::)~x; .z.s -100!x; type x; .z.s[-103!(0b;x)],enlist -101!x; ()]}[];
  stk:.prof.extend.i.stackReduce[stk;nsFunction];
  update stack:enlist[stk] from FUNCTION where id in .prof.run.id[], step in .prof.run.step[]
 }.;

.prof.extend.parent:{[nsFunction;bUnary;args]
  p:$[0<c:count RUNNING[]; (0!RUNNING[])[c-1][`function]; `];
  update parent:p from FUNCTION where id in .prof.run.id[], step in .prof.run.step[]
 }.;

.prof.extend.depth:{[nsFunction;bUnary;args]
  update depth:.prof.run.depth[] from FUNCTION where id in .prof.run.id[], step in .prof.run.step[]
 }.;

.prof.extend.user:{[nsFunction;bUnary;args]
  update user:.z.u from FUNCTION where id in .prof.run.id[], step in .prof.run.step[]
 }.;

.prof.extend.context:{[nsFunction;bUnary;args]
  update context:system"d" from FUNCTION where id in .prof.run.id[], step in .prof.run.step[]
 }.;

.prof.extend.input:{[nsFunction;bUnary;args]
  update input:enlist[args] from FUNCTION where id in .prof.run.id[], step in .prof.run.step[]
 }.;

// =======
//  Hooks
// =======
.prof.z.dd.exec:{[nsFunction;bUnary;args]
  $[0~count RUNNING[];
      [runid:-6!(+:;RUN.ID;1); step:get RUN.ITER set 1];
      [runid:RUN.ID[]        ; step:-6!(+:;RUN.ITER;1)]
  ];

  function:.Q.ts ORIGINAL[nsFunction];
  arguments:$[bUnary;enlist;::] args;

  FUNCTION insert `id`step`function!(runid;step;nsFunction);
  RUNNING insert (step;nsFunction;0D);

  start:.z.p;
  out:.prof.h.trap.apply[function;arguments];
  end:.z.p;

  / Execution failure
  if[out[0];
    if[not UNWINDING[];
      UNWINDING set 1b;
      RUN.ERROR set out[1;0];
      .prof.h.log.error "exception caught at ", string[nsFunction];
      .prof.h.log.error .prof.h.trap.print[out[1;0];.prof.extend.i.stackReduce[out[1;1];nsFunction]]
    ];

    if[1~step; .prof.run.reset[]; -6!"'",enlist RUN.ERROR[]];
    '`unwind
  ];

  / Execution success
  child:RUNNING[step;`child];
  delete from RUNNING where n=step;
  total:end-start;
  own  :total-child;
  space:out[1;0;1];

  RUN.STEP set step;
  FUNCTION upsert `id`step`start`end`total`own`space!(runid;step;start;end;total;own;space);
  $[1~step; .prof.run.reset[]; RUNNING upsert @[last[0!RUNNING[]];`child;+;total]];

  out[1;1]
 }.;

if[not .miq.hook.is.defined[`.prof.z.exec]; .miq.hook.expunge[`.prof.z.exec]];


/
TODO .prof.z.dd.exec should rely on as little external code as possible to allow profiling of any function; `.trap.apply, `.trap.print, `.Q.trp, `.Q.ts, ...
TODO exclude exact and pattern, so I can better define what should not be profiled
TODO .prof.original[] could return original definitions even when passes function if .prof.i.ENTRY_POINT[] is named .prof.i.ENTRY_POINT@<.fn.name> when defined... worth it for the user convenience??? prolly not, profiler is not for newbs
TODO enable profiling of variables and constants, so I can track their use
IDEA implement self-correction option to approximate time it takes to execute exec hooks and remove it from totals?
TODO make columns and extension configurable
TODO record failures and stacks in separate table, keyed by `id`step
IDEA make run.* constants local?
TODO mark failed runs in FUNCTION
TODO auto-detect number of frames that wrapper takes in stack to optimise .prof.extend.stack


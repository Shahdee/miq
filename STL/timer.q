// ========= \\
//   TIMER   \\
// ========= \\
/ Timer overload
/ Distinction between a job and a run!!!
/ Job - a task to run, possibly periodically
/ Run - an instance of Job, that is executed
/:
/ - No more than one scheduled run per job
/ - Execution is guaranteed to happen, not guaranteed to happend exactly when scheduled

\d .timer

// ===========
//  Constants
// ===========
SELF:system"d";
BB  :.miq.bb.new[SELF];
INIT:(` sv BB,`init) set 0b;

JOBS    :` sv BB,`jobs;      / Job definitions
RUNS    :` sv BB,`runs;      / Run schedules
PENDING :` sv BB,`run`pending;   / Indices of pending runs in RUNS
/ONHOLD  :`.bb.timer.onhold;    / Indices of onhold runs in RUNS
/RETRY   :`.bb.timer.retry;     / Indices of runs to retry in RUNS
INTERVAL:` sv BB,`interval;  / Miliseconds between triggering .z.ts
RUNID   :` sv BB,`run`id;     / Current run id

/ Config
CFG.EXPLICIT:.miq.cfg.explicit[SELF];
CFG.DEFAULT :` sv SELF,`config`default;
CFG.APPLIED :.miq.bb.new[` sv SELF,`config];
CFG.CONSTANT:` sv SELF,`C;

/ Table template
/ status - `omamori`enabled`disabled`deleted
TT.JOBS:1!flip
  `id`create`expire`status`function`argument`expectsTime`lazy`period`retries`setback`lzp!"SPPS**BBNJNP"$\:();

/table.jobs:`jobId xkey flip
/  `jobId`create`expire`status`function`argument`expectsTime`lazy`period`retries`setback`lzp!"SPPS**BBNJNP"$\:();
/`table.jobs insert (`;0Wp;0Wp;`omamori;'[;];enlist"";1b;1b;`timespan$0;0;`timespan$0;.z.p);

/ status - `omamori`pending`running`completed`failed`skipped`onhold`deleted
/ TODO add `retry and keep retrying the same runId until success / no attempts left?
/ TODO make runId key, every retry a new run, and don't schedule until retries are completed
/table.schedule:`runId xkey flip
TT.RUNS:update `s#id from flip
  `id`job`execute`actual`status`function`argument`expectsTime`lazy`periodic`retries`lzp!"JSPPS**BBBJP"$\:();
/table.runs:update `s#runId from flip
/  `runId`jobId`execute`actual`status`function`argument`expectsTime`lazy`periodic`retries`lzp!"JSPPS**BBBJP"$\:();
/`table.runs insert (0;`;0Wp;0Wp;`omamori;'[;];enlist"";1b;1b;1b;0;.z.p);

// ========
//  Config
// ========
.miq.fn.copy[`.timer.config.set     ;.miq.cfg.set[CFG.EXPLICIT;CFG.DEFAULT]];
.miq.fn.copy[`.timer.config.assemble;.miq.cfg.assemble[CFG.EXPLICIT;CFG.DEFAULT]];
.miq.fn.copy[`.timer.config.get     ;.miq.cfg.get[CFG.APPLIED]];

.miq.cfg.default[CFG.DEFAULT].'(
  (`jobs   ;TT.JOBS )
 ;(`runs   ;TT.RUNS )
  );

.timer.config.apply:{[]
  .miq.cfg.apply[CFG.CONSTANT;CFG.APPLIED;.timer.config.assemble]
    each `jobs`runs;

  JOBS upsert C.JOBS[];
  RUNS upsert C.RUNS[];
 };


// =========
//  General
// =========
// Public
.timer.init:{[t]
  JOBS     set @[get;JOBS   ;TT.JOBS ];
  RUNS     set @[get;RUNS   ;TT.RUNS ];
  PENDING  set @[get;PENDING;`long$()];
  RUNID    set @[get;RUNID  ;0       ];

  `.z.ts   set .timer.ts;

  .timer.interval[$[null t;0;t]];
  .timer.start[];
 };

/ Set interval
.timer.interval:{[ms] $[null ms; get INTERVAL; get INTERVAL set ms]};
.timer.reset:start:{[] system"t ",string .timer.interval[]};
.timer.stop:{[] system"t 0"};

/newRunId:{[] .bb.timer.runId+:1};
.timer.newRunId:{[] eval(+:;RUNID;1)};       / 1 second per 1,000,000 calls slower than above
.timer.newJobId:{[] (in[;key JOBS]){`$(2?.Q.A),3?.Q.n}/`$(2?.Q.A),3?.Q.n};

.timer.ts:{[] .timer.execute[]};

.timer.execute:{[]
  / Obtain pending runs and test their execution times
  / TODO use .Q.ft to index into keyed table
  / NOTE maybe admit that runId is the same as index in the table?
  /      .Q.ft changes into simple key lookup
  run:RUNS[pending:get PENDING];
  map:run[`execute]<.z.p;

  / Filter out runs to execute now; if there aren't any, finish
  if[not max map; :()];
  run:run where map;
  index:pending where map;
/      PENDING set get[PENDING] where not map;

  / Trigger scheduled runs and mark them appropriately
  ?[.timer.run.i.trigger each run;`.timer.run.i.success;`.timer.run.i.failure]@'run[`id];

  / Retry logic
  /TODO run.i.retry each select from pending where not status;

  / Schedule new & fill in skipped runs, update indices PENDING (remove executed, add new)
  PENDING set pending:(pending where not map),raze .timer.run.i.schedule each run where run`periodic;

  / Speed up timer if there are runs within next INTERVAL
  runIn:(min RUNS[pending;`execute]) - .z.p;

  $[(.timer.interval[]*1000000)>runIn;
    system"t ",string`long$max 1,ceiling runIn%1000000; / could be system"t ",.log.debug ...
    .timer.reset[]
  ];
 };

// Exposed Jobs & Runs interface
/ Creates a new job, returns (jobId;runId)
.timer.job.new:{[dict] /[create;expire;status;fn;arg;expectsTime;lazy;period;retries;setback]
  default:`create`expire`status`expectsTime`lazy`retries`period`setback!
    (2#0Np),(`enabled;0b;0b;0),2#`timespan$0;
  dict:default,dict;

  /now:$[null dict[`create]; [dict[`create]:.z.p; 1b]; 0b];
  if[null dict[`create]; dict[`create]:.z.p];
  if[null dict[`expire]; dict[`expire]:dict[`create]];

  / Allow jobs to pass backward in time?
/  if[dict[`create]>dict[`expire]; .log.error "Invalid time range"];
  if[count missing:cols[JOBS] except `id`lzp,key dict; .log.error "Missing key:",-3!missing];
  if[not dict[`status] in `enabled`disabled; .log.error "Invalid status:",-3!dict`status];

  / TODO if[not -11h~type dict[`function]; dict:[`function]: some lambda code...];

  dict[`period ]:`timespan$$[-7h~type dict[`period ];dict[`period ]*1000000;dict[`period ]];
  dict[`setback]:`timespan$$[-7h~type dict[`setback];dict[`setback]*1000000;dict[`setback]];

  JOBS insert (`id`lzp!(id:.timer.newJobId[];.z.p)),dict;

  / Return (jobId;runId) tuple
  (id;$[`enabled~dict[`status]; .timer.job.schedule[id]; 0N])
  / Logging - created jobId: , not scheduled...
 };

/ Scheduling a job creates a run
.timer.job.schedule:{[id]
  if[id in RUNS[get PENDING;`job];
    .log.warn "Job ",(-3!id)," already scheduled as: ",-3!active
  ];

  dict:JOBS[id];
  dict[`job]:id;
  dict[`periodic]:0<>dict[`period];
  dict[`retries ]:0;
/  dict[`execute ]:dict[`create]-dict[`period];


  / If periodic & more than one period ahead of creation
  dict[`execute]:$[not[dict`periodic] | dict[`create]>.z.p-1000000000;
    dict[`create]-dict[`period];  / Hack for run.i.new as it is incrementing my execute time
    last dict[`create]+dict[`period]*1_til 1+floor(0N!.z.p-dict[`create])%dict[`period]
  ];

  /TODO handle disable / enable scenarios

/  dict[`execute]:$[dict[`periodic] & (.z.p-dict[`create])>dict[`period];
/    last dict[`create]+dict[`period]*1+til floor(.z.p-dict[`create])%dict[`period];
/    dict[`create]
/  ];

  PENDING set get[PENDING],i:RUNS insert (cols RUNS)#.timer.run.i.new dict;

  RUNS[i;`id]
 };

/ Ad hoc job execution
.timer.job.execute:{[id]
  .log.info "Non-scheduled execution requested for id: ",-3!id;
  / TODO insert runId with `manual into RUNS
  .timer.run.i.execute JOBS[id]
 };

/ Clone a job identified by given jobId, issuing new jobId and returning it
.timer.job.clone  :{[id] JOBS insert JOBS[id],enlist[`id]!enlist[id:.timer.newJobId[]]; id};
/ TODO remove / add pending run indices from PENDING
.timer.job.delete :{[sJob] update status:`deleted  from JOBS where id=sJob; .timer.run.i.delete[sJob]};
.timer.job.disable:{[sJob] update status:`disabled from JOBS where id=sJob;};
.timer.job.enable :{[sJob] update status:`enabled  from JOBS where id=sJob; .timer.job.schedule[sJob]};

/job.disable:{[id] $[`enabled~s:JOBS[id][`status]; run.i.hold[id]; '"Cannot hold - job is ",-3!s];};
/ TODO maybe just have .timer.execute ignore it every time it runs?
.timer.run.hold   :{[jRun]
  index:exec i from RUNS where id=jRun,status=`pending;
  update status:`onhold from RUNS where i=index;
  PENDING set get[PENDING] except index;
/  ONHOLD
 };

.timer.run.release:{[jRun]
  index:exec i from RUNS where id=jRun,status=`onhold;
  update status:`pending from RUNS where i=index;
  PENDING set get[PENDING],index;
 };


// Internal
/ Trigger run; returns 1b on success, 0b otherwise
.timer.run.i.trigger:{[dict]
  .log.info "Triggering run: ",-3!dict`id;

  dict[`actual]:.z.p;
  update actual:dict[`actual] from RUNS where id=dict[`id];

  .timer.run.i.execute[dict]
 };

.timer.run.i.execute:{[dict]
  .log.info "Function: ",-3!dict`function;
  .log.info "Argument: ",-3!dict`argument;

  / TODO Trapping should be taken care of by a separate module
  r:.Q.trp[
    .[dict[`function]];
    (),dict[`argument],$[dict[`expectsTime];dict[`execute];()];
    {[e;bt] .log.error "Run failed: ",e,"\nBacktrace:\n",.Q.sbt bt;0b}
  ];

  $[0b~r;0b;1b]
 };

/ Schedules new run and returns it's index
.timer.run.i.schedule:{[dict]
  dict:dict,`expire`period#JOBS[dict`job];
  schedule:dict[`execute]+dict[`period];
  / TODO calculate schedule here and guarantee last run of any jobId?

  $[(dict[`expire]<schedule);
    [
      .log.info "job [",(-3!dict[`job]),"] has completed it's schedule";
      0#0
    ];
    dict[`lazy] & schedule<.z.p;
    [
      RUNS insert`expire`period _ tab:select from .timer.run.i.lazy[dict] where execute<dict[`expire];
      RUNS insert`expire`period _ .timer.run.i.new last tab / check that we are <dict[`expire] ??
    ];
    [
      RUNS insert`expire`period _ .timer.run.i.new dict
    ]
  ]
 };

/ Generate `pending run based on given run
.timer.run.i.new:{[dict]
  schedule:dict[`execute]+dict[`period];
  dict,(!/)flip(
    (`id     ; .timer.newRunId[] );
    (`execute; schedule   );
    (`actual ; 0Np        );
    (`status ; `pending   );
    (`lzp    ; .z.p       ))
 };

/ Generate `skipped runs based on given run
.timer.run.i.lazy:{[dict]
  n:count schedule:dict[`execute]+dict[`period]*1+til floor(.z.p-dict[`execute])%dict[`period];
  ids:(n-1).timer.newRunId\.timer.newRunId[];
  (n#enlist dict)^flip(!/)flip(
    (`id     ; ids      );
    (`execute; schedule );
    (`actual ; 0Np      );
    (`status ; `skipped );
    (`lzp    ; .z.p     ))
 };

/ Lowers `retries count
.timer.run.i.retry:{[dict]
  if[JOBS[dict`job][`retries]<exec count i from RUNS where id=dict[`id];
    RUNS insert dict,`execute`status!(dict[`execute]+dict[`setback];`pending);
  ];
 };

/ Returns schedule times since execution time
/run.i.next:{[execute;period] execute+period*1+til ceiling(.z.p-execute)%period};
/run.i.next:{[execute;period] execute+period*1+til floor(.z.p-execute)%period};
/run.i.next:{[execute;period] execute+period};

/ Delete run
.timer.run.i.delete:{[id]
  index:exec i from RUNS where job in sJob, status in`pending`onhold;
  update status:`deleted from RUNS where i in index;
  PENDING set get[PENDING] except index;
 };

/ Mark run after execution
.timer.run.i.done:{[jRun;s] update status:s from RUNS where id=jRun;};
.timer.run.i.success:.timer.run.i.done[;`completed];
.timer.run.i.failure:.timer.run.i.done[;`failed];


// Helpers
/.dk.timer.job.new:`create`expire`status`function`argument`expectsTime`lazy`period`retries`setback;


/
IDEA dict should be oTimer
TODO turn INTERVAL into CFG_INTERNAL and use local INTERNAL
TODO `u/`s on key column
TODO implement new generator column, that would call job.generate[] with
     few arguments from current job, that would be used to generate next
     job run. Intended use for jobs needing new set of arguments every run.
TODO runLambda / store lambdas somewhere and refer to them in RUNS
TODO implement retry logic
TODO figure out behaviour for job updates mid-schedule
TODO add custom schedule rule to jobs
TODO audit of operations and possibility to persist it
TODO add calendar to jobs
/
NOTE maybe we could store indices of future runs in .bb and index them into RUNS?
     would improve speed greatly! indexing is O(1), select is O(n)
NOTE would optimisating RUNS for large number of records be worth it? `p on jobId, status maybe?


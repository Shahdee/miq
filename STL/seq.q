// ==================== \\
//  Sequence generator  \\
// ==================== \\
/ Generate an atom / list of any given q/miQ type

\d .seq

// ===========
//  Constants
// ===========
SELF  :system"d";
BB    :.miq.bb.new[SELF];
INIT  :(` sv BB,`init) set 0b;

/ Config
CFG.EXPLICIT:.miq.cfg.explicit[SELF];
CFG.DEFAULT :` sv SELF,`config`default;
CFG.APPLIED :.miq.bb.new[` sv SELF,`config];
CFG.CONSTANT:` sv SELF,`C;

// ========
//  Config
// ========
.miq.fn.copy[`.seq.config.set     ;.miq.cfg.set[CFG.EXPLICIT;CFG.DEFAULT]];
.miq.fn.copy[`.seq.config.assemble;.miq.cfg.assemble[CFG.EXPLICIT;CFG.DEFAULT]];
.miq.fn.copy[`.seq.config.get     ;.miq.cfg.get[CFG.APPLIED]];

.miq.cfg.default[CFG.DEFAULT].'(
  (`null   ;0b)     / Include null values in generation output
 ;(`unique ;0b)     / Store generated values to prevent returning the same values twice
 );

.seq.config.apply:{[]
  .miq.cfg.apply[CFG.CONSTANT;CFG.APPLIED;.seq.config.assemble]
    each `null`unique;
 };

// =========
//  General
// =========
.seq.init:{[fnConfigLoader]
  .log.info "Initialising: ",-3!SELF;

  .miq.cfg.loader[fnConfigLoader;last` vs SELF];
  .seq.config.apply[];

  INIT set 1b;
  .log.info "Initialisation completed: ",-3!SELF;
 };

/.seq.error.range:{[x] if[not .is.whole[x]&0<x; '.log.error "Not a valid range: ",-3!x]};
.seq.error.zeroplus:{[x] if[not .is.whole[x]&0<=x; '.log.error "Not 0 or a positive number: ",-3!x]};

.seq.seed:{[] .seq.z.seed[]};

/ Default domain resolution
.seq.domain:{[t]
  .is.error.any[`q.type`miq.type;t];

  miqt:$[.is.q.type[t]; .is.q2miq[t]; t];
  if[.is.list[miqt]; '.log.error "Ambiguous translation to miQ type: ",-3!`qtype`miqtype!(t;miqt)];

  .seq.i.domain $[.is.miq.atom[miqt]; .is.neg[miqt]; miqt]
 };

.seq.i.domain:(!). flip(
/ q types
  (`bl  ;0b                )
 ;(`gl  ;0Ng               )
 ;(`xl  ;0x0               )
 ;(`hl  ;0h                )
 ;(`il  ;0i                )
 ;(`jl  ;0j                )
 ;(`el  ;1e                )
 ;(`fl  ;1f                )
 ;(`cl  ;" "               )
 ;(`qsl ;`4                )
 ;(`pl  ;0Wp               )
 ;(`ml  ;1   +"m"$0Wp-1    )   / 2292.05m
 ;(`dl  ;1   +"d"$0Wp-1    )   / 2292.04.11
 ;(`zl  ;1e-8+"z"$0Wp-1    )   / 2292.04.10T23:47:16.855
 ;(`nl  ;0Wn               )
 ;(`ul  ;1   +99:59        )
 ;(`vl  ;1   +99:59:59     )
 ;(`tl  ;1   +99:59:59.999 )
/ miQ types
 ;(`sl  ;`4                )
 ;(`nsl ;`4                )
 ;(`fsl ;`4                )
 ;(`csl ;`4                )
  );

// Random single (rand)
.seq.rand:{[t;range] first .seq.roll[t;1;range]};

// Random repeating (roll)
.seq.roll:{[t;len;range]
  .is.error.any[`q.type`miq.type;t];
  .seq.error.zeroplus[len];

  len?$[.is.none[range]; .seq.domain[t]; range]
 };

// Random unique (deal)
.seq.deal:{[t;len;range]
  .is.error.any[`q.type`miq.type;t];
  .seq.error.zeroplus[len];

  r:$[.is.none[range]; .seq.domain[t]; range];
  if[t in (`g;`gl;2h;-2h); :neg[len]?0Ng];
  if[t in (`j;`jl;7h;-7h); :neg[len]?r];

  l:len;
  return:();
  while[len>count return:distinct return,.seq.roll[t;l;r];
    l:0|len-`long$count[return]*.75;
  ];
  len#return
 };

/`op`fp`fn`qc`qi`qd`qk`qt

// =======
//  Hooks
// =======
.seq.z.dd.seed:.miq.u.seed;

if[not .miq.hook.is.defined`.seq.z.seed; .miq.hook.expunge`.seq.z.seed];


/
TODO .seq.next[] ? a->b, 1->2
TODO qt/qk generation
TODO fn/fp/op generation
IDEA permute?? 0N?(1;"a";2;"c";"D")
TODO allow configuration of custom ranges!
     either (from;to) or fully enumerated (from .. to)
TODO make .seq.deal[] safe to call, i.e. when range is not big enough to satisfy length


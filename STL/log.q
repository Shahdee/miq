// ========= \\
//  Logging  \\
// ========= \\

\d .log

// ===========
//  Constants
// ===========
SELF:system"d";
BB  :.miq.bb.new[SELF];
INIT:(` sv BB,`init) set 0b;
DATE:` sv BB,`date;             / Date used for messaging
FILE:` sv BB,`file;             / File holding log output

SO.LOG   :` sv BB,`so`log;      / Dynamic load projection
SO.LOADED:` sv BB,`so`loaded;   / True when C code is loaded

/ Config
CFG.EXPLICIT:.miq.cfg.explicit[SELF];
CFG.DEFAULT :` sv SELF,`config`default;
CFG.APPLIED :.miq.bb.new[` sv SELF,`config];
CFG.CONSTANT:` sv SELF,`C;

/ Table template
/TT.RULES:flip`tier`instance`clan`role`name`level`mode`threshold!"SSSSSSSJ"$\:();

// ========
//  Config
// ========
.miq.fn.copy[`.log.config.set     ;.miq.cfg.set[CFG.EXPLICIT;CFG.DEFAULT]];
.miq.fn.copy[`.log.config.assemble;.miq.cfg.assemble[CFG.EXPLICIT;CFG.DEFAULT]];
.miq.fn.copy[`.log.config.get     ;.miq.cfg.get[CFG.APPLIED]];

/ IDEA redirect unknown log levels to .log.disabled?
/      will there be unknown log levels within `stl module family at all?
/ IDEA configurable?
.log.config.i.handler:{[sLevel]
  handler:` sv `stl.log,sLevel;
  implementer:` sv SELF,sLevel;

  .miq.handler.define[1b;handler;implementer;1;upper[string sLevel]," level log output; input:cl; output:cl"];
 };

.miq.cfg.default[CFG.DEFAULT].'(
  / C library
  (`so.class          ;`so                               )
 ;(`so.name           ;`log                              )
  / Logging configuration
 ;(`level             ;`info                             )
 ;(`levels            ;`debug`show`info`warn`error`fatal )
 ;(`STDOUT            ;-1                                )
 ;(`STDERR            ;-2                                )
 ;(`delimiter         ;"|"                               )
  / Caller
 ;(`caller.enable     ;1b                                )
 ;(`caller.skip       ;(".q.*";"*.is.*")                 )
  / Log file name & location
 ;(`file.directory    ;`:.                               )
 ;(`file.name         ;`                                 )
 ;(`file.extension    ;`log                              )
 ;(`file.mode         ;0N                                )
 ;(`dir.autocreate    ;0b                                )
  / Colour
 ;(`colour.default    ;1b                                )
  / Log rotation
 ;(`rotate.enable     ;0b                                )
 ;(`rotate.mode       ;`day                              )
 ;(`rotate.modes      ;`none`day`line`size               )
 ;(`rotate.threshold  ;1                                 )
 ;(`rotate.method     ;`inode                            )
 ;(`rotate.methods    ;`inode`dup2                       )
/ ;(`rules             ;TT.RULES                          )
  );

.log.config.apply:{[]
  .miq.cfg.apply[CFG.CONSTANT;CFG.APPLIED;.log.config.assemble]
    each `so.class`so.name`level`levels`STDOUT`STDERR`delimiter`caller.enable`caller.skip
   ,`file.directory`file.extension`file.mode`dir.autocreate`colour.default
   ,`rotate.enable`rotate.mode`rotate.modes`rotate.threshold`rotate.method`rotate.methods
   ,enlist(`file.name;{$[`~x; .log.z.generate.name[]; x]});

  .log.set.date[];      / Initialises DATE
  FILE set .miq.fd.stdout[];
  .log.colour.init[C.COLOUR.DEFAULT[]];

  / Get my C code
  {[x]
    if[get SO.LOADED set not first x; .log.so.bind get SO.LOG set last x]
  } .log.h.trap.apply[.log.h.module.load .;get'[C.SO`CLASS`NAME]];

  if[not .miq.fs.is.dir[C.FILE.DIRECTORY[]];
    if[C.DIR.AUTOCREATE[]; .miq.fs.dir.new .` vs C.FILE.DIRECTORY[]];
  ];

  if[not C.ROTATE.ENABLE[]; C.ROTATE.MODE set `none];
  .log.rotate.init[];

  / Populate default definition of log level hook to identity if it does not exist
  {[x]
    dd:.miq.hook.default ` sv SELF,`z,x;
    if[not .miq.ns.is.present[dd]; dd set (::)]
  } each 1_key `.log.level;

  .log.config.local[];
  .log.set.level[C.LEVEL[]];
  .log.config.i.handler'[C.LEVELS[]];

  $[C.CALLER.ENABLE[]; .miq.hook.primary[`.log.z.caller;`.log.caller.hook;.log.caller.hook]; .miq.hook.expunge[`.log.z.caller]];
 };

.log.config.local:{[]
  / Severity strings (padded, in the shape of "[LEVEL]")
  L.SEVERITY ::{x!(2+max count each l)$'"[",'(upper l:string x),'"]"}[C.LEVELS[]];
  L.THRESHOLD::C.ROTATE.THRESHOLD[];  / Threshold to determine if .log.rotate[] needed
  L.COUNTER  ::0;                     / Iterator to keep track of log file usage
  L.DELIMITER::C.DELIMITER[];         / Delimiter used in log messages
  L.DATE     ::DATE[];                / Date used to check for new day
  L.STR_DATE ::string[L.DATE];        / Date used in log message
  / Text is represented as byte vector, so the header is 14 bytes. This value
  / will be added to another serialised message, so substracting 2*14
  / https://code.kx.com/v2/basics/ipc/#byte-vector
  L.FIXED_SIZE::-28+-22!.log.i.print[(::);C.LEVEL[];""];

  / Caller depth constants
  depth:.miq.hook.depth[`.log.z.caller;.log.z.print;(::;`info;"")];
  levels:(C.LEVELS[]?C.LEVEL[])_C.LEVELS[];
  L.CALLER.DEPTH::levels!depth+2+(')[count;.miq.hook.definitions]'[` sv'SELF,'`z,/:levels];
  L.CALLER.SKIP ::C.CALLER.SKIP[];
 };

// ==========
//  Messages
// ==========
.log.i.messages:{[]
  .is.generate[`warn`error;;1;]./:\:.[;(::;0);` sv SELF,`is,]enlist(
    (`level ;"Invalid log level: ")
  );

  .is.generate[`warn`error;;2;]./:\:.[;(::;0);` sv SELF,`is,](
    (`rotate.mode   ;"Invalid log rotate mode: "  )
   ;(`rotate.method ;"Invalid log rotate method: ")
  );
 };

// =========
//  General
// =========
.log.init:{[fnConfigLoader]
  .miq.handler.import[;` sv SELF,`h]'[`stl.log`stl.trap`miq.module];
  .log.i.messages[];

  .log.h.log.info "Initialising: ",-3!SELF;

  .miq.cfg.loader[fnConfigLoader;last` vs SELF];
  .log.config.apply[];

  / Rotate log file when not interactive and not logging out into configured file
  if[any (`:/dev/null;1b) = .miq.fd.info[0i][`target`stale];
    if[not FILE[]~.log.z.generate.file[]; .log.z.rotate[]];
  ];

  INIT set 1b;
  .log.h.log.info "Initialisation completed: ",-3!SELF;
 };

.log.caller.hook:{[sSeverity] "<",@[.miq.u.nCallerSkip[;L.CALLER.SKIP]; L.CALLER.DEPTH[sSeverity]; {.log.h.log.warn ".log.z.caller failed: ",x; "~~~FAIL~~~"}],">"};

.log.so.bind:{[fpSo]
  .log.so.i.redirect:fpSo(`redirect;2);   / .log.i.redirect[cl|s|fs File;jMode]
 };

/ Accessors
/ IDEA maybe replace all accessors with .log.status[]? It could include history of all files written to and so on...
.log.date  :{[] DATE[]};
/.log.rotate:{[] {lower[key x]!get'[get x]} 1_ROTATE[]};
/.log.file  :{[] {lower[key x]!get'[get x]} 1_FILE[]};
/.log.rules :{[] RULES[]};

/ Checks
.log.is.level:{[x] .is.error.s[x]; x in C.LEVELS[]};
.log.is.rotate.mode:{[x] .is.error.s[x]; x in C.ROTATE.MODES[]};
.log.is.rotate.method:{[x] .is.error.s[x]; x in C.ROTATE.METHODS[]};

/ Runtime configuration
/ Set a new date
.log.set.date:{[dDate]
  date:$[.is.none[dDate]; .miq.z.d(); [.is.error.d[dDate]; dDate]];
  DATE set L.DATE::date;
  L.STR_DATE::string[L.DATE];
 };

/ Set log level and define log output functions
.log.set.level:{[sLevel]
  .is.error.s[sLevel];
  .log.is.error.level[sLevel];

  .log.h.log.info "Setting log level mode to: ",-3!sLevel;
  C.LEVEL set sLevel;

  index  :C.LEVELS[]?sLevel;      / Index of required level
  disable:index # C.LEVELS[];     / Disable preceeding
  enable :index _ C.LEVELS[];     / Enable the rest

  / TODO implement hook config to define if hook needs primary or not
  /      that will allow you to keep definitions and put dd into proper namespace
  / log level hooks are special and don't live in z namespace
  lvl:{[enable;level]
    hook:` sv SELF,`z,level; function:` sv SELF,`level,level;
    $[enable; .miq.hook.primary[hook; function; get function]; .miq.hook.expunge[hook]];

    accessor:` sv SELF,level;
    function:get "{[x] ",string[hook]," (x)}";
    .miq.fn.copy[accessor;function];

    / TODO decide whether this is a good idea; you are relying on .miq.log.i.* to not spam the warning during every startup sequence...
    /if[.miq.ns.is.present accessor;
    /  if[not function~get accessor;
    /    if[not any get[accessor]~'get each` sv'`.miq.log.i,'`on`off,'level;
    /      .log.h.log.warn "Log accessor overwritten an unexpected function: ",-3!accessor
    /    ]
    /  ]
    /];
  };

  lvl[0b] each disable;
  lvl[1b] each enable;
  sLevel
 };

.log.set.rotate.mode:{[sMode;jThreshold]
  .is.error[`s`j]@'(sMode;jThreshold);
  .log.is.error.rotate.mode[sMode];

  if[not sMode~C.ROTATE.MODE[];
    L.COUNTER::0;
    / TODO instead of reset, run a calculation on existing file
    .log.h.log.info "Log rotate counter reset";
  ];

  .log.h.log.info "Setting log rotate mode to: ",-3!(sMode;jThreshold);
  C.ROTATE.MODE      set sMode;
  C.ROTATE.THRESHOLD set L.THRESHOLD::jThreshold;

  .miq.hook.primary[`.log.z.print; fn; get[fn:` sv SELF,`rotate`mode,sMode,`print].];
  .miq.hook.primary[`.log.z.tally; fn; get[fn:` sv SELF,`rotate`mode,sMode,`tally]];
  sMode
 };

.log.set.rotate.threshold:{[jThreshold]
  .is.error.j[jThreshold];
  .log.h.log.info "Log rotate threshold changed: ",-3!jThreshold;
  get C.ROTATE.THRESHOLD set L.THRESHOLD::jThreshold
 };

.log.set.rotate.method:{[sMethod]
  .is.error.s[sMethod];
  .log.is.error.rotate.method[sMethod];

  if[not[SO.LOADED[]] & `dup2~sMethod; '.log.h.log.error "Cannot use log rotate method without C library: ",-3!sMethod];

  .miq.hook.primary[`.log.z.rotate; fn; get fn:` sv SELF,`rotate`method,sMethod];
  sMethod
 };

// =========
//  Logging
// =========
/ Checks if date has advanced
.log.i.checkDate:{[]
  $[0<(.miq.z.d())-L.DATE; [diff:(.miq.z.d())-L.DATE; .log.set.date[.miq.z.d()]; diff]; 0]
 };

/ Prints messages to output
.log.i.print:{[jStream;sSeverity;clMessage]
  jStream@.log.z.message (sSeverity;clMessage)
 };

// Entry functions
.log.level.debug:{.log.z.tally[x]; .log.z.print (C.STDOUT[];`debug;x); x};      / DEBUG - Low level information for tracking problems
.log.level.show: {.log.z.tally[x]; 1 .Q.s x; x};                              / SHOW  - Display objects
.log.level.info: {.log.z.tally[x]; .log.z.print (C.STDOUT[];`info ;x); x};      / INFO  - Essential information
.log.level.warn: {.log.z.tally[x]; .log.z.print (C.STDOUT[];`warn ;x); x};      / WARN  - Something is not right, need to notify
.log.level.error:{.log.z.tally[x]; .log.z.print (C.STDERR[];`error;x); x};      / ERROR - Failure, termination of execution should follow
.log.level.fatal:{.log.z.tally[x]; .log.z.print (C.STDERR[];`fatal;x); x};      / FATAL - Used for errors from which we cannot recover

// Colours
.log.colour.init:{[b]
  $[b;
    [
      .miq.hook.register[`.log.z.message;`.log.colour.warn ;.log.colour.warn ];
      .miq.hook.register[`.log.z.message;`.log.colour.error;.log.colour.error]
    ];
    [
      if[.miq.hook.is.label[`.log.z.message;`.log.colour.warn ]; .miq.hook.remove[`.log.z.message;`.log.colour.warn ]];
      if[.miq.hook.is.label[`.log.z.message;`.log.colour.error]; .miq.hook.remove[`.log.z.message;`.log.colour.error]]
    ]
  ]
 };

.log.colour.warn :{[sSeverity;clMessage] if[`warn ~sSeverity; .miq.z.LAST[1]:.miq.colour.fg[`yellow;.miq.z.LAST 1]]}.;
.log.colour.error:{[sSeverity;clMessage] if[`error~sSeverity; .miq.z.LAST[1]:.miq.colour.fg[`red   ;.miq.z.LAST 1]]}.;

// ========
//  Rotate
// ========
.log.rotate.init:{[]
  .log.set.rotate.mode[C.ROTATE.MODE[];C.ROTATE.THRESHOLD[]];
  .log.set.rotate.method[C.ROTATE.METHOD[]];
 };

// Tally and print for rotate modes
/ None mode - disable rotation
.log.rotate.mode.none.tally:{[clMessage] .log.i.checkDate[]};
.log.rotate.mode.none.print:.miq.u.copy .log.i.print;

/ Day mode
.log.rotate.mode.day.tally:{[clMessage]
  if[diff:.log.i.checkDate[]; L.COUNTER+:diff;
    if[not L.THRESHOLD>L.COUNTER; .log.z.rotate[]]
  ]
 };

.log.rotate.mode.day.print:.miq.u.copy .log.i.print;

/ Line mode
.log.rotate.mode.line.tally:{[clMessage]
  .log.i.checkDate[];
  if[not L.THRESHOLD>L.COUNTER+:1; .log.z.rotate[]]
 };

.log.rotate.mode.line.print:.miq.u.copy .log.i.print;

/ Size mode
.log.rotate.mode.size.tally:{[clMessage]
  .log.i.checkDate[];
  if[not L.THRESHOLD>L.COUNTER+:L.FIXED_SIZE+-22!clMessage; .log.z.rotate[]]
 };

.log.rotate.mode.size.print:.miq.u.copy .log.i.print;

// Log file rotation
/ TODO Generate below rotate functions using code insert capabilities
/ Requires script to be run with >> redirect
.log.rotate.method.inode:{[fsFile]
  new:$[.is.none[fsFile]; .log.z.generate.file[]; [.is.error.fs[fsFile]; fsFile]];

  .log.h.log.info "Rotating logfile to: ",-3!new;
  if[new~FILE[];
    .log.h.log.error "Log rotate aborted! New log file is identical to current one: ",-3!new;
    :()
  ];

  old:FILE[];
  system"mv ",(1_string old)," ",(1_string new);
  system"cp ",(1_string new)," ",(1_string old);
  system"echo \"\">",1_string new;

  .log.h.log.info "Logfile Rotated from: ",-3!FILE[];
  get FILE set new
 };

/ Redirect output to a new file by duplicating file descriptor
/ NOTE will be deprecated in kdb 4.0 where \1 and \2 can be redirected to the same file
.log.rotate.method.dup2:{[fsFile]
  new:$[.is.none[fsFile]; .log.z.generate.file[]; [.is.error.fs[fsFile]; fsFile]];

  .log.h.log.info "Rotating logfile to: ",-3!new;
  if[new~FILE[];
    .log.h.log.error "Log rotate aborted! New log file is identical to current one: ",-3!new;
    :()
  ];

  .log.so.i.redirect[fsNew;C.FILE.MODE[]];
  .log.h.log.info "Logfile rotated from: ",-3!FILE[];
  get FILE set new
 };

// =======
//  Rules
// =======
/ TODO Make into hooks!!!
/ Initiate logging rules and apply
/.log.rule.init:{[]
/  .OS.PROCESS xkey RULES;
/  .log.h.log.info "Initiating logging rules";
/  .log.h.log.show RULES[];
/  .log.rule.apply[RULES[]];
/ };

/ Apply a logging rule matching our process definition
/.log.rule.apply:{[ktRules]
/  .is.error.kt[ktRules];
/  /rule:@[ktRules;] .clan.bestMatch[.clan.self;key ktRules];
/  rules:select from ktRules where .clan.match[.clan.self] each key ktRules;
/  match:.clan.compare[.clan.self] each key rules;
/  rule :first 0!select from rules where max[match]=match;

/  if[all null each rule; .log.h.log.info "No matching rule found"; :()];
/  .log.set.mode . rule`mode`threshold;
/  .log.set.level  rule`level;

/  .log.h.log.info "New logging rule applied: ",-3!`level`mode`threshold#rule;
/ };

// =======
//  Hooks
// =======
/ Log name and file generators
.log.z.dd.generate.name:{[x]
  opts:$[.is.qd[x]; x; `$raze each .Q.opt[.z.x]];

  $[`logname in key opts; opts`logname;
    `name in key opts   ; opts`name;
    `~file:string .z.f  ; `$6?.Q.a;
    "/" in file         ; `$last cut[;file]1+where"/"=file;
                          `$file
  ]
 };

.log.z.dd.generate.file:{[x]
  file:string $[.is.s x; x; .is.error.none x; C.FILE.NAME[]];
  date:L.STR_DATE[0 1 2 3 5 6 8 9];
  time:string`second$.miq.z.t();
  name:`$file,"_",date,$[`day~C.ROTATE.MODE[];"";"_",time];

  ` sv C.FILE.DIRECTORY[],` sv name,C.FILE.EXTENSION[]
 };

/ User friendly hook defaults (:
.log.z.dd.print :.miq.u.copy .[.log.i.print];
.log.z.dd.tally :{[x]     '"Initiate `.log module first!"};
.log.z.dd.rotate:{[x]     '"Initiate `.log module first!"};
.log.z.dd.caller:{""};

.log.z.dd.message:{[sSeverity;clMessage]
  L.DELIMITER sv (L.STR_DATE;string[.miq.z.t()];L.SEVERITY[sSeverity];.log.z.caller[sSeverity];clMessage)
 }.;

/ Default hook functionality
if[not .miq.hook.is.defined[`.log.z.generate.name]; .miq.hook.expunge[`.log.z.generate.name]];
if[not .miq.hook.is.defined[`.log.z.generate.file]; .miq.hook.expunge[`.log.z.generate.file]];
if[not .miq.hook.is.defined[`.log.z.tally        ]; .miq.hook.expunge[`.log.z.tally        ]];
if[not .miq.hook.is.defined[`.log.z.print        ]; .miq.hook.expunge[`.log.z.print        ]];
if[not .miq.hook.is.defined[`.log.z.rotate       ]; .miq.hook.expunge[`.log.z.rotate       ]];
if[not .miq.hook.is.defined[`.log.z.caller       ]; .miq.hook.expunge[`.log.z.caller       ]];
if[not .miq.hook.is.defined[`.log.z.message      ]; .miq.hook.expunge[`.log.z.message      ]];


/
TODO Recalculate threshold after mode switch
TODO .cfg.log.timezone and look for it in cfg and opts?
IDEA .cfg.log.print configuration of output format
TODO Introduce colours to logger
TODO Log zipping to be done by slaves on master process
TODO Have debuglevel remove all .log.LEVEL calls from code to speed it up!!
IDEA Add support for logging STDOUT and STDERR into separate files?


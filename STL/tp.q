// ============== \\
//  Ticker plant  \\
// ============== \\

\d .tp

// ===========
//  Constants
// ===========
SELF    :system"d";
BB      :.miq.bb.new[SELF];
INIT    :(` sv BB,`init) set 0b;
MODE    :` sv BB,`mode;           / Mode of data dissemination
INTERVAL:` sv BB,`interval;       / Publish interval in `batched mode

DATE:` sv BB,`date;               / What do you think this is?
EOD :` sv BB,`eod;                / When to trigger EOD

MODES  :(` sv BB,`modes) set `immediate`batched`custom;    / Implemented TP modes
HANDLES:(1#`)!enlist 0#0i;        / Subscription dict of sTable!chlHandle
TABLES :0#`;                      / Tables available for subscription

/ Config
CFG.EXPLICIT:.miq.cfg.explicit[SELF];
CFG.DEFAULT :` sv SELF,`config`default;
CFG.APPLIED :.miq.bb.new[` sv SELF,`config];
CFG.CONSTANT:` sv SELF,`C;

// ========
//  Config
// ========
.miq.fn.copy[`.tp.config.set     ;.miq.cfg.set[CFG.EXPLICIT;CFG.DEFAULT]];
.miq.fn.copy[`.tp.config.assemble;.miq.cfg.assemble[CFG.EXPLICIT;CFG.DEFAULT]];
.miq.fn.copy[`.tp.config.get     ;.miq.cfg.get[CFG.APPLIED]];

.miq.cfg.default[CFG.DEFAULT].'(
  (`mode           ;`immediate         )
 ;(`interval       ;100                )
 ;(`date           ;.miq.z.d           )
  );

.tp.config.apply:{[]
  .miq.cfg.apply[CFG.CONSTANT;CFG.APPLIED;.tp.config.assemble]
    each `mode`interval`date;

  .tp.set.date[C.DATE[]];
  .tp.set.mode[C.MODE[]];

  if[`batched~C.MODE[]; .tp.set.interval[C.INTERVAL[]]];
  .miq.hook.expunge each` sv'`.tp.z,'`sub`unsub`add`del`eod`eodcheck;

  .tp.config.local[];
 };

.tp.config.local:{[]
  L.DATE     ::C.DATE[];
  L.DATE_FUNC::$[.is.d fn:.tp.config.get[`date]; {.z.d}; fn];
 };

// =========
//  General
// =========
.tp.init:{[fnConfigLoader]
  .log.info "Initialising: ",-3!SELF;

  .miq.cfg.loader[fnConfigLoader;last` vs SELF];
  .tp.config.apply[];

  INIT set 1b;
  .log.info "Initialisation completed: ",-3!SELF;
 };

.tp.start:{[]
  /.tp.batched.pub[]
 };

.tp.stop:{[]

 };

// Settings
.tp.set.mode:{[sMode]
  .is.error.s[sMode];
  if[not sMode in MODES[]; '.log.error "Invalid publish mode: ",-3!sMode];

  .log.info "Setting ticker plant mode to: ",-3!sMode;
  if[sMode~`custom;
    .log.info "Using custom hooks: ",-3!`.tp.z.upd`.tp.z.pub;
    .miq.hook.is.error.defined each`.tp.z.upd`.tp.z.pub;
    :C.MODE set sMode
  ];

  .miq.hook.primary[`.tp.z.upd; n; get[n:` sv SELF,sMode,`upd].];
  .miq.hook.primary[`.tp.z.pub; n; $[`immediate~sMode;.;::] get[n:` sv SELF,sMode,`pub]];
  .tp.config.set[`mode;sMode];
  C.MODE set sMode
 };

/@ Sets interval for `batched mode
.tp.set.interval:{[n]
  .is.error.whole[n];
  if[n<0; '.log.error "Invalid interval number: ",-3!n];
  .tp.config.set[`interval;n];
  system"t ",string get C.INTERVAL set n
 };

.tp.set.date:{[d]
  .is.error.any[`d`fn`fp;d];
  DATE set L.DATE::$[.is.d[d]; d; d[]]
 };

// Accessors
.tp.date   :{[] DATE[]};
.tp.mode   :{[] C.MODE[]};
.tp.modes  :{[] MODES[]};
.tp.handles:{[] HANDLES[]};
.tp.tables :{[] TABLES[]};

// =======
//  Modes
// =======
/ immediate (pass-through)
.tp.immediate.upd:{[sTable;data]
  $[sTable in TABLES; .tp.z.pub (sTable;data); .log.error "Invalid table ",-3!sTable];
 };

.tp.immediate.pub:{[sTable;data]
  -25!(HANDLES[sTable];(`upd;sTable;data));
 };

/ batched (bulk publish)
.tp.batched.upd:{[sTable;data]
  $[sTable in TABLES; sTable insert data; .log.error "Invalid table ",-3!sTable];
 };

.tp.batched.pub:{[sTable]
  if[count data:get sTable;
    -25!(HANDLES[sTable];(`upd;sTable;data));
    @[`.;sTable;0#]
  ]
 };

// ================
//  Table Handling
// ================
.tp.table.add:{[sTable;schema]
  .is.error.s[sTable];
  TABLES::distinct TABLES,sTable
  / TODO announce new tables
 };

.tp.table.del:{[sTable]
  .is.error.s[sTable];
  if[any .is.ch each HANDLES[sTable];
    '.log.error "Cannot delete table due to active subscriptions:",-3!sTable
  ];
  TABLES _:TABLES?sTable
 };

.tp.table.verify:{[sTable;data]
  all`date`time`sym in key flip data
 };

.tp.table.error.verify:{[sTable;data]
  if[not all`date`time`sym in key flip data; '.log.error "Data missing columns: ",-3!`date`time`sym]
 };

// =======
//  Hooks
// =======
/ Cleanup
.tp.pc: {[ch] .tp.z.unsub (`;ch)};

/ Subscription handling
.tp.z.dd.sub:{[sTable;ch]
  .is.error[`s`ch]@'(sTable;ch);
  if[not sTable in `,TABLES[]; '.log.error "Unknown table ",-3!sTable];
  $[`~sTable; :.tp.z.sub each flip(TABLES[];ch); .tp.z.add (sTable;ch)];
 };

.tp.z.dd.unsub:{[sTable;ch]
  .is.error[`s`ch]@'(sTable;ch);
  if[not sTable in `,TABLES; '.log.error "Unknown table ",-3!sTable];
  $[`~table; :.tp.z.unsub each flip(TABLES[];ch); .tp.z.del (sTable;ch)];
 };

.tp.z.dd.add:{[sTable;ch] HANDLES[sTable]:distinct HANDLES[sTable],ch; sTable};
.tp.z.dd.del:{[sTable;ch] HANDLES[sTable]_:HANDLES[sTable]?ch; sTable};

/ EOD signal
/eod:{[date] -25!(union/[HANDLES];(`eod;date))};
.tp.z.dd.eod:{[date] -25!(distinct raze HANDLES;(`eod;date))};

.tp.z.dd.eodcheck:{[]
  if[L.DATE_FUNC[]>L.DATE;
    .tp.set.date[d:L.DATE_FUNC[]];
    .tp.z.eod[d]
  ]
 };

// Expose sub and unsub
`..sub   set {[x] .tp.z.sub   (x;.z.w)};
`..unsub set {[x] .tp.z.unsub (x;.z.w)};

/
TODO Table insert error trapping and redirecting to failed update table
TODO Table addition and removal
TODO Have separate TP for feeds with time and without
TODO Hybrid mode? Some tables `immediate, some `batched ...
TODO Symbol subscription?
TODO Integrate transaction logging from tl.q
TODO Make sure TP sends only data with today's timestamp
TODO Optimise eod check ~ supply function that provides a timestamp for given TZ and calculate
     when a next roll should happen in UTC; .z.p>EOD is quicker
TODO Protected evaluation of function input in .tp.set.date[]


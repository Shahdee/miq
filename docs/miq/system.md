System
===
---
Routines interacting with shell the process is running in.

**Errors**

| Message                               | Explanation                                                           |
| ------------------------------------- | --------------------------------------------------------------------- |
| `No such system command`              | Command unavailable in the current shell environment                  |
| `No such environment variable`        | Environment variable not defined in the current shell environment     |
| `Process not attached to a terminal`  | STDIN is not attached to PTS                                          |

**Examples**
```
q).miq.system.require[`cmd;`ifconfig]
`ifconfig
q).miq.system.require[`cmd;`ipconfig]
2021.04.12 | 21:00:06.314 | [ERROR] | <.miq.system.require> | System command unavailable: `ipconfig
'System command unavailable: `ipconfig

q).miq.system.is.variable'[`QHOME`QINIT]
10b
q).miq.system.is.terminal[]
1b

q).miq.system.env[]
CONDA_SHLVL   | ,"1"
CONDA_EXE     | "/opt/miniconda/bin/conda"
_             | "/usr/bin/env"
...
```

## .miq.system.is.command
| parameter | Type | Description            |
| --------- | ---- | ---------------------- |
| `s`       | s    | System command name    |

Return `1b` if `s` is an available system command, `0b` otherwise.

## .miq.system.is.variable
| parameter | Type | Description                |
| --------- | ---- | -------------------------- |
| `s`       | s    | Environment variable name  |

Return `1b` if `s` is an available environment variable, `0b` otherwise.

## .miq.system.is.variable
Return `1b` if STDIN is attached to a PTS device under `/dev/pts`, `0b` otherwise.

## .miq.system.require
| parameter     | Type | Description            |\*| Interface          | Purpose                   |
| ------------- | ---- | ---------------------- |--| ------------------ | ------------------------- |
| `sResource`   | s    | Resource type          |\*| `` `s ``           | Unnamed resources         |
| `name`        | -    | Resource name          |\*| `` `s` ``          | Named resourced           |

Check that resource `name` is available. Return `name` if it is, throw a signal if it isn't. `sResource` can be one of:
* `command|cmd` - check for presence of system command
* `variable|var` - check for presence of environment variable
* `terminal|tty|pts` - check that process is attached to a terminal

## .miq.system.env
Return a dictionary of environment variables and their values.


Module
======
---
miQ uses uniform interface to enable loading of `q`/`k` code and binding to `so` libraries. Modules are divided into classes based on their source and purpose. Below classes are supported out of the box:

| Class      | Description                                                            |
| ---------- | ---------------------------------------------------------------------- |
| module     | Holds `q` and `k` code to extend code base with additional functions   |
| config     | Holds configuration for (not only) libraries                           |
| definition | Holds miQ object definitions used by some libraries                    |
| role       | Holds algorithm to perform a specific function                         |
| so         | Holds C shared objects for dynamic linking                             |

Every class is associated with a filesystem path(s) from where modules will be sourced. The path will either come from an environment variable, or is directly set by the user. See the configuration section for details.

On initialisation, miQ will (1) check for special internal LOADED variables and (2) inspect environment variables to determine if any of the recofnised variables have been set, and on success appends respective classes in `module.classes` cofiguration. Subsequently, all module class paths are explored for matching files and result is used to populate the `LIBRARY` table.

**Errors**

| Message                                 | Explanation                                                             |
| --------------------------------------- | ----------------------------------------------------------------------- |
| `No matching module`                    | `sName` failed to resolve and no module within `LIBRARY` matches        |
| `Module name ambiguous`                 | `sName` has resolved to more than a single entry within `LIBRARY` table |
| `Not a module class`                    | `sClass` has not been configured or initialised                         |
| `Class extension not configured`        | *CLASS* extension pattern has not been configured                       |
| `Class directory/envvar not configured` | Neither of envvar and directory has been configured for *CLASS*         |
| `Class directory empty`                 | *CLASS* directory has not been supplied                                 |
| `Class directory invalid`               | *CLASS* directory is not a directory                                    |

**Examples**
```
q).miq.module.load[`module;`slot]
21:11:18.021 [INFO]   <.miq.module.i.require> Require module: `class`name!`module`slot
21:11:18.023 [INFO]   <.miq.module.i.require> Require module: `class`name!`definition`module
21:11:18.023 [INFO]   <.miq.bb.new> Creating new blackboard: `.bb.slot
21:11:18.024 [INFO]   <.miq.bb.new> Creating new blackboard: `.bb.slot.config
q).miq.module.loaded[`module;`slot]
1b
q).miq.module.reload[`module;`slot]
21:11:53.684 [INFO]   <.miq.module.i.require> Reloading module: `class`name!`module`slot
21:11:53.686 [INFO]   <.miq.module.i.require> Require module: `class`name!`definition`module
21:11:53.686 [INFO]   <.miq.bb.new> Blackboard exists, skipping: `.bb.slot
21:11:53.686 [INFO]   <.miq.bb.new> Blackboard exists, skipping: `.bb.slot.config
q).miq.module.resolve[`config;`slot]
directory                name extension
---------------------------------------
:/work/michal/repo/q/cfg slot q
q)select from .miq.module.library[] where name=`miq
directory                name extension| class      loaded lzp                           request
---------------------------------------| -------------------------------------------------------
:/work/michal/repo/q/miq miq  q        | module    1      2020.05.20D21:11:06.412206000
:/work/michal/repo/q/cfg miq  q        | config     1      2020.05.20D21:11:10.137901000
:/work/michal/repo/q/def miq  q        | definition 0
q).miq.module.path key select from .miq.module.library[] where name=`miq
`:/work/michal/repo/q/miq/miq.q`:/work/michal/repo/q/cfg/miq.q`:/work/michal/repo/q/def/miq.q
```

## .miq.module.classes
| parameter | Type | Description |
| --------- | ---- | ----------- |
| `x`       | none |             |

Return list of initialised module classes.

## .miq.module.is.class
| parameter | Type | Description    |
| --------- | ---- | -------------- |
| `sClass`  | s    | Any class name |

Return `1b` if class exists, otherwise `0b`.

## .miq.module.library
Returns full `LIBRARY` table if no argument give. If an argument is given, it will attempt to index into `LIBRARY` using the argument as key.

## .miq.module.path
| parameter | Type | Description                              |
| --------- | ---- | ---------------------------------------- |
| `qtLib`   | qt   | Table of `key[TT.LIBRARY]` format        |

Composes full qualified file system paths of modules from input table.

## .miq.module.resolve
| parameter | Type  | Description                              |
| --------- | ----- | ---------------------------------------- |
| `sClass`  | s     | Any of initialised classes               |
| `sName`   | s\|sl | A file name with or without an extension |

Identify `sName` module of class `sClass`. `sName` can be either a file name, file name with extension, or a list in the form of `(file name;extension)`. It will explore `sClass` paths in the order they've been defined and will return module(s) of first path that contains any matches.

## .miq.module.load
| parameter | Type  | Description                              |
| --------- | ----- | ---------------------------------------- |
| `sClass`  | s     | Any of initialised classes               |
| `sName`   | s\|sl | A file name with or without an extension |

Use to load `sClass` modules of matching `sName`. In case the class is one of `module`,`config`,`definition` or `role`, it will compose a full qualified path of in the format of `` `:/path/to/file.extension`` and attempts to load it using `system"l "`. In the case of `so` class, it will instead return a dynamic load projection ``2:[`:/path/to/file]``.

When module that has already been loaded is requested again, the request will silently be ignored. Use `.miq.module.reload` to force load the module.

## .miq.module.reload
| parameter | Type  | Description                              |
| --------- | ----- | ---------------------------------------- |
| `sClass`  | s     | Any of initialised classes               |
| `sName`   | s\|sl | A file name with or without an extension |

Same as `.miq.module.load` only that it  will *always* load fresh code and can't be called to load the module in the first place.

## .miq.module.loaded
| parameter | Type  | Description                              |
| --------- | ----- | ---------------------------------------- |
| `sClass`  | s     | Any of initialised classes               |
| `sName`   | s\|sl | A file name with or without an extension |

Returns `1b` when module match is exact and given module is loaded. Returns `0b` if no module matches, module is not loaded or more than a single module matches.


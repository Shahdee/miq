Configuration
=============
---
Config provides module developers with a standard set of routines to manage complexity of module configuration. Default configuration values are recommended to be present in the module code itself, while user configuration is to be kept in a separate file. Using the configuration routines you can assemble the configuration item by overlaying explicit configuration, provided by the user, over default configuration, provided by module developer. Application of this assembled configuration will result in values being persisted on blackboard, together with other run-time items created by the module.

Standard way to store the configuration is under context configured via `root.cfg`. Modules create their own sub-contexts within, named after themsleves.

Standard constants used to store configuration values:

| Constant       | Description                                                                              |
| -------------- | ---------------------------------------------------------------------------------------- |
| `CFG.EXPLICIT` | User configuration values, external to module                                            |
| `CFG.DEFAULT`  | Default configuration values, interned in module's source code                           |
| `CFG.APPLIED`  | Context to store the applied configuration values                                        |
| `CFG.CONSTANT` | Context in which the constants referring to applied configuration item will be created   |

Configurable modules are, by standard, utilising `set`, `assemble`, `get` and `apply` functions within `.<MODULE>.config` context to implement configuration handling.

**Errors**

| Message                  | Explanation                                         |
| ------------------------ | --------------------------------------------------- |
| `No configuration exist` | Given context is not residing under blackboard root |
| `No such configuration`  | Given identifier is not preset on the blackboard    |

**Examples**
```
q).miq.cfg.root[]
`.cfg
q).miq.cfg.explicit[`test]
`.cfg.test
q).miq.cfg.loader[.miq.module.load[`config];`ipc]
21:51:41.997 [INFO]   <.miq.cfg.loader> Executing configuration loader: "`.miq.module.load[,`config]"
21:51:41.997 [INFO]   <.slot.module.load> Require module: `class`name!`config`ipc
21:51:41.997 [INFO]   <.slot.module.load> Loading module: `slot`name`extension!`CFG`ipc`q
q).miq.cfg.set[`.miq.cfg;`.miq.config.default;`tmp.enable;0b]
0b
q).miq.cfg.assemble[`.miq.cfg;`.miq.config.default;`tmp.enable]
0b
q)fnAssemble:.miq.cfg.assemble[`.miq.cfg;`.miq.config.default]
q).miq.cfg.apply[`miq;`.bb.miq.config;fnAssemble;`tmp.enable]
`miq.TMP.ENABLE
q).miq.cfg.get[`.bb.miq.config;`tmp.enable]
0b
```

## .miq.cfg.root
Return configuration root as configured via `root.cfg`.

## .miq.cfg.explicit
| parameter | Type | Description        |
| --------- | ---- | ------------------ |
| `sName`   | s    | Identifier         |

Shorthand to retrieve own configuration context as long as we are adhering to standard. Configuration context is obtained as a concatenation of `root.cfg` and `sName` stripped of initial dot. Unline `.miq.bb.new`, this function does not create an empty context.

## .miq.cfg.loader
| parameter        | Type | Description        |
| ---------------- | ---- | ------------------ |
| `fnConfigLoader` | fn   | Blackboard context |
| `argument`       | -    | Identifier         |

Standard function to streamline configuration loading during module initiation. It accepts function capable of loading code `fnConfigLoader` and a single parameter of any type that is accepted by the loader function.

## .miq.cfg.default
| parameter    | Type | Description                                  |
| ------------ | ---- | -------------------------------------------- |
| `nsDefault`  | ns   | Context storing default configuration values |
| `sName`      | s    | Identifier                                   |
| `content`    | -    | Anything goes                                |

Create `sName` identifier within `nsDefault` context and setting it to `content`. Useful to generate context holding default configuration values.

## .miq.cfg.set
| parameter    | Type | Description                                  |
| ------------ | ---- | -------------------------------------------- |
| `nsExplicit` | ns   | Context storing user configuration values    |
| `nsDefault`  | ns   | Context storing default configuration values |
| `sName`      | s    | Identifier                                   |
| `content`    | -    | Anything goes                                |

Set configuration item identified by `sName` to `content` value within `nsExplicit` configuration context storing user defined configuration values and returns the `content` itself. First part of `sName` identifier is checked against `nsDefault` keys and warns if a match can't be found.

## .miq.cfg.assemble
| parameter    | Type | Description                                  |
| ------------ | ---- | -------------------------------------------- |
| `nsExplicit` | ns   | Context storing user configuration values    |
| `nsDefault`  | ns   | Context storing default configuration values |
| `sName`      | s    | Identifier                                   |

Look up configuration item identified by `sName` and returns it's value. User configuration under `nsExplicit` is checked first, default values under `nsDefault` are checked thereafter. If neither context contains the requested item, signal.

## .miq.cfg.apply
| parameter       | Type      | Description                                   |
| --------------- | --------- | --------------------------------------------- |
| `nsSelf`        | ns        | Context where constant is to be created       |
| `nsApplied`     | ns        | Context to store applied configuration        |
| `fnCfgAssemble` | ns        | Function returning configuration of item `nf` |
| `nf`            | s\|(s;fn) | Name function combination. Function optional  |

Backbone of configuration routines. If `nf` is a simple symbol, it is taken as configuration `identifier` and supplied to `fnCfgAssemble` that returns the configuration value of given item. The following action take place afterwards:
* persist the configuration value under `nsApplied` applied configuration context
* store the address to above item into a constant named after uppercase `identifier` under `nsSelf` context
* return the full constant address

If `nf` is a 2-item list of `(identifier;function)`, then output of `fnCfgAssemble` is then passed into `function` for post-processing before following the above steps.

## .miq.cfg.get
| parameter   | Type | Description                            |
| ----------- | ---- | -------------------------------------- |
| `nsApplied` | ns   | Context to store applied configuration |
| `sName`     | s    | Identifier                             |

Retrieve `sName` configuration item from `nsApplied` configuration context if it exists, signal otherwise.

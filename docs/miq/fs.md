Filesystem
==========
---
Essential routines to inspect and manipulate files and directories on the filesystem.

**Errors**

| Message                       | Explanation                       |
| ----------------------------- | --------------------------------- |
| `No such file or directory`   | `fs` does not exist               |
| `No such file`                | `fs` is not a file                |
| `No such directory`           | `fs` is not a directory           |
| `Not a softlink`              | `fs` is not a symbolic link       |
| `Path is not absolute`        | `fs` is not an absolute path      |
| `Path is not relative`        | `fs` is not a relative path       |
| `Not an ancestor`             | `dir` is not ancestor of `fs`     |
| `Invalid path`                | `fs` not resolvable               |

**Examples**
```
q).miq.fs.is.present each `:name.ext`:test.q`:hdb
011b
q).miq.fs.is.file each `:name.ext`:test.q`:hdb
010b
q).miq.fs.is.dir each `:name.ext`:test.q`:hdb
001b

q).miq.fs.type each `:name.ext`:test.q`:hdb
``file`dir

q).miq.fs.is.ancestor[`:/app/miQ;`:/app/miQ/lib]
1b

q)-3!.miq.fs.file.split each `name.ext`test.q`file
"(`name`ext;`test`q;`file`)"
q).miq.fs.file.join each .miq.fs.file.split each `name.ext`test.q`file
`name.ext`test.q`file
q).miq.fs.file.join `long`file`name`
`long.file.name

q).miq.fs.is.file each `:name.ext`:test.q`:hdb
010b

q).miq.fs.file.new[`:/tmp/dir] each `f`g.k`test.txt
`:/tmp/dir/f`:/tmp/dir/g.k`:/tmp/dir/test.txt
q).miq.fs.file.rm `:/tmp/dir/test.txt
18:57:25.341 [INFO]   <.miq.fs.file.rm> Deleting file: `:/tmp/dir/test.txt
`:/tmp/dir/test.txt

q).miq.fs.is.dir[`:/tmp/dir]
1b
q).miq.fs.dir.new[`:/tmp/dir] each (`etc;`sub`a)
`:/tmp/dir/etc`:/tmp/dir/sub/a

q).miq.fs.file.new[`:/tmp/dir/etc] each `shadow`passwd
`:/tmp/dir/etc/shadow`:/tmp/dir/etc/passwd
q).miq.fs.file.new[`:/tmp/dir/sub] each `1`2.e
`:/tmp/dir/sub/1`:/tmp/dir/sub/2.e
q).miq.fs.file.new[`:/tmp/dir/sub/a] each `a`b
`:/tmp/dir/sub/a/a`:/tmp/dir/sub/a/b
q).miq.fs.dir.rm `:/tmp/dir
19:01:28.522 [INFO]   <.miq.fs.file.rm> Deleting file: `:/tmp/dir/f
19:01:28.522 [INFO]   <.miq.fs.file.rm> Deleting file: `:/tmp/dir/g.k
19:01:28.522 [INFO]   <.miq.fs.file.rm> Deleting file: `:/tmp/dir/etc/passwd
19:01:28.522 [INFO]   <.miq.fs.file.rm> Deleting file: `:/tmp/dir/etc/shadow
19:01:28.522 [INFO]   <.miq.fs.dir.rm> Deleting directory: `:/tmp/dir/etc
19:01:28.523 [INFO]   <.miq.fs.file.rm> Deleting file: `:/tmp/dir/sub/1
19:01:28.523 [INFO]   <.miq.fs.file.rm> Deleting file: `:/tmp/dir/sub/2.e
19:01:28.523 [INFO]   <.miq.fs.file.rm> Deleting file: `:/tmp/dir/sub/a/a
19:01:28.523 [INFO]   <.miq.fs.file.rm> Deleting file: `:/tmp/dir/sub/a/b
19:01:28.523 [INFO]   <.miq.fs.dir.rm> Deleting directory: `:/tmp/dir/sub/a
19:01:28.523 [INFO]   <.miq.fs.dir.rm> Deleting directory: `:/tmp/dir/sub
19:01:28.523 [INFO]   <.miq.fs.dir.rm> Deleting directory: `:/tmp/dir
directory| `:/tmp/dir`:/tmp/dir/etc`:/tmp/dir/sub`:/tmp/dir/sub/a
file     | `:/tmp/dir/f`:/tmp/dir/g.k`:/tmp/dir/etc/passwd`:/tmp/dir/etc/shadow`:/tmp/dir/sub/1`:/tmp/dir/sub/2.e`:/tmp/dir/sub/a/a`:/tmp/dir/sub/a/b
```

## Checks
### .miq.fs.is.present
| parameter | Type | Description           |
| --------- | ---- | --------------------- |
| `fs`      | fs   | File or directory     |

Return `1b` if `fs` physically exist on the filesystem, `0b` otherwise.

### .miq.fs.is.file
| parameter | Type | Description           |
| --------- | ---- | --------------------- |
| `fs`      | fs   | File or directory     |

Return `1b` if `fs` is a file on the filesystem, `0b` otherwise.

### .miq.fs.is.dir
| parameter | Type | Description           |
| --------- | ---- | --------------------- |
| `fs`      | fs   | File or directory     |

Return `1b` if `fs` is a directory on the filesystem, `0b` otherwise.

### .miq.fs.is.softlink
| parameter | Type | Description           |
| --------- | ---- | --------------------- |
| `fs`      | fs   | File or directory     |

Return `1b` if `fs` is a softlink, `0b` otherwise.

> [!WARNING] Broken links will throw a signal saying that the file or directory does not exist. This is a limitation of `key` keyword that dereferences symlinks by default.

### .miq.fs.is.absolute
| parameter | Type | Description           |
| --------- | ---- | --------------------- |
| `fs`      | fs   | File or directory     |

Return `1b` if `fs` is an absolute path, `0b` otherwise.

### .miq.fs.is.relative
| parameter | Type | Description           |
| --------- | ---- | --------------------- |
| `fs`      | fs   | File or directory     |

Return `1b` if `fs` is a relative path, `0b` otherwise.

### .miq.fs.is.ancestor
| parameter | Type | Description            |
| --------- | ---- | ---------------------- |
| `fsDir`   | fs   | Directory              |
| `fs`      | fs   | File or directory      |

Return `1b` if `fsDir` is ancestor to `fs`, `0b` otherwise.

## Casting
### .miq.fs.to.absolute
| parameter | Type    | Description            |
| --------- | ------- | ---------------------- |
| `fs`      | fs\|fsl | File or directory      |

Return the absolute pathname of `fs` using current directory. Able to operate over lists to reduce number of expensive system calls.

### .miq.fs.to.relative
| parameter | Type    | Description            |
| --------- | ------- | ---------------------- |
| `fs`      | fs\|fsl | File or directory      |

Return the pathname of `fs` relative to current directory. Able to operate over lists to reduce number of expensive system calls.

## General
### .miq.fs.type
| parameter | Type | Description        |
| --------- | ---- | ------------------ |
| `fs`      | fs   | File or directory  |

Return null symbol `` ` `` if `fs` does not exist on the filesystem, `file` if it's a file or `dir` if it's a directory.

### .miq.fs.tree
| parameter | Type | Description           |
| --------- | ---- | --------------------- |
| `fs`      | fs   | Directory             |

Return a dictionary of all files and directories under `fs`, inclusive, in the form of `(path!type)`. Traverse depth-first, ignore non-existent paths.

### .miq.fs.rm
| parameter | Type | Description           |
| --------- | ---- | --------------------- |
| `fs`      | fs   | File or directory     |

Recursive deletion. Return dictionary `(path!type)` of deleted items in order of deletion.

## Path
### .miq.fs.path.absolute
| parameter | Type    | Description            |
| --------- | ------- | ---------------------- |
| `fs1`     | fs      | File or directory      |
| `fs2`     | fs\|fsl | File or directory      |

Return the absolute pathname of `fs2` using `fs1` as starting point. If `fs1` is not absolute, it will first be converted to one. In order to potentially reduce the number of expensive system calls, `fs2` accepts a list as well as atom.

### .miq.fs.path.relative
| parameter | Type    | Description            |
| --------- | ------- | ---------------------- |
| `fs1`     | fs      | File or directory      |
| `fs2`     | fs\|fsl | File or directory      |

Return the pathname of `fs2` relative to `fs1`. If `fs1` is not absolute, it will first be converted to one. In order to potentially reduce the number of expensive system calls, `fs2` accepts a list as well as atom.

### .miq.fs.path.real
| parameter | Type    | Description            |
| --------- | ------- | ---------------------- |
| `fs`      | fs      | File or directory      |

Return the cannonicalised path of `fs`.

> [!NOTE] Uses bash builtin `realpath` if available. Pure q implementation is approximatelly 10x slower.

### .miq.fs.path.common
| parameter | Type    | Description            |
| --------- | ------- | ---------------------- |
| `fs1`     | fs      | File or directory      |
| `fs2`     | fs      | File or directory      |

Return common path root of both file paths, and an empty list `` `symbol$() `` if there's none.

## Files
### .miq.fs.file.split
| parameter | Type | Description |
| --------- | ---- | ----------- |
| `sFile`   | s    | File name   |

Return `sFile` split into `name` and `extension`.

### .miq.fs.file.join
| parameter   | Type | Description                  |
| ------------| ---- | ---------------------------- |
| `slNameExt` | sl   | List of file name components |

Join the file name components together into a file name. If last element of `slNameExt` is null, consider the file to have no extension and exclude it.

### .miq.fs.file.new
| parameter | Type | Description   |
| --------- | ---- | ------------- |
| `fsDir`   | fs   | Directory     |
| `sName`   | s    | File name     |

Create an empty file `sName` withing `fsDir` and return file symbol of newly created file. If `fsDir` does not exist, it will be silently created if possible. If `sName` file exists, it will be silently overwritten.

### .miq.fs.file.rm
| parameter | Type | Description |
| --------- | ---- | ----------- |
| `fs`      | fs   | File        |

Delete `fs` and return file symbol of given file.

## Directories
### .miq.fs.dir.content
| parameter | Type | Description   |
| --------- | ---- | ------------- |
| `fs`      | fs   | Directory     |

Return content of the directory in the form of `(item!type)`.

### .miq.fs.dir.new
| parameter | Type | Description                            |
| --------- | ---- | -------------------------------------- |
| `fsDir`   | fs   | Directory                              |
| `slName`  | sl   | Directories to nest in the given order |

Create a nested directory structure within `fsDir` using elementes of `slName`. That means the directories are create in depth, and not each of them as children of `fsDir` directly. If `fsDir` does not exist, it will be silently created if possible.

### .miq.fs.dir.rm
| parameter | Type | Description   |
| --------- | ---- | ------------- |
| `fs`      | fs   | Directory     |

Delete `fs` and return file symbol of given directory.


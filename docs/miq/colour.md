Colour
===
---
Colorisation of strings for unix console.

> [!ATTENTION] Makes use of *true colour* capabilities of your terminal. Your terminal is unlikely to lack support, so ask uncle Google and configure it!

**Errors**

| Message                               | Explanation                                                                           |
| ------------------------------------- | ------------------------------------------------------------------------------------- |
| `Invalid colour name`                 | Colour not defined                                                                    |
| `Invalid colour mode`                 | Mode does not exist                                                                   |
| `Invalid RGB value`                   | Not a valid RGB code                                                                  |
| `Colour already defined`              | Attempt to `.miq.colour.add` a colour that's already defined                          |
| `No such tag`                         | Tag not recognised, see their list in [`.miq.colour.process`](#miqcolourprocess)     |
| `Invalid tag argument`                | Tag argument is invalid                                                               |

**Examples**
```
q).miq.colour.names[]
`red`yellow`green`cyan`blue`purple`black`white
q).miq.colour.modes[]
``off`bold`dim`invert`underline

q).miq.colour.is.colour`byzantine
0b
q).miq.colour.is.rgb 74 20 64
1b

q).miq.colour.add[`byzantine;74 20 64]
`byzantine
q).miq.colour.fg[`byzantine] "Text in byzantine colour"
"\033[38;2;74;20;64mText in byzantine colour\033[0m"
q).miq.colour.is.colour`byzantine
1b

/ Prints out the above text underlined and on yellow background
q)-1 .miq.colour.bg[`yellow] .miq.colour.mode[`underline] .miq.colour.fg[`byzantine] "Text in byzantine colour";

/ Same as above using in-line tags
q)-1 .miq.colour.process "${underline}${bg yellow}${fg byzantine}Text in byzantine colour${off}";
Text in byzantine colour

/ You can play around with the tags, but don't forget to add ${off} at the end, otherwise the colour coding will persist
Text in byzantine colour
q)-1 .miq.colour.process "${bg blue}${fg red}Text in ${fg 111 222 111}funky${fg red} colour${off}";
Text in funky colour
```

## .miq.colour.is.colour
| parameter | Type  | Description                   |
| --------- | ----- | ----------------------------- |
| `sColour` | s     | Colour name                   |

## .miq.colour.is.mode
| parameter | Type  | Description                   |
| --------- | ----- | ----------------------------- |
| `sMode`   | s     | Colour mode                   |

## .miq.colour.is.rgb
| parameter | Type  | Description                   |
| --------- | ----- | ----------------------------- |
| `jlRGB`   | jl    | Three longs representing RGB  |

Returb `1b` if `jlRGB` is a valid RGB value. RGB value has to consist of three numbers that are each in range of 0-255, i.e. byzantine RGB colour is `74 20 64`.

## .miq.colour.codes
Return dictionary of colour definitions in `colour!RGB` format.

## .miq.colour.names
List of available colours.

## .miq.colour.modes
List of applicable colour modes.

## .miq.colour.add
| parameter | Type  | Description                   |
| --------- | ----- | ----------------------------- |
| `sColour` | s     | Colour name                   |
| `jlRGB`   | jl    | Three longs representing RGB  |

Add definition `jlRGB` of colour `sColour`. Can only add colours that are not yet defined.

## .miq.colour.remove
| parameter | Type  | Description                   |
| --------- | ----- | ----------------------------- |
| `sColour` | s     | Colour name                   |

Remove definition of `sColour`. Can only remove already defined colours.

## .miq.colour.mode
| parameter | Type  | Description                   |
| --------- | ----- | ----------------------------- |
| `sMode`   | s     | Colour mode                   |
| `clText`  | cl    | Text to be modified           |

Applies colour mode `sMode` to `clText`.

## .miq.colour.fg
| parameter | Type  | Description                   |
| --------- | ----- | ----------------------------- |
| `sColour` | s     | Colour name                   |
| `clText`  | cl    | Text to be modified           |

Applies foreground (text) colour `sColour` to `clText`.

## .miq.colour.bg
| parameter | Type  | Description                   |
| --------- | ----- | ----------------------------- |
| `sColour` | s     | Colour name                   |
| `clText`  | cl    | Text to be modified           |

Applies background colour `sColour` to `clText`.

## .miq.colour.process
| parameter | Type  | Description                   |
| --------- | ----- | ----------------------------- |
| `clText`  | cl    | Text with colour tags         |

Parses `clText` for any in-line tags that are then applied to the text itself. Tags take the form of `${...}`. For usage examples, see the **examples** section.

List of tags:
* `${mode}` - apply `mode` to text, run `.miq.colour.modes[]` for the full list
* `${fg ...}` - apply colour to foreground (text); argument is either a name of the colour or its RGB definition
* `${bg ...}` - apply colour to background; argument is either a name of the colour or its RGB definition


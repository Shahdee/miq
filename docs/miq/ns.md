Namespace
=========
---
This section provides routines to create, alter, inspect and destroy context (namespace). It is as simple as it sounds.

* **Address** - symbol of `ns` type representing the full path of any context or name, starting from root context
* **Context** - a dictionary whose (1) keys are symbols (2) that do not contain dots and (3) the list of dictionary values is of `generic` list type
* **Name** - any address or an object that is not a dictionary

**Errors**

| Message                                     | Explanation                                    |
| ------------------------------------------- | ---------------------------------------------- |
| `No such namespace address`                 | Given namespace address does not exist         |
| `No such context`                           | Given context does not exist                   |
| `No such name`                              | Given name does not exist                      |
| `Context is not an ancestor`                | Given context is not an ancestor to given name |

**Examples**
```
q).miq.ns.type each (`.miq;`.miq.module.load)
`context`name
q).miq.ns.type `.miq.NoThErE
2021.02.25 | 22:53:37.825 | [ERROR] | <.miq.ns.is.error.present> | No such namespace address: `.miq.NoThErE
'No such namespace address: `.miq.NoThErE
q).miq.ns.is.present each (`.miq;`.miq.module.load;`.miq.NoThErE)
110b
q).miq.ns.is.context each (`.miq;`.miq.module.load;`.miq.NoThErE)
100b
q).miq.ns.is.name each (`.miq;`.miq.module.load;`.miq.NoThErE)
010b
q).miq.ns.is.ancestor[`.ns1.a]'[`.ns1.a.good`.ns1.b.bad]
10b

q).miq.library
{[x] .miq.z.library[x]}
q).miq.ns.set[`.miq.library;{"Let me in!"}]
`.miq.library
q).miq.library
{"Let me in!"}

q).miq.ns.purge[`.miq.library]
23:07:33.489 [WARN]   <.miq.ns.purge> No such context: `.miq.library
q).miq.ns.purge[`.miq.cfg]
`.miq.cfg

q).miq.ns.user[]
`.miq`.log`.is`.bb`.slot`.cfg`.MIQ`.CFG`.ROLE`.DEF`.SO`.pi`.ipc`.clan`.exec`.mat

q).miq.ns.tree[]
..bb       | name
.miq       | context
.log       | context
.is        | context
.bb        | context
q).miq.ns.tree[`.bb]
.bb.                  | name
.bb.miq               | context
.bb.miq.              | name
.bb.miq.config        | context
.bb.miq.init          | name
.bb.miq.library       | name
```

## Checks
### .miq.ns.is.present
| parameter | Type | Description          |
| --------- | ---- | -------------------- |
| `ns`      | ns   | Namespace address    |

Return `1b` if `ns` is either a `context` or a `name`, and `0b` if neither.

### .miq.ns.is.context
| parameter | Type | Description          |
| --------- | ---- | -------------------- |
| `ns`      | ns   | Namespace address    |

Return `1b` is `ns` is a `context`, as opposed to being a `name`.

### .miq.ns.is.name
| parameter | Type | Description          |
| --------- | ---- | -------------------- |
| `ns`      | ns   | Namespace address    |

Return `1b` is `ns` is a `name`, as opposed to being a `context`.

### .miq.ns.is.ancestor
| parameter   | Type | Description                 |
| ----------- | ---- | --------------------------- |
| `nsContext` | ns   | Context address             |
| `nsName`    | ns   | Name address                |

Return `1b` if `nsContext` is ancestor to `nsName`, `0b` otherwise.

## .miq.ns.type
| parameter | Type | Description          |
| --------- | ---- | -------------------- |
| `ns`      | ns   | Namespace address    |

Return `` `context`` if `ns` is a context, `` `name`` otherwise. If `ns` is not present, throw an error.

## .miq.ns.path.absolute
| parameter | Type  | Description               |
| --------- | ----- | ------------------------- |
| `ns`      | s\|ns | Name / Namespace address  |

Return absolute path of `ns`, relative to the current working directory.

## .miq.ns.path.relative
| parameter | Type  | Description               |
| --------- | ----- | ------------------------- |
| `ns`      | s\|ns | Name / Namespace address  |

Return the path of `ns` relative to the current working directory.

## .miq.ns.set
| parameter    | Type | Description                 |
| ------------ | ---- | --------------------------- |
| `nsName`     | ns   | Name address                |
| `content`    | -    | Value to set `nsName` to    |

Sets `nsName` to `content` value the `nsName` address.

## .miq.ns.delete
| parameter  | Type | Description                 |
| ---------- | ---- | --------------------------- |
| `nsName`   | ns   | Context or name address     |

Splits `nsName` namespace into `context` and `name` and performs deletion of `name` from `context`, returning the affected `context`. If `context` is `` ` ``, meaning the `name` is one of the root contexts, purge it using `.miq.ns.purge` routine.

## .miq.ns.purge
| parameter   | Type | Description     |
| ----------- | ---- | --------------- |
| `nsContext` | ns   | Context address |

Sets `nsContext` to `1#.q` and return the `nsContext` address. If `nsContext` does not exist, then warn and return generic null.

## .miq.ns.inject
| parameter   | Type | Description               |
| ----------- | ---- | ------------------------- |
| `ch`        | ch   | Handle to another process |
| `nsContext` | ns   | Context address           |

Deliver `nsContext` to process available on handle `ch`.

## .miq.ns.user
Return all user defined namespaces within root context, except the reserved ones as returned by `.miq.ns.reserved[]`.

## .miq.ns.reserved
List of reserved namespaces, as configured by `ns.reserved.list`. If `ns.reserved.enable` is false, no namespaces are considered reserved and return an empty symbol list.

> [!ATTENTION] Just because you *can* place your code into a single letter context does not mean that you also *should*. Please observe the best practices and respect the fact that KX reserves the use of single letter namespaces!

## .miq.ns.tree
| parameter   | Type                | Description                           |
| ----------- | ------------------- | ------------------------------------- |
| `nsContext` | none\|qd\|ns\|nsl   | Namespace address(es) or context dict |

Explore `nsContext` and return a dictionary of `ns!type` format, where `ns` is the address within namespace / object and `type` is either `name` or `context`. Namespaces returned by `.miq.ns.reserved[]` are excluded unless requested explictly.
* `none` - return the full tree (same as ` ``. `)
* `ns` - return tree starting from `nsContext`
  * `` ` ``  - return tree of names in the root context
  * `` `. `` - return full tree except names in the root context
* `nsl` - return tree for all of constituents, following the `ns` rule above
* `qd` - return tree of the given object

## .miq.ns.trim
| parameter   | Type         | Description                      |
| ----------- | ------------ | -------------------------------- |
| `slLeaf`    | s\|sl        | List of leaf names to trim       |
| `qd`        | qd           | Tree to trim                     |

Remove leafs (names) that match any of `slLeaf` names from *ns tree* `qd`. By *ns tree* we understand the return value of `.miq.ns.tree[]`.

## .miq.ns.prune
| parameter   | Type         | Description                      |
| ----------- | ------------ | -------------------------------- |
| `slBranch`  | s\|sl        | List of branch names to prune    |
| `qd`        | qd           | Tree to trim                     |

Remove branches (contexts) and all nested names that match any of `slBranch` names from *ns tree* `qd`. By *ns tree* we understand the return value of `.miq.ns.tree[]`.

## Gist
### .miq.ns.gist.new
| parameter   | Type              | Description                           |
| ----------- | ----------------- | ------------------------------------- |
| `nsContext` | none\|qd\|ns\|nsl | Namespace address(es) or context dict |

Construct a new gist of addresses within `nsContext`, as returned by `.miq.ns.tree[]`, store it in the `GISTS` table and return the name of the newly created gist. Gist is a dictionary of `nsAddress!descriptor`, where the descriptor is determined by the type of object being described:
* Empty lists of any type and types `` `nyi`generic `` are described using `()`
* Types `` `qd`qdl`qk`qkl`qt`qtl `` are described using their `count`
* Types `` `qp`qpl `` are simply described as `` `parted ``
* Type `code` is described using `md5` hash of its stringed and razed details
* All other types are described by `md5` hash of their stringed and razed forms

### .miq.ns.gist.delete
| parameter   | Type         | Description                      |
| ----------- | ------------ | -------------------------------- |
| `sName`     | s            | Gist name                        |

Delete the gist identified by `sName` from the collection of gists and return `sName`.

### .miq.ns.gist.get
| parameter   | Type         | Description                      |
| ----------- | ------------ | -------------------------------- |
| `sName`     | s            | Gist name                        |

Return the gist object identified by `sName`.

### .miq.ns.gist.info
| parameter   | Type         | Description                      |
| ----------- | ------------ | -------------------------------- |
| `sName`     | none\|s      | Gist name                        |

Return all information on gist identified by `sName`. If `sName` is `None`, return information on all recorded gists.

### .miq.ns.gist.diff
| parameter   | Type         | Description                      |
| ----------- | ------------ | -------------------------------- |
| `sName1`    | s            | Gist name to compare against     |
| `sName2`    | s            | Gist name to compare             |

Compare gist `sName2` against `sName1`, using `sName1` gist as base, and return a dictionary describing the changes for each of the keys. The dictionary returned has the shape of: `` `matched`changed`removed`added!"SSSS"$\:() ``


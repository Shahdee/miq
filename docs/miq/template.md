Template
===
---
Creation of templated routines supporting interface desired.
* **interface** - description of inputs the template takes, i.e. `` `j`il` `` means an arity 3 interface accepting argument of types `(long;int list;anything)`
* **nulls in interface** - match any type
* **implementer** - a function implementing the interface, i.e. `{[j;il;anythin] ...}`

> [!ATTENTION]
> * Projection of templated functions have to be explicit
> * Projection of unary interface is not supported
> * Interfaces per template are sorted by arity and number of nulls involved (prevents interface collision)

**Errors**

| Message                                               | Explanation                                                                       |
| ----------------------------------------------------- | --------------------------------------------------------------------------------- |
| `No such template`                                    | No templated function of such name is present                                     |
| `No such template interface`                          | Templated function does not currently support this interface                      |
| `Not a valid template interface`                      | Interface definition is not valid                                                 |
| `No matching interface`                               | Templated function could not match input to interface implementer                 |
| `Not a valid template blueprint`                      | Given object does not match a definition of a template blueprint                  |
| `Template interface already defined`                  | Attempt to introduce template interface that is already defined                   |

**Examples**
```
/ Define functions
q).a.a:{x+y}
q).a.b:{`year$x}
q).a.c:{x#type y}


/ Create new templated function and assign above functions as the interface implementers
q).miq.template.add[`.t.a;`j`j;`.a.a]
2021.03.20 | 16:17:43.396 | [INFO]  | <.miq.template.add> | Creating new template interface: `template`interface!(`.t.a;`j`j)
`.t.a
q).miq.template.add[`.t.a;`d;`.a.b]
2021.03.20 | 16:17:45.215 | [INFO]  | <.miq.template.add> | Creating new template interface: `template`interface!`.t.a`d
`.t.a
q).miq.template.add[`.t.a;`j`;`.a.c]
2021.03.20 | 16:17:47.055 | [INFO]  | <.miq.template.add> | Creating new template interface: `template`interface!(`.t.a;`j`)
`.t.a


/ Test fire template
q).t.a[1;2]
3
q).t.a[2021.02.21]
2021i
q).t.a[3;`symbol]
-11 -11 -11h


/ Fail on unknown interface
q).t.a[1]
2021.03.20 | 16:19:43.786 | [ERROR] | <.t.a> | No matching interface: `template`interface!(`.t.a;,`j)
'No matching interface: `template`interface!(`.t.a;,`j)
  [0]  .t.a[1]
       ^


/ Projections have to be explicit
q).t.a[3]@/:(`a;.z.p;.z.d)
2021.03.20 | 16:20:26.544 | [ERROR] | <.t.a> | No matching interface: `template`interface!(`.t.a;,`j)
'No matching interface: `template`interface!(`.t.a;,`j)
  [0]  .t.a[3]@/:(`a;.z.p;.z.d)
       ^
q).t.a[3;]@/:(`a;.z.p;.z.d)
-11 -11 -11
-12 -12 -12
-14 -14 -14


/ List templated functions
q).miq.template.list[]
,`.t.a


/ Show interface information on a templated function; interfaces are sorted in order of execution
q).miq.template.info[`.t.a]
name interface| function arity
--------------| --------------
.t.a ,`d      | .a.b     1
.t.a `j`j     | .a.a     2
.t.a `j`      | .a.c     2


/ Regenerate templated function
q).miq.template.update[`.t.a]
`.t.a

/ Remove templated function interfaces
/ Removing the last one will result in templated function address being deleted
q).miq.template.remove .' get each key .miq.template.info[`.t.a]
2021.03.20 | 16:54:03.382 | [INFO]  | <.miq.template.remove> | Deleting an existing template interface: `template`interface!(`.t.a;,`d)
2021.03.20 | 16:54:03.385 | [INFO]  | <.miq.template.remove> | Deleting an existing template interface: `template`interface!(`.t.a;`j`j)
2021.03.20 | 16:54:03.387 | [INFO]  | <.miq.template.remove> | Deleting an existing template interface: `template`interface!(`.t.a;`j`)
2021.03.20 | 16:54:03.387 | [INFO]  | <.miq.ns.delete> | Deleting name: `.t.a
`.t.a`.t.a`.t.a

q).miq.ns.is.present`.t.a
0b
```

## Checks
### .miq.template.is.present
| parameter         | Type  | Description            |
| ----------------- | ----- | ---------------------- |
| `nsName`          | ns    | Template name          |

Return `1b` if template `nsName` is present, `0b` otherwise.

### .miq.template.is.defined
| parameter         | Type  | Description            |
| ----------------- | ----- | ---------------------- |
| `nsName`          | ns    | Template name          |
| `slInterface`     | s\|sl | Interface description  |

Return `1b` if template `nsName` accepts interface `slInterface`, `0b` otherwise.

### .miq.template.is.interface
| parameter         | Type  | Description            |
| ----------------- | ----- | ---------------------- |
| `slInterface`     | s\|sl | Interface description  |

Return `1b` if interface `slInterface` is a valid interface description, `0b` otherwise.

### .miq.template.is.implementer
| parameter         | Type  | Description            |
| ----------------- | ----- | ---------------------- |
| `nsImplementer`   | ns    | Implementer address    |

Return `1b` if `nsImplementer` is implementing an interface.

### .miq.template.is.blueprint
| parameter         | Type  | Description            |
| ----------------- | ----- | ---------------------- |
| `qt`              | qt    | Template blueprint     |

Return `1b` if `qt` is a template blueprint.

Blueprint is a table of 3 columns where each are of conforming type:
* `name` - templated function address, type `ns`
* `interface` - a valid interface, as checked by `.miq.template.is.interface[]`
* `implementer` - address to an executable function, as checked by `.miq.fn.is.address[]`

## .miq.template.list
Return list of templated functions.

## .miq.template.info
| parameter         | Type      | Description            |
| ----------------- | --------- | ---------------------- |
| `nsName`          | ns        | Template name          |

Return interface definitions of `nsName` template.

## .miq.template.mock
| parameter         | Type  | Description            |
| ----------------- | ----- | ---------------------- |
| `nsName`          | ns    | Template name          |
| `arguments`       | -     | Any arguments          |

Return interface definitions of `nsName` template that could be used if `nsName` was executed with `arguments`.

## .miq.template.blueprint
| parameter         | Type  | Description            |
| ----------------- | ----- | ---------------------- |
| `qtBlueprint`     | qt    | Template blueprint     |

Take the template blueprint and implement it:
1. `.miq.template.delete[]` all blueprint templates in existence
1. `.miq.template.add[]` every single blueprint entry

This will result in templated function definitions

## .miq.template.generate
| parameter     | Type  | Description            |
| ------------- | ----- | ---------------------- |
| `nsName`      | ns    | Template name          |

Return a function wrapping all implementers of `nsName` template.

## .miq.template.add
| parameter         | Type  | Description            |
| ----------------- | ----- | ---------------------- |
| `nsName`          | ns    | Template name          |
| `slInterface`     | s\|sl | Interface description  |
| `nsImplementer`   | ns    | Implementer address    |

Add an interface `slInterface` for template `nsName` and set its implementer to `nsImplementer`.

## .miq.template.remove
| parameter     | Type  | Description            |
| ------------- | ----- | ---------------------- |
| `nsName`      | ns    | Template name          |
| `slInterface` | s\|sl | Interface description  |

Remove interface `slInterface` from template `nsName`. Will call `.miq.template.delete[nsName]` on removal of last interface of `nsName`.

## .miq.template.delete
| parameter     | Type  | Description            |
| ------------- | ----- | ---------------------- |
| `nsName`      | ns    | Template name          |

Remove all interface of `nsName` template and delete the templated function itself.

## .miq.template.update
| parameter     | Type  | Description            |
| ------------- | ----- | ---------------------- |
| `nsName`      | ns    | Template name          |

Update template `nsName` definition.


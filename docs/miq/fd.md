FD
==
---
Routines operating on file descriptors of current process.

**Examples**
```
q).miq.fd.stdin[]
`:/dev/pts/5

q).miq.fd.list[]
0 1 2 3 4 5 6 7 8i
q).miq.fd.info each -2#.miq.fd.list[]
fd htype stale file                target
----------------------------------------------------
7  ch    0     :/proc/2051953/fd/7 :socket:[8436863]
8  ch    0     :/proc/2051953/fd/8 :socket:[8436900]
q).miq.fd.info[]
fd htype  stale file                target
-----------------------------------------------------------
0  `fh`ch 0     :/proc/2051953/fd/0 :/dev/pts/6
1  ,`fh   0     :/proc/2051953/fd/1 :/dev/pts/6
2  ,`fh   0     :/proc/2051953/fd/2 :/dev/pts/6
3  ,`fh   0     :/proc/2051953/fd/3 :/var/lib/sss/mc/passwd
4  ,`fh   0     :/proc/2051953/fd/4 :/dev/urandom
5  ,`fh   0     :/proc/2051953/fd/5 :socket:[8436815]
6  ,`fh   0     :/proc/2051953/fd/6 :socket:[8436816]
7  ,`ch   0     :/proc/2051953/fd/7 :socket:[8436863]
8  ,`ch   0     :/proc/2051953/fd/8 :socket:[8436900]

/ Create a new out file, redirect STDOUT and STDERR there, and write a couple of lines of non-sense
q).miq.fs.file.new[`:/tmp;`test.log]
`:/tmp/test.log
q).miq.fd.redirect[1 2;`:/tmp/test.log]
Missing separate debuginfo for /work/michal/install/q/2019.09.19/q64/l64/q
Try: dnf --enablerepo='*debug*' install /usr/lib/debug/.build-id/82/0a4be82ceb41b77f5650b79aef00c9cff81606.debug
1+1
hahaha

/ Now re-attach STDOUT and STDERR to your terminal!
.miq.fd.redirect[1 2;.miq.fd.stdin[]]
"Attaching to process 9279"
"[New LWP 9280]"
"[New LWP 9282]"
"[Thread debugging using libthread_db enabled]"
"Using host libthread_db library \"/lib64/libthread_db.so.1\"."
"0x00007fec7bd90a3a in __waitpid (pid=9970, stat_loc=0x7fff8be2be1c, options=0) at ../sysdeps/unix/sysv/linux/waitpid.c:30"
"30\t  return SYSCALL_CANCEL (wait4, pid, stat_loc, options, NULL);"
"$1 = 1"
"$2 = 2"
"$3 = 0"
"Detaching from program: /work/michal/install/q/2019.09.19/q64/l64/q, process 9279"
q).log.info "Welcome back!"
2020.09.26 | 13:46:36.655 | [INFO]  | <> Welcome back!

/ And check out /tmp/test.log 
q)\cat /tmp/test.log
"\"Attaching to process 9279\""
"\"[New LWP 9280]\""
"\"[New LWP 9282]\""
"\"[Thread debugging using libthread_db enabled]\""
"\"Using host libthread_db library \\\"/lib64/libthread_db.so.1\\\".\""
"\"0x00007fec7bd90a3a in __waitpid (pid=9955, stat_loc=0x7fff8be2be1c, options=0) at ../sysdeps/unix/sysv/linux/waitpid.c:30\""
"\"30\\t  return SYSCALL_CANCEL (wait4, pid, stat_loc, options, NULL);\""
"\"$1 = 1\""
"\"$2 = 2\""
"\"$3 = 0\""
"\"Detaching from program: /work/michal/install/q/2019.09.19/q64/l64/q, process 9279\""
"q)2"
"q)'hahaha"
"  [0]  hahaha"
"       ^"
"q)Missing separate debuginfo for /work/michal/install/q/2019.09.19/q64/l64/q"
"Try: dnf --enablerepo='*debug*' install /usr/lib/debug/.build-id/82/0a4be82ceb41b77f5650b79aef00c9cff81606.debug"
q)
```

## .miq.fd.stdin
Return STDIN file.

## .miq.fd.stdout
Return STDOUT file.

## .miq.fd.stderr
Return STDERR file.

## .miq.fd.list
Return a list of handles opened by the process. in the form of `handle!handleType`.

## .miq.fd.info
| parameter | Type          | Description               |
| --------- | ------------- | ------------------------- |
| `fd`      | none\|fd      | File Descriptor (handle)  |

Return `qd` of form `` `fd`htype`stale`file`target!`i`sl`b`fs`fs``, describing the currently open file descriptor `fd`. If `fd` is `none`, assume information is required on all open file descriptors. `htype` stands for *handle type* and can be one or more of:`fh`,`ch`.

> [!WARNING] `0i` is a pseudo IPC handle and counts as both `fh` and `ch`.

## .miq.fd.listener
Return the file descriptor of listener socket.

## .miq.fd.redirect
| parameter | Type    | Description    |
| --------- | ------- | -------------- |
| `fhl`     | fh\|fhl | File handle(s) |
| `fsFile`  | fs      | File symbol    |

Redirect handles `fhl` to file `fsFile` on the running process. To control whether redirect appends to or overwrites `fsFile`, configure `fd.redirect.append`. This functionality is implemented `gdb` command using `open` and `dup2` C calls.


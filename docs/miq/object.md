Object
===
---
Object is a named dictonary whose keys (signature) and their expected types are defined in advance during class definition. The type can be either:
* miQ type,
* object class name,
* null symbol to allow any value,
* or a list of above values

> [!NOTE] The order of keys in signature is not important.

**Errors**

| Message                                               | Explanation                                                                   |
| ----------------------------------------------------- | ----------------------------------------------------------------------------- |
| `Signature and types are of different count`          | Object definition given incompatible signature and types lists                |
| `Object class name clash with miQ type name`          | Cannot define object whose name is also a name of an miQ type                 |
| `Object class signature types unrecognised`           | Accepted 'types' include miQ types, class names and null symbol               |
| `Object class already defined`                        | Attempting implicit class redefinition, make it explicit if intended          |
| `Object class not defined`                            | Given class is not known to miQ                                               |
| `Count of values not matching the count of signature` | Attempt to construct an object without providing the right count of values    |
| `Object construction failed due to invalid values`    | Attempt to construct object providing incorrect types of values               |
| `Not a valid object`                                  | Object is a dictionary with keys of type `` `s ``                             |
| `Not an instance of known class`                      | Object instance did not match any object definition                           |
| `Object not of class`                                 | Plain as day, supplied object does not belong to the specified object class   |
| `Objects have to be of same class`                    | Supplied objects were not identified to be of the same class                  |

**Examples**
```
/ Define object class
q).miq.object.define[`my.test;`a`b;`s`jl]
`my.test

/ Test it
q).miq.object.is.class[`my.test] `a`b!(`sym;1 10)
1b
q).miq.object.is.class[`my.test] `a`b!`sym`sym
0b
q).miq.object.verify[`my.test] `a`b!`sym`sym
a| 1
b| 0

/ Derive new class from my.test
q).miq.object.extend[`my.test;`my.test2;`c`d;`p`dl]
`my.test2

/ Number of primary operations can be done directly imported shorthand functions
q).miq.object.import[`my.;`.t]
`my.test`my.test2
q)key each 1_.t
test | class signature definition construct tt verify
test2| class signature definition construct tt verify

q).t.test.class[]
`my.test
q).t.test.signature[]
`a`b

q).t.test.definition[]
a| s
b| jl
q).t.test2.definition[]
a| s
b| jl
c| p
d| dl

q).t.test.construct (`sym;1 2)
a| `sym
b| 1 2
q).t.test.tt[]
a b
---
q).t.test.verify `a`b!`sym`sym
a| 1
b| 0

/ Set operations, except broken at the moment
q)o1:`a`b!(`sym;1 10)
q)o2:`a`b!(`sym;1 20)
q).miq.object[`diff`inter`except`union].\:(o1;o2)
a          b
-------------------
`symbol$() 10 20
,`sym      ,1
`symbol$() `long$()
,`sym      1 10 20

/ Export return q code in string form that defines the object requested, good for initialising required objects as first thing after start
q).miq.object.export[`my.test]
".miq.object.define[`my.test;`s#`a`b;`s`jl]"

/ import, export and resolve accept dot-wise partial class names
q).miq.object.resolve `my
,`my.test
```

## .miq.object.define
| parameter     | Type    | Description            |\*| Interface          | Purpose                   |
| ------------- | ------- | ---------------------- |--| ------------------ | ------------------------- |
| `bRedefine`   | b       | Explicit redefine      |\*| `` `s`sl` ``       | Plain class definition    |
| `sClass`      | s       | Class name             |\*| `` `b`s`sl` ``     | Explicit (re-)definition  |
| `slSignature` | sl      | Object keys            |\*|                    |                           |
| `types`       | sl\|sll | Types of object keys   |\*|                    |                           |

Define object class `sClass` as having signature `slSignature` of `types` types. If `bRedefine` is supplied and true, it won't complain about attempting to implicitly redefine `sClass`.

> [!NOTE] Signature key can map to more than a single type, hence `types` can be a list of symbol lists.

## .miq.object.extend
| parameter     | Type    | Description            |\*| Interface          | Purpose                   |
| ------------- | ------- | ---------------------- |--| ------------------ | ------------------------- |
| `bRedefine`   | b       | Explicit redefine      |\*| `` `s`s`sl` ``     | Plain class extension     |
| `sClass`      | s       | Class to extend        |\*| `` `b`s`s`sl` ``   | Explicit (re-)extension   |
| `sNewClass`   | s       | New class name         |\*|                    |                           |
| `slSignature` | sl      | Object keys            |\*|                    |                           |
| `types`       | sl\|sll | Types of object keys   |\*|                    |                           |

Define object class `sNewClass` inheriting definition of `sClass` and extending it using `slSignature` and `types`. If `bRedefine` is supplied and true, it won't complain about attempting to implicitly redefine `sNewClass`.

## .miq.object.delete
| parameter     | Type | Description            |
| ------------- | ---- | ---------------------- |
| `sClass`      | s    | Class name             |

Delete `sClass` definition.

## .miq.object.identify
| parameter     | Type | Description            |
| ------------- | ---- | ---------------------- |
| `object`      | qd   | Instantiated class     |

Return class of `object` if identification successful, `` ` `` otherwise.

## .miq.object.resolve
| parameter     | Type | Description            |
| ------------- | ---- | ---------------------- |
| `sClass`      | s    | (Partial) class name   |

Return class names that match `sClass` in a part-wise manner, where parts are delimited by a dot.

## .miq.object.import
| parameter     | Type | Description            |
| ------------- | ---- | ---------------------- |
| `sClass`      | s    | (Partial) class name   |
| `nsContext`   | ns   | Context to import to   |

Create shorthands for [primary operations](#primary-operations) for class `sClass` within context `nsContext`. The functions will be projected with `sClass` and also include a function `class[]` that will simply return the full name of the class that has been imported.

## .miq.object.export
| parameter     | Type | Description            |\*| Interface          | Purpose                           |
| ------------- | ---- | ---------------------- |--| ------------------ | --------------------------------- |
| `bRedefine`   | b    | Explicit redefine      |\*| `` `s ``           | Export without redefinition flag  |
| `sClass`      | s    | (Partial) class name   |\*| `` `b`s ``         | Export with explicit redefinition |

Return q code in a string that will (re-)define the `sClass` when evaluated.

## Checks
### .miq.object.is.defined
| parameter     | Type | Description            |
| ------------- | ---- | ---------------------- |
| `sClass`      | s    | Class name             |

Return `1b` if `sClass` is a defined class, `0b` otherwise.

### .miq.object.is.valid
| parameter     | Type | Description            |
| ------------- | ---- | ---------------------- |
| `object`      | qd   | Instantiated class     |

Return `1b` if `object` has a structure expected of an object, `0b` otherwise.

### .miq.object.is.object
| parameter     | Type | Description            |
| ------------- | ---- | ---------------------- |
| `object`      | qd   | Instantiated class     |

Return `1b` if `object` is an instantiation of known class, `0b` otherwise.

### .miq.object.is.class
| parameter     | Type | Description            |
| ------------- | ---- | ---------------------- |
| `sClass`      | s    | Class name             |
| `object`      | qd   | Instantiated class     |

Return `1b` if `object` is an instantion of class `sClass`.

## Utilities
### .miq.object.require
| parameter     | Type | Description            |
| ------------- | ---- | ---------------------- |
| `sClass`      | s    | Class name             |

Throw a signal if `sClass` is not defined at the point of execution.

### .miq.object.classes
Return a list of all defined classes.

## Primary operations
### .miq.object.signature
| parameter     | Type | Description            |
| ------------- | ---- | ---------------------- |
| `sClass`      | s    | Class name             |

Return the signature of `sClass`.

### .miq.object.definition
| parameter     | Type | Description            |
| ------------- | ---- | ---------------------- |
| `sClass`      | s    | Class name             |

Return the definition of `sClass`.

### .miq.object.construct
| parameter     | Type      | Description            |
| ------------- | --------- | ---------------------- |
| `sClass`      | s         | Class name             |
| `values`      | qd\|list  | Any values             |

Return an instantion of `sClass` using `values` to initialise. If `values` is a dictionary, look up the keys matching signature of `sClass` and use their values. If `values` is a list, it has to be of the same length as `sClass` signature. Constructed object is then type checked to make sure it is a proper instantiation of `sClass`.

### .miq.object.tt
| parameter     | Type | Description            |
| ------------- | ---- | ---------------------- |
| `sClass`      | s    | Class name             |

Return an empty table derived from `sClass` definition.

### .miq.object.verify
| parameter     | Type | Description            |
| ------------- | ---- | ---------------------- |
| `sClass`      | s    | Class name             |
| `object`      | qd   | Instantiated class     |

Return a dictionary mapping the keys to boolean atoms. The keys are a combination of keys `sClass` signature and the tested `object`. Boolean values are `1b` when the given key complies with `sClass` definition, `0b` otherwise.

## Set operations
### .miq.object.diff
### .miq.object.inter
### .miq.object.except
### .miq.object.union
| parameter     | Type | Description            |
| ------------- | ---- | ---------------------- |
| `o1`          | qd   | Object                 |
| `o2`          | qd   | Object                 |

> [!ATTENTION] `.miq.object.except[]` is not working properly at the moment.

Return result of set operation on objet `o1` and `o2` elements *by key*. Both objects have to be of the same class, as returned by `.miq.object.identify[]`.
* `diff` - values found only in `o1` or `o2`
* `inter` - values found in both `o1` and `o2`
* `except` - values found in `o1` but not in `o2`
* `union` - all values from `o1` and `o2`


Hook
====
---
miQ provides advanced hook management and extends standard kdb `.z` hooks with new ones.

> [!NOTE] All hooks are executed via apply `@`. For hook implementations of arity greater than 1, project via dot apply `.`.

Hook definitions are divided into *primary* and *non-primary*. Every hook can have only one *primary* definition at any given time, in addition to any number of *non-primary* definitions. If there's no primary *definition*, hook will not execute.

If there's only a single definition associated with hook, it is directly assigned to hook itself for perfomance reasons. If there's more than one definition associated with a hook, execution is done via `.miq.hook.process`. *Primary* definition is executed first, it's result assigned to `.miq.z.LAST` (see [Special hooks](#Special)), and only then are the remaining definitions executed.

Individual modules can define their own hooks as well. To do that, and utilise `.miq.hook` capabilities, the hook has to be defined under `<NSPATH>.z` namespace, and its default definition under `<NSPATH>.z.dd` namespace. As an example, `.module.z.nice` defines `nice` hook of `module`, while `.module.z.dd.nice` is it's default implementation. It is common to define only the default implementation and initiate it using `.miq.hook.expunge` if hook is not defined.

**Errors**

| Message                                            | Explanation                                              |
| -------------------------------------------------- | -------------------------------------------------------- |
| `Hook not defined`                                 | Hook does not exist                                      |
| `Hook label reserved`                              | Label reserved for internal use      use                 |
| `Attempt to overwrite primary hook by non-primary` | Use `.miq.hook.primary` to alter primary hook definition |
| `No functions registered`                          | Given hook has no functions registered                   |
| `Not a hook name`                                  | Not a valid hook name                                    |
| `No such label for`                                | Label not known to hook                                  |

**Examples**
```

```


## .miq.hook.process
| parameter  | Type | Description              |
| ---------- | ---- | ------------------------ |
| `nsHook`   | ns   | Hook name                |
| `fnlHooks` | fnl  | List of hook definitions |
| `x`        | -    | Any input                |

Applicable for hooks that have a primary definition and at least one non-primary. It executes hook definitions in order they've been registered, with exception of primary definition which is always executed first. The output of primary execution is stored in `.miq.z.LAST` hook in the format of `(nsHook;returnValue)`, from which the `returnValue` is returned to caller. This allows modification of `returnValue` by subsequent non-primary definitions, effectively altering the output of primary definition.

> [!NOTE] This function is executed under root context.

## .miq.hook.z
Return an `nsl` enumeration of all known kdb hooks.

## .miq.hook.is.defined
| parameter | Type | Description |
| --------- | ---- | ----------- |
| `nsHook`  | ns   | Hook name   |

Returns `1b` if `nsHook` is defined, `0b` otherwise.

## .miq.hook.is.name
| parameter  | Type | Description |
| ---------- | ---- | ----------- |
| `nsHook`   | ns   | Hook name   |

Return `1b` if `nsHook` is a valid hook name, otherwise `0b`.

## .miq.hook.is.label
| parameter | Type  | Description |
| --------- | ----- | ----------- |
| `nsHook`  | ns    | Hook name   |
| `label`   | s\|ns | Label       |

Return `1b` if a definition under `label` is known to `nsHook`, otherwise `0b`.

## .miq.hook.definitions
| parameter | Type | Description |
| --------- | ---- | ----------- |
| `nsHook`  | ns   | Hook name   |

Return a dictionary of `label!definition` for `nsHook`.

## .miq.hook.default
| parameter | Type | Description |
| --------- | ---- | ----------- |
| `nsHook`  | ns   | Hook name   |

Return the name of default implementation for `nsHook`, whether it exist or not.

## .miq.hook.expunge
| parameter | Type | Description |
| --------- | ---- | ----------- |
| `nsHook`  | ns   | Hook name   |

Set primary `nsHook` definition to it's default implementation, if it has one. If not, then call `\x` for kdb hooks, or delete the name for non-kdb hooks.

## .miq.hook.info
| parameter | Type | Description |
| --------- | ---- | ----------- |
| `ns`      | ns   | Context     |

Return a table of known hooks, their default implementations and current status within `ns` context. If `ns` is `NONE`, it will be treated as `` ` ``.

## .miq.hook.primary
| parameter | Type          | Description           |
| --------- | ------------- | --------------------- |
| `label`   | s\|ns         | Hook definition label |
| `nsHook`  | ns            | Hook name             |
| `fn`      | s\|fn\|op\|qc | Hook definition       |

Set primary definition of hook `nsHook` to `fn`, labelled `label`. `fn` can be either a function or an address of a function in the form of symbol. Refresh `.miq.hook.process` projection if it's in use.

## .miq.hook.register
| parameter | Type          | Description           |
| --------- | ------------- | --------------------- |
| `label`   | s\|ns         | Hook definition label |
| `nsHook`  | ns            | Hook name             |
| `fn`      | s\|fn\|op\|qc | Hook definition       |

Register non-primary `nsHook` hook definition `fn` under `label` label. If the combined number of hook definitions is over 1, and primary definition exists, refresh hook's `.miq.hook.process` projection.

## .miq.hook.remove
| parameter | Type  | Description           |
| --------- | ----- | --------------------- |
| `label`   | s\|ns | Hook definition label |
| `nsHook`  | ns    | Hook name             |

Remove `nsHook` hook definition labelled `label`. If `label` is primary definition, call `.miq.hook.expunge[nsHook]`. If primary definition exists, then either refresh `.miq.hook.process` projection if there's more than 1 definition, or assign the primary definition to hook directly for improved performance.

## .miq.hook.depth
| parameter  | Type          | Description            |
| ---------- | ------------- | ---------------------- |
| `nsHook`   | ns            | Hook name              |
| `fnOrigin` | s\|fn\|op\|qc | Entry point function   |
| `arg`      | -             | Argument to `fnOrigin` |

Return number of function calls in between execution of `fnOrigin arg` and `nsHook`, inclusive. Returns `0N` if `nsHook` is not execute as part of `fnOrigin` code path.


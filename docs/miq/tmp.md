Temporary storage
=================
---
If enabled, provides capability to handle temporary files and directories within allocated space on disk.

**Errors**

| Message                        | Explanation                                  |
| ------------------------------ | -------------------------------------------- |
| `TMP directory does not exist` | `TMP.PATH` has not been created              |
| `TMP not enabled`              | tmp has not been enabled                     |
| `Not in TMP`                   | File / directory to rm is not part of tmp    |

**Examples**
```
q).miq.config.set[`tmp.dir;`miQ.test];
q).miq.config.apply[]
...

q).miq.tmp.is.enabled[]
1b

q).miq.tmp.file.new `Michal
`:/tmp/miQ.test/Michal.UDkNgBzH/Michal
q)`:/tmp/miQ.test/Michal.UDkNgBzH/Michal set til 5
`:/tmp/miQ.test/Michal.UDkNgBzH/Michal
q).miq.tmp.file.rm `:/tmp/miQ.test/Michal.UDkNgBzH/Michal
2021.02.08 | 21:17:49.032 | [INFO]  | <.miq.tmp.file.rm> | Deleting TMP file: `:/tmp/miQ.test/Michal.UDkNgBzH/Michal
2021.02.08 | 21:17:49.033 | [INFO]  | <.miq.fs.file.rm> | Deleting file: `:/tmp/miQ.test/Michal.UDkNgBzH/Michal
2021.02.08 | 21:17:49.034 | [INFO]  | <.miq.tmp.file.rm> | Deleting empty TMP directory: `:/tmp/miQ.test/Michal.UDkNgBzH


q)tmp:0N!` sv .miq.tmp.dir.new[`test],`file.txt
`:/tmp/miQ.test/test.kIFrkZ8C/file.txt
q)tmp set til 5
`:/tmp/miQ.test/test.kIFrkZ8C/file.txt
q).miq.tmp.dir.rm `:/tmp/miQ.test/test.kIFrkZ8C/file.txt
2021.02.08 | 21:19:26.381 | [WARN]  | <.miq.tmp.dir.rm> | No TMP directory to delete: `:/tmp/miQ.test/test.kIFrkZ8C/file.txt
q).miq.tmp.dir.rm `:/tmp/miQ.test/test.kIFrkZ8C
2021.02.08 | 21:19:43.874 | [INFO]  | <.miq.tmp.dir.rm> | Deleting TMP directory: `:/tmp/miQ.test/test.kIFrkZ8C
2021.02.08 | 21:19:43.874 | [INFO]  | <.miq.fs.file.rm> | Deleting file: `:/tmp/miQ.test/test.kIFrkZ8C/file.txt
2021.02.08 | 21:19:43.874 | [INFO]  | <.miq.fs.dir.rm> | Deleting directory: `:/tmp/miQ.test/test.kIFrkZ8C
```

## .miq.tmp.enable
Enables tmp functionality and creates the `tmp.dir` if not created and `tmp.autocreate` is true. It is automatically executed as part of `.miq.config.apply` call if `tmp.enable` is true.

## .miq.tmp.clear
Clear out TMP directory of all content.

## .miq.tmp.disable
Disables tmp functionality.

## .miq.tmp.is.enabled
Returns `1b` if tmp functionality is enabled, otherwise `0b`.

## .miq.tmp.file.new
| parameter | Type | Description            |
| --------- | ---- | ---------------------- |
| `sName`   | s    | Name of temporary file |

Return full path to a file named `sName`, in it's own dedicated directory created by `.miq.tmp.dir.new`, and nested under `tmp.dir`. No file is created.

## .miq.tmp.file.rm
| parameter | Type | Description            |
| --------- | ---- | ---------------------- |
| `fsName`  | fs   | Path to temporary file |

Delete `fsName`. If it leaves an empty directory behind, it gets deleted as well. Warn if `fsName` does not exist.

## .miq.tmp.dir.path
Return current tmp directory path, see `TMP.PATH`.

## .miq.tmp.dir.new
| parameter | Type | Description                 |
| --------- | ---- | --------------------------- |
| `sName`   | s    | Name of temporary directory |

Create an empty directory named `sName.XXXXXXXX` and return it's full path.

## .miq.tmp.dir.rm
| parameter | Type | Description                 |
| --------- | ---- | --------------------------- |
| `fsName`  | fs   | Path to temporary directory |

Recursively delete all content of `fsName` directory, including itself. Warn if `fsName` does not exist.

Functional query
================
---
Set of routines to construct, manipulate and inspect functional queries. Currently it's **WORK IN PROGRESS** and needs much more love. Will probably get around to do this when creating a query routing module.

**Examples**
```
q)cond:`extension`class!`q`module
q)-3!.miq.fq.d2cond[=;cond]
"((=;`extension;,`q);(=;`class;,`module))"
q)?[`.bb.miq.library;.miq.fq.d2cond[=;cond];0b;()]
directory                name  extension| class  loaded lzp                           request
----------------------------------------| ---------------------------------------------------
:/work/michal/repo/q/miq miq   q        | module 1      2020.05.20D22:23:45.427833000
q)cond:`name`class!(`miq;`module`config)
q)?[`.bb.miq.library;.miq.fq.d2cond[(=;in);cond];0b;()]
directory                name extension| class  loaded lzp                           request
---------------------------------------| ---------------------------------------------------
:/work/michal/repo/q/miq miq  q        | module 1      2020.05.20D22:23:45.427833000
:/work/michal/repo/q/cfg miq  q        | config 1      2020.05.20D22:23:45.453988000

q)-3!.miq.fq.not[1] .miq.fq.d2cond[(=;in);cond]
"((=;`name;,`miq);(~:;(in;`class;,`module`config)))"
q)?[`.bb.miq.library;.miq.fq.not[1] .miq.fq.d2cond[(=;in);cond];0b;()]
directory                name extension| class      loaded lzp request
---------------------------------------| -----------------------------
:/work/michal/repo/q/def miq  q        | definition 0
```

## .miq.fq.d2cond
| parameter | Type | Description                            |
| --------- | ---- | -------------------------------------- |
| `op`      | -    | Operator(s) to use on dictionary items |
| `dict`    | qd   | Columns!values dictionary              |

Number of `op` ops has to be either 1 or equal to number of `dict` keys. Function will map `op` to `dict` keys and values and return a list of conditions suitable for functional queries.

## .miq.fq.not
| parameter | Type  | Description                       |
| --------- | ----- | --------------------------------- |
| `idx`     | j\|jl | Indices of `cond` to negate       |
| `cond`    | -     | Condition in k-tree form to alter |

Takes k-tree `cond` and prepends `idx` items with `not`.

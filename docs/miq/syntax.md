Syntax
===
---
Family of routines to evaluate naming, syntactical and other miQ conventions.

At the moment the suite is limited to naming conventions. Desired functionality includes:
* Code quality / coverage (i.e. how well are functions protected via type checking, use of hungarian notations, ...)
* Convention enforcement (i.e. no `\d` within functions, formal parameters are properly anotated or are in whitelist, ...)


**Errors**

| Message                               | Explanation                                       |
| ------------------------------------- | ------------------------------------------------- |
| `Name not a constant`                 | -                                                 |
| `Name not a literal`                  | -                                                 |
| `Name not an internal definition`     | -                                                 |
| `Name not a public definition`        | -                                                 |
| `Name not a handler`                  | -                                                 |
| `Name not a template`                 | -                                                 |
| `Name not a hook`                     | -                                                 |

**Examples**
```
q).miq.syntax.name.classify[`.miq.CONSTANT]
`constant`public

q).miq.syntax.name.classify[`.miq.z.require]
`literal`hook`public

q).miq.syntax.name.classify[`.miq.i.require]
`literal`internal

q).miq.syntax.name.classify `.miq.h.log.info
`literal`handler`public

q).miq.syntax.name.classify `.miq.t.emplate
`literal`template`public
```

## .miq.syntax.name.is.\*
Inspect the name and determines if it belongs to the semantic naming group using miQ naming standards. The name can be either of `s` or `ns` type.

| Function          | Example               | miQ convention                                |
|------------------ | --------------------- | --------------------------------------------- |
| `` `constant ``   | `` `.ns.CNST ``       | Upper case name                               |
| `` `literal ``    | `` `.ns.cnst ``       | Not a `constant`                              |
| `` `internal ``   | `` `.ns.i.fn ``       | Nested under `i` context                      |
| `` `public ``     | `` `.ns.fn ``         | Not an `internal`                             |
| `` `handler ``    | `` `.ns.h.fn ``       | Nested under `h` context                      |
| `` `template ``   | `` `.ns.t.fn ``       | Nested under `t` context                      |
| `` `hook ``       | `` `.ns.z.fn ``       | Nested under `z` context and not `z.dd`       |

## .miq.syntax.name.classify
| parameter | Type  | Description |
| --------- | ----- | ----------- |
| `sName`   | s\|ns | Name        |

Return `(variable,class,function)` classification of `sName` using [`.miq.syntax.name.is.\*`](miqsyntaxnameis) functions as described on this page.
* `variable` - one of `constant`, `literal`
* `class`    - one of `handler`, `template`, `hook` or `` `$() `` if not a hook or handler
* `function` - one of `internal`, `public`


Examples
========
--------

## Warn log level, no caller's name
```
q).log.info "1+1";
2020.11.15 | 00:15:37.653 | [INFO]  | <> | 1+1

q).log.config.set[`level;`warn];
q).log.config.set[`caller.enable;0b];
q).log.config.apply[];
q).log.info "1+1";
q).log.warn "1+1";
2020.11.15 | 00:13:46.357 | [WARN]  |  | 1+1

q).log.set.level`info;
q).log.info "1+1";
2020.11.15 | 00:15:15.067 | [INFO]  |  | 1+1
```

## Add a custom log level
```
q).log.level.trump:{.log.z.tally[x]; .log.z.print (.log.STDERR[];`trump;x); x}
q).log.config.get[`levels]
`debug`show`info`warn`error`fatal
q).log.config.set[`levels;`debug`show`info`warn`trump`error`fatal]
`debug`show`info`warn`trump`error`fatal
q).log.config.set[`level;`trump]
`trump
q).log.config.apply[]
2020.11.15 | 00:19:47.522 | [INFO]  | <.slot.module.load> | Loading module: `slot`name`extension!`SO`log`so
2020.11.15 | 00:19:47.537 | [INFO]  | <.miq.u.nCaller> | Setting log rotate mode to: (`day;1)
2020.11.15 | 00:19:47.539 | [INFO]  | <.miq.u.nCaller> | Setting log level mode to: `trump

q).log.warn "Can't see me!";
q).log.trump "Truth is a lie!";
2020.11.15 | 00:42:24.508 | [TRUMP] | <> | Truth is a lie!
q).log.error "Liar detected!";
2020.11.15 | 00:42:26.379 | [ERROR] | <> | Liar detected!
q)
```

## Count number of error messages
```
q).miq.hook.register[`.log.z.error;`error.count;{BigBadError+:1}]
`.log.z.error
q).log.error "Liar detected!";
2020.11.15 | 00:44:35.466 | [ERROR] | <> | Liar detected!
q).log.error "Liar detected!";
2020.11.15 | 00:44:38.211 | [ERROR] | <> | Liar detected!
q)BigBadError
2
```

## Manual log rotation to a new file
```
.log.z.roll[`:/tmp/new.log]
```



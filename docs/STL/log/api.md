API
===
---

## Initialisation
### .log.config.apply
* If `so.lib` of `so.class` exists, load it and pass into `.log.so.bind`
* If `file.directory` does not exist, and `dir.autocreate` is set to true, create it
* Initiate log rotation routines via `.log.rotate.init`
* Configure `.log.z.caller` hook as per `caller.enable` configuration item
* Populate default hook definitions of every implemented log level
* Set log level to `level`

### .log.init
* Apply configuration via `.log.config.apply`
* Automatically redirect output to a new log file if current output is stale, `/dev/null`, or differs from `.log.z.generate.file[]`.

### .log.rotate.init
Initiate & configure log rotation capabilities.

### .log.colour.init
| parameter     | Type      | Description             |
| ------------- | --------- | ----------------------- |
| `b`           | b         | True or false           |

Activate / deactivate default warn and error colouring.
* `warn`  - yellow, using `.log.colour.yellow` to post process `.log.z.message`
* `error` - red, using `.log.colour.red` to post process of `.log.z.message`

### .log.so.bind
| parameter     | Type      | Description             |
| ------------- | --------- | ----------------------- |
| `fpSo`        | fp        | Dynamic load projection |

Binds C functions to q names. `fpSo` has to accept a single argument of function's `(name;arity)`, and return a valid q object.
* `.log.so.i.redirect` - routine used by `.log.rotate.method.dup2` to enable log rotation across partitions.

## Checks
### .log.is.level
| parameter     | Type      | Description             |
| ------------- | --------- | ----------------------- |
| `x`           | s         | Log level               |

Return `1b` if `x` is a valid log levels as defined in `levels`.

### .log.is.rotate.mode
| parameter     | Type      | Description             |
| ------------- | --------- | ----------------------- |
| `x`           | s         | Rotate mode             |

Return `1b` if `x` is a valid rotate mode as defined in `rotate.modes`.

### .log.is.rotate.method
| parameter     | Type      | Description             |
| ------------- | --------- | ----------------------- |
| `x`           | s         | Rotate method           |

Return `1b` if `x` is a valid rotate method as defined in `rotate.methods`.

## Runtime configuration
### .log.set.date
| parameter     | Type      | Description           |
| ------------- | --------- | --------------------- |
| `dDate`       | d         | Date                  |

Set date used for logging.

### .log.set.level
| parameter     | Type      | Description           |
| ------------- | --------- | --------------------- |
| `sLevel`      | s         | Log level             |

Set logging to level `sLevel`.

### .log.set.rotate.mode
| parameter     | Type      | Description           |
| ------------- | --------- | --------------------- |
| `sMode`       | s         | Rotate mode           |
| `jThreshold`  | j         | Threshold             |

Set log rotation mode to `sMode` with frequency of `jThreshold`.

### .log.set.rotate.threshold
| parameter     | Type      | Description           |
| ------------- | --------- | --------------------- |
| `jThreshold`  | j         | Threshold             |

Set frequency of log rotation to `jThreshold`.

### .log.set.rotate.method
| parameter     | Type      | Description           |
| ------------- | --------- | --------------------- |
| `sMethod`     | s         | Rotate method         |

Set the method used to perform log rotation to `sMethod`.

## .log.level.\*
Log level implementations. To add a custom log level:
1. Define a new log level function under `.log.level`
1. Include the new log level in `levels` configuration
1. Re-run `.log.config.apply`

This will automatically generate a hook `.log.z.<LEVEL>` with an accessor `.log.<LEVEL>`.

> [!ATTENTION] Your new log level name can conflict with names already used within `.log` context. Make sure it does not happen.

## .log.rotate.mode.\*
Implements `tally` and `print` routines used for individual rotate modes.

### .log.rotate.mode.tally
| parameter     | Type      | Description           |
| ------------- | --------- | --------------------- |
| `clMessage`   | cl        | Log message           |

Enable log rotation according to `rotate.threshold`. Used as implementation of `.log.z.tally` hook.
* `.log.rotate.mode.none.tally` - Makes sure the log date is current, no log rotation
* `.log.rotate.mode.day.tally`  - If date changes more than `rotate.threshold` times, initiates log rotation
* `.log.rotate.mode.line.tally` - If number of lines printed out exceeds `rotate.threshold`, initates log rotation
* `.log.rotate.mode.size.tally` - If the size of log file exceeds `rotate.threshold`, initates log rotation

### .log.rotate.mode.print
| parameter     | Type      | Description           |
| ------------- | --------- | --------------------- |
| `jStream`     | j         | File descriptor       |
| `sSeverity`   | s         | Log level             |
| `clMessage`   | cl        | Log message           |

> [!NOTE] There is no distiction in the `print` implementation between individual rotate modes as of now.

Print out `clMessage` of log level `sSeverity` into `jStream`, as formatted by `.log.z.message`. Used as implementation of `.log.z.print` hook.

* `.log.rotate.mode.none.print`
* `.log.rotate.mode.day.print`
* `.log.rotate.mode.line.print`
* `.log.rotate.mode.size.print`

## .log.rotate.method.\*
| parameter     | Type      | Description           |
| ------------- | --------- | --------------------- |
| `fsFile`      | fs        | File to rotate to     |

Routines used to perform log rotation on disk. Used as implementation of `.log.z.rotate` hook.
* `.log.rotate.method.inode` - Rotate log file using the fact that renaming an open file will keep its file descriptor. Limited to log rotation within a single partition.
* `.log.rotate.method.dup2`  - Rotate log file using C `dup2` calls. Requires C library to be loaded, see [.log.so.bind](#logsobind).

## Hooks
### .log.z.generate.name
| parameter     | Type      | Description           |
| ------------- | --------- | --------------------- |
| `x`           | none\|qd  | None or qd of options |

Generate the log name. If `x` is a dict, treat it as an output of `.Q.opt`, otherwise call `.Q.opt` on its own.

### .log.z.generate.file
| parameter     | Type      | Description           |
| ------------- | --------- | --------------------- |
| `x`           | none\|s   | File name             |

Generate log file name. If `x` is a symbol, use it as a log name, otherwise use `LOG.FILE` as generated by `.log.z.generate.name`.

### .log.z.tally
One of [log rotation mode implementations](#tally).

### .log.z.print
One of [log rotation mode implementations](#print).

### .log.z.rotate
One of [log rotation method implementations](#logrotatemethod).

### .log.z.caller
| parameter     | Type      | Description           |
| ------------- | --------- | --------------------- |
| `sSeverity`   | s         | Log level             |

When `caller.enable` is false, use default definition of `{""}`, otherwise employ `.log.caller.hook`. `sSeverity` is used to index into `L.CALLER.DEPTH`, which holds information on how many frames are are required to jump up to reach `.log.<LEVEL>` caller.

### .log.z.message
| parameter     | Type      | Description           |
| ------------- | --------- | --------------------- |
| `sSeverity`   | s         | Log level             |
| `clMessage`   | cl        | Log message           |

Compose full log line for `clMessage`, adhering to `sSeverity`. There are multiple parts to every log line, joined together by `delimiter`, namely:
* date
* time
* severity
* caller (if `caller.enable` is true)
* log message


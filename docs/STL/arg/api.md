API
===
---

## Checks
### .arg.is.option
| parameter    | Type | Description                 |
| ------------ | ---- | --------------------------- |
| `sOpt`       | s    | Option name                 |

Return `1b` if `sOpt` is a defined option.

### .arg.is.supplied
| parameter    | Type | Description                 |
| ------------ | ---- | --------------------------- |
| `sOpt`       | s    | Option name                 |

Return `1b` if `sOpt` is supplied.

### .arg.is.mandatory
| parameter    | Type | Description                 |
| ------------ | ---- | --------------------------- |
| `sOpt`       | s    | Option name                 |

Return `1b` if `sOpt` is mandatory.

### .arg.is.optional
| parameter    | Type | Description                 |
| ------------ | ---- | --------------------------- |
| `sOpt`       | s    | Option name                 |

Return `1b` if `sOpt` is optional.

### .arg.is.alias
| parameter    | Type | Description                 |
| ------------ | ---- | --------------------------- |
| `sAlias`     | s    | Alias name                  |

Return `1b` if `sAlias` is a defined alias for an option.

## .arg.mandatory.\*
### .arg.mandatory.list
Return definition of all options that are mandatory.

### .arg.mandatory.add
| parameter    | Type | Description                 |
| ------------ | ---- | --------------------------- |
| `sOpt`       | s    | Option name                 |

Configures option `sOpt` as mandatory.

### .arg.mandatory.remove
| parameter    | Type | Description                 |
| ------------ | ---- | --------------------------- |
| `sOpt`       | s    | Option name                 |

Remove mandatory configuration of option `sOpt`, making it optional.

## .arg.alias.\*
### .arg.alias.list
| parameter    | Type | Description                 |
| ------------ | ---- | --------------------------- |
| `sAlias`     | s    | Alias name                  |

If `sAlias` is `None`, return a table of aliases, their cast type and option mapped to. If `sAlias` is a valid alias, return an entry only for `sAlias`.

### .arg.alias.resolve
| parameter    | Type | Description                 |
| ------------ | ---- | --------------------------- |
| `sAlias`     | s    | Alias name                  |

Translate `sAlias` to its option.

### .arg.alias.add
| parameter    | Type | Description                 |
| ------------ | ---- | --------------------------- |
| `sOpt`       | s    | Option name                 |
| `sAlias`     | s    | Alias name                  |

Add `sAlias` as aliasto `sOpt` option and return `sAlias`.

### .arg.alias.remove
| parameter    | Type | Description                 |
| ------------ | ---- | --------------------------- |
| `sAlias`     | s    | Alias name                  |

Remove `sAlias` definition and return `sAlias`.

## .arg.assert
Verify that all mandatory arguments have been supplied and throw a signal on failure.

## .arg.usage
| parameter         | Type      | Description               |\*| Interface          | Purpose                                   |
| ----------------- | --------- | ------------------------- |--| ------------------ | ----------------------------------------- |
| `clDescription`   | none\|cl  | Program description       |\*| `` `cl ``          | Description only                          |
| `clUsage`         | none\|cl  | Program synopsis          |\*| `` `sl ``          | Options only                              |
| `slOpt`           | none\|sl  | Option name list          |\*| `` ` ``            | Every parameter takes on default values   |
|                   |           |                           |\*| `` `cl`cl ``       | Description and usage supplied            |
|                   |           |                           |\*| `` `cl`sl ``       | Description and options supplied          |
|                   |           |                           |\*| `` `cl`cl`sl ``    | Description, usage and options supplied   |
|                   |           |                           |\*| `  ```  `          | Pass any arguments directly               |

Prints out program description and simple usage followed by calling `.arg.help[]` using `slOpt` as argument. If `clUsage` is `none`, program description will simply state that it is missing.

## .arg.help
| parameter    | Type       | Description                 |
| ------------ | ---------- | --------------------------- |
| `slOpt`      | none\|sl   | Option name list            |

Print out elementary help text for `slOpt` options, split into *mandatory* and *optional* segments. Each line has option name, its aliases, type of argument it takes and description of the option itself. All information is sourced from `OPTS`. If `slOpt` is `none`, default to all *user-defined* options.

## .arg.set
| parameter    | Type | Description                 |
| ------------ | ---- | --------------------------- |
| `sOpt`       | s    | Option name                 |
| `content`    | -    | Any value                   |

Set argument of option `sOpt` to `content`. Verify `content` is of correct type, as per option configuration.

## .arg.inject
| parameter    | Type | Description                 |
| ------------ | ---- | --------------------------- |
| `sOpt`       | s    | Option name                 |
| `content`    | -    | Any value                   |

Injects a known and unsupplied option `sOpt` with argument `content`. Verify `content` is of correct type, as per option configuration.

## .arg.get
| parameter    | Type | Description                 |
| ------------ | ---- | --------------------------- |
| `sOpt`       | s    | Option name                 |

Retrieve argument of option `sOpt`.

## .arg.describe
| parameter       | Type | Description                 |
| --------------- | ---- | --------------------------- |
| `sOpt`          | s    | Option name                 |
| `clDescription` | cl   | Option description          |

Add description `clDescription` to option `sOpt` and return `sOpt`.

## .arg.add
| parameter    | Type | Description                 |
| ------------ | ---- | --------------------------- |
| `sOpt`       | s    | Option name                 |
| `sCast`      | s    | Casting type                |

Add a new option definition `sOpt` of type `sCast` and return `sOpt`.

## .arg.remove
| parameter    | Type | Description                 |
| ------------ | ---- | --------------------------- |
| `sOpt`       | s    | Option name                 |

Remove definition of `sOpt` option and return `sOpt`.

## .arg.process
| parameter    | Type | Description                 |
| ------------ | ---- | --------------------------- |
| `zx`         | cll  | `.z.x`-like list of strings |

Call `.arg.parse` on `zx` and appends the output to `ARGS`.

## .arg.parse
| parameter    | Type | Description                 |
| ------------ | ---- | --------------------------- |
| `zx`         | cll  | `.z.x`-like list of strings |

Parse `zx` and output `opt!arg` dictionary of options and their processed arguments. If `zx` is `NONE`, default to `.z.X`. It will verify option arguments after casting to confirm they are of expected type.

## .arg.compose
| parameter    | Type | Description                 |
| ------------ | ---- | --------------------------- |
| `qdOpts`     | qd   | `opt!arg` dict              |

Translate a dictionary of options `qdOpts` to a string that can be supplied as arguments to a q script. Options will be parsed according to their defined casting type.

## Hooks
### .arg.z.message
| parameter    | Type | Description                 |
| ------------ | ---- | --------------------------- |
| `sOpt`       | s    | Option name                 |
| `sCast`      | s    | Casting type                |
| `arg`        | -    | Casted option argument      |

Log out message when option's `sOpt` argument `arg` is not of expected `sCast` type. When `strict` is disabled, logs a warning, otherwise logs an error and throw a signal.

### .arg.z.parse.\*
* atom, list, range
| parameter    | Type | Description                 |
| ------------ | ---- | --------------------------- |
| `sCast`      | s    | Casting type                |
| `clArg`      | cl   | Option argument as string   |

Generic parser to cast option arguments `clArg` to `sCast` type. Used in absence of type specific parse hook.

* b, bl, fs, fsl, cs, csl
| parameter    | Type | Description                 |
| ------------ | ---- | --------------------------- |
| `clArg`      | cl   | Option argument as string   |

Type specific hook to cast option argument `clArg`.

### .arg.z.compose.\*
* atom, list, range
| parameter    | Type | Description                 |
| ------------ | ---- | --------------------------- |
| `sCast`      | s    | Casting type                |
| `arg`        | -    | Casted option argument      |

Generic algorithm to reverse parse casting operation on option argument `arg` of type `sCast`. Used in absence of type specific compose hook.

* b, bl, fs, fsl, cs, csl
| parameter    | Type | Description                 |
| ------------ | ---- | --------------------------- |
| `arg`        | -    | Casted option argument      |

Type specific hook to reverse parse casting operation on option argument `arg`.

### .arg.z.is.\*
| parameter    | Type | Description                 |
| ------------ | ---- | --------------------------- |
| `sOpt`       | s    | Option name                 |
| `arg`        | -    | Casted option argument      |

This set of hooks is implementing type checking for options.


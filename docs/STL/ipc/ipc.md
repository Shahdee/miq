ipc
===
---
Module handling inter-process communication duties.

## Configuration
| Name                | Type    | Default               | Definition                                                |
| ------------------- | ------- | --------------------- | --------------------------------------------------------- |
| `timeout`           | `j`     | `5000`                | Connection timeout                                        |
| `flush`             | `b`     | `0b`                  | Flush, or not to flush?                                   |
| `hostname`          | `s`     | `.z.h`                | Default hostname for address creation                     |
| `port.range`        | `jl`    | `0 65535`             | Valid port range                                          |
| `port.unprivileged` | `j`     | `1024`                | Lowest unpriviledged port                                 |
| `service.file`      | `fs`    | `` `:/etc/services``  | Path to services definition file                          |
| `service.strict`    | `b`     | `1b`                  | Strict syntax check of service names                      |

## Constants
| Name              | Definition                                            |
| ----------------- | ----------------------------------------------------- |
| `LISTENER`        | Last open listener port                               |
| `SERVICES`        | Details of services sourced from `service.file`       |
| `INTERFACES`      | Available network interfaces and their attributes     |
| `HANDLES`         | Information on handles, both current and past         |
| `SERVICE.LIST`    | List of defined services and their aliases            |

## Table templates
* `TT.SERVICES`
| name | protocol | port | alias |
| ---- | -------- | ---- | ----- |
| s    | s        | j    | sl    |

* `TT.INTERFACES`
| ifname | operstate | family | scope | address | hostname |
| ------ | --------- | ------ | ----- | ------  | -------- |
| s      | s         | s      | s     | s       | s        |

* `TT.HANDLES`
| handle |!| address | openSelf | openTime | closeSelf | closeTime |
| ------ |-| ------- | -------- | -------- | --------- | --------- |
| i      |!| s       | b        | p        | b         | p         |

## Accessors
| Name                      | target                | Note                                                      |
| ------------------------- | --------------------- | --------------------------------------------------------- |
| `.ipc.listener`           | `LISTENER`            | -                                                         |
| `.ipc.services`           | `SERVICES`            | -                                                         |
| `.ipc.interfaces`         | `INTERFACES`          | -                                                         |
| `.ipc.handles`            | `HANDLES`             | Filters on input, see [`.ipc.handles`](api#ipchandles)    |
| `.ipc.port.range`         | `PORT.RANGE`          | -                                                         |
| `.ipc.port.unprivileged`  | `PORT.UNPRIVILEGED`   | -                                                         |

## System commands
| Command   | Used by                                   | Comment                                                           |
| --------- | ----------------------------------------- | ----------------------------------------------------------------- |
| `ip`      | `.ipc.i.interfaces`, `.ipc.i.ipaddr`      | Required to generate `INTERFACES`                                 |
| `lsof`    | `.ipc.handle.i.lsof`, `.ipc.port.i.lsof`  | `.ipc.handle.info` and `.z.po` will not work                      |
| `bash`    | `.ipc.test`                               | Required to test whether connection to an address is open already |
| `timeout` | `.ipc.test`                               | ===||===                                                          |
| `echo`    | `.ipc.test`                               | ===||===                                                          |

## Errors
| Message                       | Explanation                                                                       |
| ----------------------------- | --------------------------------------------------------------------------------- |
| `No matching address`         | Attempt to disconnect from an unknown address                                     |
| `lsof failed for handle`      | I am sorry if you see this, you will have to debug `.ipc.handle.i.lsof`           |
| `Not a valid port number`     | Port number not within `PORT.RANGE`                                               |
| `Not a valid service name`    | No such service name with TCP interface defined in `SERVICE.FILE`                 |
| `Not a valid handle`          | Connection handle does not exist                                                  |
| `Not a valid IPC address`     | Invalid address, see its definition at [`.ipc.is.address`](api#ipcisaddress)      |
| `Not a valid IPC listener`    | Invalid listener, see its definition at [`.ipc.is.listener`](api#ipcislistener)   |


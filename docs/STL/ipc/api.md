API
===
---

## Checks
### .ipc.is.port
| parameter    | Type         | Description           |
| ------------ | ------------ | --------------------- |
| `x`          | whole        | Port number           |

Return `1b` if `x` is within [`port.range`](ipc#configuration), `0b` otherwise.

### .ipc.is.service
| parameter    | Type         | Description           |
| ------------ | ------------ | --------------------- |
| `x`          | s            | Service name          |

Return `1b` if `x` is a known service name as defined by [`service.file`](ipc#configuration), `0b` otherwise.

### .ipc.is.interface
| parameter    | Type         | Description           |
| ------------ | ------------ | --------------------- |
| `x`          | s            | Interface name        |

Return `1b` if `x` is a known network interface on current machine, `0b` otherwise.

### .ipc.is.handle
| parameter    | Type         | Description           |
| ------------ | ------------ | --------------------- |
| `ch`         | whole        | Connection handle     |

Return `1b` if `ch` is a valid connection handle, `0b` otherwise.

### .ipc.is.address
| parameter    | Type         | Description           |
| ------------ | ------------ | --------------------- |
| `x`          | -            | Address               |

Return `1b` if `x` is one of:
* valid service name \| `s`
* valid port number \| `whole`
* connection symbol with valid service name or port number \| `cs`
* character list \| `cl`
* (rp;host;service|port) \| `(b;s;s|whole)`
* (rp;service|port) \| `(b;s|whole)`

### .ipc.is.listener
| parameter    | Type         | Description           |
| ------------ | ------------ | --------------------- |
| `x`          | `cl`         | Listener              |

Return `1b` if `x` is a listener-like string. Host name is verified via [`.ipc.syntax.host`](#ipcsyntaxhost), port via [`.ipc.is.port`](#ipcisport) and service via [`.ipc.syntax.service`](#ipcsyntaxservice).

## Casting
### .ipc.to.address
| parameter    | Type         | Description           |
| ------------ | ------------ | --------------------- |
| `x`          | -            | Address               |

Transform service name, port number, connection symbol or `address` as describe under [`.ipc.is.address`](#ipcisaddress) to a connection symbol.

### .ipc.to.listener
| parameter    | Type         | Description           |
| ------------ | ------------ | --------------------- |
| `x`          | -            | Address               |

Transform service name, port number, connection symbol or `address` as describe under [`.ipc.is.address`](#ipcisaddress) to a listener string, ready to be supplied to `\p` command.

## Naming syntax
### .ipc.syntax.host
| parameter    | Type         | Description           |
| ------------ | ------------ | --------------------- |
| `x`          | s\|cl        | Host name             |

Return `1b` upon verification that the host name is conforming to **Preferred name syntax** as specified by [RFC 1035](https://tools.ietf.org/html/rfc1035#section-2.3.1).
* Max 253 characters long
* Only letters, digits and hyphen character allowed
* Every label (dot delimited parts) must be at most 63 character long, start with a letter, and end with a letter or digit

### .ipc.syntax.service
| parameter    | Type         | Description           |
| ------------ | ------------ | --------------------- |
| `x`          | s\|cl        | Service name          |

Return `1b` upon berification that the service name is conforming to **Service Name Syntax** as specified by [RFC 6335](https://tools.ietf.org/html/rfc6335#section-5.1).
* Max 15 characters long
* Must not begin or end with a hyphen
* Only letters, digits and hyphen character allowed
* Must contain at least one letter
* Hyphens must not be adjacent to other hyphens

> [!WARNING] Minority of service names, notably their aliases, are still non-conforming and will fail this check. Set `service.strict` to `0b` if you cannot avoid using non-conforming service names.

## .ipc.listen
| parameter    | Type         | Description           |
| ------------ | ------------ | --------------------- |
| `port`       | -            | Port to listen on     |

Change listener to `port` using native `\p` command and return listener string. Accepts (any of):
* `s` - service name, checked via [`.ipc.is.service`](#ipcisservice)
* `cl` - has to conform to `-p` command line option
* `whole` - a simple numeric value
* `address` - IPC address `(b;s;j|s)`

## .ipc.test
| parameter    | Type         | Description           |
| ------------ | ------------ | --------------------- |
| `address`    | -            | Listener to test      |

Return `1b` when able to establish a TCP connection to `address`, `0b` otherwise. If host name is not defined, use `.z.h`.

## .ipc.connect
| parameter    | Type           | Description           |
| ------------ | -------------- | --------------------- |
| `address`    | -              | Address to connect to |

Attempts connection to `address` using default [`TIMEOUT`](ipc#configuration). To override, supply a 2-item list of `(address;timeout)`, where `timeout` is a `whole` number.

## .ipc.disconnect
| parameter    | Type           | Description                |
| ------------ | -------------- | -------------------------- |
| `address`    | -              | Address to disconnect from |

Disconnect from `address`, which can be of any form accepted by [`.ipc.handles`](#ipchandles) accessor, which is used to obtain connection handles to close.

## .ipc.oneshot
| parameter    | Type | Description           |
| ------------ | ---- | --------------------- |
| `address`    | -    | Address to connect to |
| `msg`        | -    | Valid handle message  |

One-shot `msg` to `address`.

## .ipc.sync
| parameter    | Type       | Description           |
| ------------ | ---------- | --------------------- |
| `ch`         | ch\|chl    | Connection handle     |
| `msg`        | -          | Valid handle message  |

Send `msg` to handle `ch` synchronously.

## .ipc.async
| parameter    | Type       | Description           |
| ------------ | ---------- | --------------------- |
| `ch`         | ch\|chl    | Connection handle     |
| `msg`        | -          | Valid handle message  |

Send `msg` to handle `ch` asynchronously.

## .ipc.flush
| parameter    | Type       | Description           |
| ------------ | ---------- | --------------------- |
| `ch`         | ch\|chl    | Connection handle     |

Flush asynchronous messages on handle `ch`.

## .ipc.handles
| parameter    | Type                   | Description           |
| ------------ | ---------------------- | --------------------- |
| `x`          | none\|b\|i\|j\|cs\|s   | Connection descriptor |

Return a (subset of) [`HANDLES`](ipc#constants). When `x` is of type:
* `none` - return all connections
* `b` - return only connections that have been opened or closed by this process if `x` is true, exact opposite otherwise
* `i|j` - return only connections whose connection handle matches `x`
* `cs` - return only connections whose address matches connection symbol `x`
* `s` - return only connections whose address contains symbol `x`

> [!TIP] When matching processes running on ports 5000 and 5001, use symbol `` `500``. If you intend to match port exactly, use long `5000`.

## .ipc.port
### .ipc.port.is.open
| parameter    | Type         | Description           |
| ------------ | ------------ | --------------------- |
| `x`          | j            | Port number           |

Return `1b` if `x` is currently open on the box and in LISTEN state, `0b` otherwise.

### .ipc.port.is.free
| parameter    | Type         | Description           |
| ------------ | ------------ | --------------------- |
| `x`          | j            | Port number           |

Return `1b` if `x` is available to be used on the box, `0b` otherwise.

### .ipc.port.is.privileged
| parameter    | Type         | Description           |
| ------------ | ------------ | --------------------- |
| `x`          | j            | Port number           |

Return `1b` if `x` is privileged, `0b` otherwise.

### .ipc.port.is.unprivileged
| parameter    | Type         | Description           |
| ------------ | ------------ | --------------------- |
| `x`          | j            | Port number           |

Return `1b` if `x` is unprivileged, `0b` otherwise.

### .ipc.port.list.all
| parameter    | Type         | Description           |
| ------------ | ------------ | --------------------- |
| `x`          | j            | Port number           |

Return a list of all valid ports on the box, as defined by `port.range`, that are greater than or equal to `x`. If `x` is `None`, return as is.

### .ipc.port.list.open
| parameter    | Type         | Description           |
| ------------ | ------------ | --------------------- |
| `x`          | j            | Port number           |

Return a list of all ports currently in LISTEN state that are greater than or equal to `x`. If `x` is `None`, return as is.

> [!NOTE] Won't show ports opened by other users.

### .ipc.port.list.free
| parameter    | Type         | Description           |
| ------------ | ------------ | --------------------- |
| `x`          | j            | Port number           |

Return a list of all available ports that are greater than or equal to `x`. If `x` is `None`, return as is.

### .ipc.port.range
Return a list of two longs representing full port range of the host, as per `port.range` configuration item.

### .ipc.port.unprivileged
Return the lowest unprivileged port number, as per `port.unprivileged` configuration item.

### .ipc.port.random
| parameter    | Type         | Description           |
| ------------ | ------------ | --------------------- |
| `j`          | j            | Number of ports       |
| `constraint` | none\|j\|jl  | Port range constraint |

Return list of `j` available port numbers, constrained by `constraint`:
* `none` - use full port range
* `j` - lower boundary for ports
* `jl` - both lower and upper boundary for ports

### .ipc.port.table
Return a table of processes listening on TCP port.

## .ipc.handle.\*
### .ipc.handle.list
List of currently open handles, as supplied by `.z.W`.

### .ipc.handle.info
| parameter    | Type         | Description           |
| ------------ | ------------ | --------------------- |
| `ch`         | whole        | Connection handle     |

Return info on connection handle `ch` obtained via `lsof` system command.

### .ipc.handle.host
| parameter    | Type         | Description           |
| ------------ | ------------ | --------------------- |
| `ch`         | whole        | Connection handle     |

Shortcut to obtain host from `.ipc.handle.info`.

### .ipc.handle.port
| parameter    | Type         | Description           |
| ------------ | ------------ | --------------------- |
| `ch`         | whole        | Connection handle     |

Shortcut to obtain port from `.ipc.handle.info`.

### .ipc.handle.refresh
Update the [`HANDLES`](ipc#constants) internal state with open handles that are currently missing from it. That could happen if we have opened a connection circumventing the ipc calls, for example.

## .ipc.po
| parameter    | Type | Description           |
| ------------ | ---- | --------------------- |
| `handle`     | -    | Open handle           |

`.z.po` hook to update [`HANDLES`](ipc#constants). Will override values if `handle` is being re-used.

## .ipc.pc
| parameter    | Type | Description           |
| ------------ | ---- | --------------------- |
| `handle`     | -    | Just closed handle    |

`.z.pc` hook to update [`HANDLES`](ipc#constants).


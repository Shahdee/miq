prof
===
---
Profiling module. Provides capabilities to measure function runtime and it's share of load compared to other profiled functions.

## Configuration
> [!ATTENTION] Enabling storing and tracking of additional information will cause the execution to take longer, adding time to run additional profiler code to parent's `own` runtime, and potentially distorting the proprotional time relationship between functions.

| Name                | Type    | Default                   | Definition                                |
| ------------------- | ------- | ------------------------- | ----------------------------------------- |
| `stack`             | `b`     | `0b`                      | Store stack trace                         |
| `parent`            | `b`     | `0b`                      | Store parent                              |
| `depth`             | `b`     | `0b`                      | Store depth of the execution tree         |
| `user`              | `b`     | `0b`                      | Store user                                |
| `context`           | `b`     | `0b`                      | Store context (after function execution)  |
| `input`             | `b`     | `0b`                      | Store function input                      |
| `exclude`           | `sl`    | `.prof.config.exclude[]`  | List of functions excluded from profiling |

## Constants
| Name             | Definition                                 |
| ---------------- | ------------------------------------------ |
| `ORIGINAL`       | Function definitions before profiling      |
| `RUN.ID`         | Current run number                         |
| `RUN.STEP`       | Last completed step of current run ID      |
| `RUN.ITER`       | Run step number iterator                   |
| `RUN.ERROR`      | Last exception thrown                      |
| `UNWINDING`      | Flag controlling graceful stack unwinding  |
| `RUNNING`        | Current execution stack                    |
| `FUNCTION`       | History of all past executions             |
| `TOTAL`          | Last calculation of profiling stats        |

## Hooks
| Name           | Default implementation             | Description                                              |
| -------------- | ---------------------------------- | -------------------------------------------------------- |
| `.prof.z.exec` | [`.prof.z.dd.exec`](api#profzexec) | Wrap around profiled functions and act as an entry point |

## Table templates
* `TT.RUNNING`
| n   |!| function | child |
| --- |-| -------- | ----- |
| j   |!| s        | n     |

* `TT.FUNCTION`
| id  | step |!| function | stack | parent | depth | user | context | input | start | end | total | own |
| --- | ---- |-| -------- | ----- | ------ | ----- | ---- | ------- | ----- | ----- | --- | ----- | --- |
| j   | j    |!| s        | \*    | s      | j     | s    | s       | \*    | p     | p   | n     | n   |

* `TT.TOTAL`
| name |!| runs | time | ratio |
| ---- |-| ---- | ---- | ----- |
| s    |!| j    | n    | f     |

## Accessors
| Name             | target             | Note                                         |
| ---------------- | ------------------ | -------------------------------------------- |
| `.prof.enabled`  | `ORIGINAL`         | List of profiled functions                   |
| `.prof.original` | `ORIGINAL`         | Original definition of a profiled function   |
| `.prof.excluded` | `EXCLUDE`          | List of functions excluded from profiling    |
| `.prof.runs`     | `FUNCTION`         | Every step of every profiled execution       |
| `.prof.total`    | `TOTAL`            | Returns stats, recalculating them if need be |

## Errors
| Message                      | Explanation                                                         |
| ---------------------------- | ------------------------------------------------------------------- |
| `Profiling not enabled`      | Attempt to disable profiling on function that is not being profiled |


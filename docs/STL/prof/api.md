API
===
---

## .prof.enable
| parameter    | Type | Description           |
| ------------ | ---- | --------------------- |
| `nsFunction` | ns   | Context or name       |

Enable profiling of `nsFunction`. If `nsFunction` is a context, expand via `.prof.expand[]` call.

## .prof.disable
| parameter    | Type | Description           |
| ------------ | ---- | --------------------- |
| `nsFunction` | ns   | Context or name       |

Disable profiling of `nsFunction`. If `nsFunction` is a context, expands via `.prof.expand[]` call. Print out a warning if the name being disabled is not currently set to be profiled.

## .prof.expand
| parameter    | Type | Description           |
| ------------ | ---- | --------------------- |
| `nsFunction` | ns   | Context or name       |

Return list of namespace names that are a valid target of profiler. If `nsFunction` that is an existing context, expand it to the list of names, or if it is a name, simply use it. Then filter out names that are a valid target of profiling, which currently include types: `` `op`fn`fp`qc`qi ``.

## .prof.reset
Disable all profiling and revert function definitions to their original form.

## .prof.clear
Clears out all data captured by profiler.

## .prof.run.\*
> [!TIP] Automatically excluded from profiling.

Utility functions used by hooks implementing profiler.

* id - current run's ID
* step - current run's step number
* depth - execution stack depth
* error - last thrown exception
* reset - clean the runtime values, useful when profiled function fails

## Hooks
### .prof.z.exec
| parameter    | Type | Description           |
| ------------ | ---- | --------------------- |
| `nsFunction` | ns   | Context or name       |
| `bUnary`     | b    | What it says          |
| `args`       | -    | Input to `nsFunction` |

Wrap around the profiled function, tracking time and space it took for the function to execute.

### .prof.extend.\*
> [!TIP] When implementing your own function, define it in `.prof.extend` context, as all its names are automatically excluded from profiling. You will have to add it to `exclude` list manually.

> [!ATTENTION] You will see `'stack` error if your custom function includes calls to profiled functions or you have not excluded it itself from profiling!

Implemented extensions to `.prof.z.exec`. All functions are required to have the same formal parameters as `.prof.z.exec`.
* stack - capture stack trace of function as is just after function execution
* parent - capture name of the parent function
* depth - capture depth of stack
* user - capture name of user as it was just after function execution
* context - capture context as it was just after function execution
* input - capture input to the function

## Accessors
### .prof.enabled
Return a list of names that are currently profiled.

### .prof.original
| parameter    | Type | Description           |
| ------------ | ---- | --------------------- |
| `ns`         | ns   | Name address          |

Return original definition of currently profiled name.

### .prof.excluded
Return a list of names that are exempt from being profiled.

### .prof.runs
| parameter    | Type                   | Description           |
| ------------ | ---------------------- | --------------------- |
| `id`         | none\|whole\|wholeList | ID of execution run   |

Return all execution steps of run(s) identified by `id`. If `id` is `none`, return all steps of all recorded runs.

### .prof.total
| parameter    | Type                   | Description           |
| ------------ | ---------------------- | --------------------- |
| `id`         | none\|whole\|wholeList | ID of execution run   |

Return time taken by functions of all execution steps of run(s) identified by `id`, sorted by relative runtime each of them has cumulatively taken. If `id` is `none`, it will work on all recorded runs.


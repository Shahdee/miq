Standards
===
---
## Context naming & organisation
* Module named `foo` is defined and executed within `.foo` namespace
* Functions internal to module have `i` as part of context name, i.e. `.foo.i.fn[]`
* Every non-internal definition is considered public

## Blackboard
* For module `foo` and blackboard root `.bb`, module's constants are stored under `.bb.foo`
* Module should store all runtime values on blackboard
* Values stored on blackboard are retrieved via constants or `.miq.bb.get` call
* Don't hijack blackboard space of other modules or a blackboard police will knock on your doors

## Constants
* Define constants at the top of the module's code
* Visible to, and usable by, all functions executing under module's context
* For interpreted configuration items, we use `C.CONSTNAME` notation
* For object class names in use, we use `O.CLASSNAME` notation
* For transient and / or frequently accessed values we can use `L.CONSTNAME` notation
  * If there's a value that is re-used extremely but rarely changed, this can slightly improve performance
* Meta constants:
  * `M.FAMILY`    - module's family
  * `M.NAME`      - module's name
  * `M.VERSION`   - module's version
  * `M.VARIABLES` - list of environment variables the module depends on
  * `M.COMMANDS`  - list of system commands the module depends on
  * `M.MODULES`   - list of modules the module depends on, in the form of (`family.name;version)`
  * `M.OBJECTS`   - list of miQ objects the module depends on
* Special local values:
  * `SELF` (local) - module's context
  * `BB` (local)   - module's blackboard address
  * `TT` (local)   - subcontext holding table templates, which are literally empty tables serving as template for actual constants
* Configuration constants:
  * `CFG.DEFAULT`  - address of modules's default configuration
  * `CFG.EXPLICIT` - address of module's user configuration
  * `CFG.APPLIED`  - address of modules's applied configuration
  * `CFG.CONSTANT` - address of module's constant namespace
* Standard constants:
  * `INIT` - constant holds module's initialisation status and set to `0b` on every (re-)load
  * `WARM` - Unresolvable (cold) on first run, subsequently resolves to `1b` forever after

## Configuration
* For module `foo` and configuration root `.cfg`, module's explicit (user) configuration is stored under `.cfg.foo`
* For module `foo`, routines implementing internal module configuration are nested under `.foo.config` context
  * `.foo.config.default` holds default values of configuration items
  * `.foo.config.set[]` changes the configuration item's value
  * `.foo.config.get[]` retrieves the configuration item's value
  * `.foo.config.apply[]` applies current configuration
  * `.foo.config.assemble[]` will show you what value the configuration item will take on if applied now
  * Explicit configuration takes precedence over default configuration
* Every configuration item will have its own constant named after it
  * i.e. configuration item `eat.cake` can be accessed through `C.EAT.CAKE` constant in modules namespace `.foo`
* For default configuration requiring more code, define the function serving that purpose under `.foo.config.i` subcontext

## Initialisation
* Module `foo` is initialised using `.foo.init[]` function, which accepts either a (projected) function or `None` as argument
* Input is used to load additional configuration and is processed through a standard `.miq.cfg.loader[]` function
* Execution sequence
  1. Import handlers
  1. Generate warn/error functions for checks
  1. For modules, export the public API
  1. Handle constant initialisation
  1. Load module configuration through `.miq.cfg.loader[]`
  1. Apply module configuration through `.foo.config.apply[]`

## Objects
* Formal definition of complex variables
* Imported into `o` subcontext right after object class definition for convenience

## Handlers
* Primary means of dynamic dependency resolution
* Interface neutral access to commong code
* Handlers are imported to `.foo.h` namespace by default
  * Example: Instead of calling `.log.info`, call `.foo.h.log.info`

## Accessors
* Wrapper functions providing access predominantly to constants and hooks
* Sometimes provide access to functions nested further down the directory tree that could beenfit from a shorthand

## Checks
* Functions testing general properties of objects related to module's purpose
* Don't throw signal on invalid input
* Nested under `*.is.*` subcontext

## Templates
* Efficient way to split functions into multiple code paths based on the execution context
* Avoid some, sometimes even all, type checking on formal parameters


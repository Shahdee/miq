Reading guide
=============
---
Module documentation utilises standard structure to present information uniformly. Not every module needs or makes use of all of below coding practices, but if it does, this is what they mean.

> [!ATTENTION] Be mindful that a constant is created for every configuration item!

| Section           | Purpose                                                                                                                                           |
| ----------------- | ------------------------------------------------------------------------------------------------------------------------------------------------- |
| *Configuration*   | List of configuration items and their types, default values, and meaning.                                                                         |
| *Constants*       | Enumeration of constants independent of configuration and their meaning. [Standards page](standards#configuration) can act as a quick start read. |
| *Table templates* | List of table templates and their schemas. Keyed tables have a column filled with exclamation marks to demarcate key of the table.                |
| *Hooks*           | Overview of implemented hooks together with their default implementation and purpose description.                                                 |
| *Accessors*       | List of accessors including their name, target, and a description                                                                                 |

Every significant section of module's documentation will also contain:

| Section    | Purpose                                                     |
| ---------- | ----------------------------------------------------------- |
| *Errors*   | List of error messages and their explanation.               |
| *Examples* | Code snippet with exemplary usage of implemented functions. |

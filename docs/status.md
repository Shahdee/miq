Project status
===
---

Status:
* Good - in great condition
* Working - in good condition, but more work earmarked for this code
* Bad - outdated, needs a complete rehaul
* Broken - either not functional or if by a chance it is, it does not work as intended
* Stub - does not do a lot at the moment

## miQ Kernel
| Name                      | Status    | Tested    | Note                                                          |
| ------------------------- | --------- | --------- | ------------------------------------------------------------- |
| -                         | Good      | Yes       | Functions nested directly under `.miq` context                |
| Console                   | Working   | No        | Probably will be moved to a separate module                   |
| Colour                    | Good      | No        | Limited use, interface should be stable                       |
| System                    | Good      | Yes       | Limited use, interface should be stable                       |
| File descriptor           | Good      | Yes       | Underpins fd and fh miQ types                                 |
| Module                    | Working   | No        | Working well, but may be upgraded in the future               |
| Blackboard                | Working   | No        | Not used as much as I would like it to be, may need upgrade   |
| Configuration             | Good      | No        | Used extensively, interface stable                            |
| Functional query          | Stub      | Yes       | Requires more work to make it more usefull                    |
| Namespace                 | Good      | Yes       | Decent usage, interface likely stable                         |
| Filesystem                | Good      | No        | Decent usage, interface likely stable                         |
| Temporary storage         | Working   | No        | Pending rework into a separate module                         |
| QK                        | Good      | No        | Used extensively, interface likely stable                     |
| Syntax                    | Stub      | No        | Niche use, need to shape it more                              |
| Function                  | Good      | No        | Used extensively, interface stable                            |
| Utility                   | Good      | Yes       | Decent usage                                                  |
| Object & Packet           | Good      | No        | Recently rewritten and used by a few modules, needs to settle |
| Handler                   | Good      | No        | Widely used and proven to work                                |
| Template                  | Good      | Yes       | Experimental, looking for a good use                          |
| Hook                      | Bad       | No        | Pending complete rework                                       |

## miQ Subsystems
| Name                      | Status    | Tested    | Note                                                          |
| ------------------------- | --------- | --------- | ------------------------------------------------------------- |
| log                       | Good      | No        | Used widely, stable interface                                 |
| ipc                       | Good      | No        | Used widely, stable interface                                 |
| trap                      | Good      | No        | Used widely, stable interface                                 |

## miQ types & casting
| Name                      | Status    | Tested    | Note                                                          |
| ------------------------- | --------- | --------- | ------------------------------------------------------------- |
| is                        | Good      | No        | Used extensively, stable interface                            |
| to                        | Broken    | No        | Needs many improvements                                       |

## STL modules
| Name      | Status    | Tested    | Documented    | Note                                                          |
| --------- | --------- | --------- | ------------- | ------------------------------------------------------------- |
| arg       | Good      | Yes       | Yes           | Requires rework of arg parsing                                |
| clan      | Working   | No        | No            | Major upgrade required                                        |
| env       | Working   | No        | No            | Too simplistic, may need update?                              |
| exec      | Working   | No        | No            | Needs work on more features                                   |
| ipc       | Good      | Yes       | Yes           | Widely used, stable interface                                 |
| log       | Working   | No        | Yes           | May need update                                               |
| mat       | Bad       | No        | No            | Requires complete rework                                      |
| prof      | Good      | No        | Yes           | Working, interface should be stable                           |
| seq       | Working   | No        | No            | Needs improving                                               |
| schema    | Working   | No        | No            | Needs improving                                               |
| slot      | Working   | No        | No            | Used extensively, needs facelift and snapshotting upgrade     |
| timer     | Broken    | No        | No            | Needs complete rework                                         |
| tz        | Working   | No        | No            | More optimisation required                                    |
| tp        | Stub      | No        | No            | Incomplete                                                    |
| test      | Good      | No        | No            | Used to test the miQ ecosystem, good to use                   |


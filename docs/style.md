Code style
===
---
Code is usually written once and read many times over. One writes code for others to read, and ensures it's clean and easy to read for everybody, not only for author.

## Objectives
* Readability
  * Sensible and descriptive variable and function identifiers
  * Purpose of a function has to be determinable in isolation
  * Single line should not contain more than one statement
* Consistency
  * Follow code formatting rules
  * Follow standards

## q/k
* Always use `get` over `value` keyword; *no exceptions*
* Do not use k unless it is the right thing to do
* If you nonetheless use k, do not mix it with q and write the expression in k only

## Indentation & Spacing
* *No tabs*
* 2 spaces comprise a single indentation level
* no trailing whitespace at the end of the line
* Single empty line between function definitions
* Group related functions together and demarcate them visually

## Statements
* Every statement ends with a semicolon
  * Exception: the last statement of a function
* Use folding to structure long statements over multiple lines
* Use `-3!` instead of `string` to print objects into output, as it conserves the object's structure
* For multiline statements, indent closing brackets at indentation depth of first line in the statement

## Hungarian notations
* Prepend [type notation](types#miq-type-notations) to formal parameter name - for example, `jlVolume` is a long list
* If formal parameter is of generic type, or accepts multiple types, don't prepend any type to formal parameter
* Lazy notation allowed - for example, `jlVolume` could be simply referred to as `jl`
* `l` means `enlist` and can be repeated - for example, `jll` is a list of `jl` elements

## Variables & Constants
* Naming
  * *No single letter identifiers*, unless used in sleek lambdas
  * *No underscores in identifiers*
* Globals
  * Forbidden
  * Simply don't
* Variables
  * No data should be stored in the module's namespace, bar transient values that can be lost during module reload
  * If local to function, exempt from hungarian notation rule
* Constants
  * Holds address to the desired object
  * All characters are uppercase
  * Break long names using `.`
  * Should not change their initial value

## Functions
* **ALWAYS** define and call function using it's fully qualified name
* **NEVER** rely on `\d` to call function using it's relative name
* Group related lines together and separate with a single empty line from other groups
* Do not make function longer than it has to be
* Break function into smaller functions if it's doing too many things
* Naming
  * *Short and descriptive*
  * Break long names using `.`
  * Always define within a non-root context
* Formal parameters
  * *Do not modify*, copy first
  * Follow [hungarian notation](#Hungarian-notations) and use camelCase
  * Don't use table to modify function's behaviour or as a source of input
* Return value
  * Last statement should provide functions return value
  * Use `:` only for early / explicit return
* Comments
  * If needed, write a short comment of function's purpose right above it's definition
  * If function is complex and can't be broken down, consider including an example in absence of documentation
* Lambdas
  * Short, single use, throwaway function
  * default formal parameters `x`, `y`, `z` are permitted if input is properly named

## Comments
* Single slash `/` for comments explaining bits of code
* Double slash `//` for visual demarcation of code
* Code should be self-documenting and not require many comments
* Do comment the code when it is the right thing to do
* Don't comment for the sake of commenting
* Don't overuse temporary comments, such as `TODO` and `FIXME`

## qSQL & functional form
* Use qSQL for readability
* Use functional form when it's the right thing to do

## System commands
* Use `system"cmd"` over backslash `\cmd`
* Don't rely on `\d` unless you have a very good reason

